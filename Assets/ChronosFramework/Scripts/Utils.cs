﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ChronosFramework
{
    public delegate void UpdateAction();
    public delegate void UpdateAction<T>(T obj);
    public delegate void UpdateAction<T1, T2>(T1 obj1, T2 obj2);
    public delegate void UpdateAction<T1, T2, T3>(T1 obj1, T2 obj2, T3 obj3);

    public enum EasingType
    {
        Linear, EaseIn, EaseOut, ElasticIn, ElasticOut, ElasticInOut
    }

    public enum RenderingMode
    {
        Opaque, Cutout, Fade, Transparent
    }

    [System.Serializable]
    public struct EasingCurve
    {
        public AnimationCurve curve;
        public EasingType easingType;
    }

    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null)
            {
                if (Instance != this)
                {
                    Destroy(gameObject);
                }
            }
            else
            {
                Instance = this as T;
            }
        }
    }

    public static class Bezier
    {
        public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
        {
            t = Mathf.Clamp01(t);
            float oneMinusT = 1f - t;
            return
                oneMinusT * oneMinusT * oneMinusT * p0 +
                3f * oneMinusT * oneMinusT * t * p1 +
                3f * oneMinusT * t * t * p2 +
                t * t * t * p3;
        }

        public static Vector3 GetFirstDerivative(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
        {
            t = Mathf.Clamp01(t);
            float oneMinusT = 1f - t;
            return
                3f * oneMinusT * oneMinusT * (p1 - p0) +
                6f * oneMinusT * t * (p2 - p1) +
                3f * t * t * (p3 - p2);
        }
    }

    public static class Utility
    {
#region Tweens
        public static Tween FadeIn(this UnityEngine.UI.Graphic graphic, float time, float alpha = 1f)
        {
            Tween fadeInTween = new Tween(0, alpha, time, true, EasingType.Linear, (newAlpha) =>
            {
                Color newColor = graphic.color;
                newColor.a = newAlpha;
                graphic.color = newColor;
            });

            return fadeInTween;
        }

        public static Tween FadeOut(this UnityEngine.UI.Graphic graphic, float time)
        {
            Tween fadeOutTween = new Tween(graphic.color.a, 0f, time, true, EasingType.Linear, (newAlpha) =>
            {
                Color newColor = graphic.color;
                newColor.a = newAlpha;
                graphic.color = newColor;
            });

            return fadeOutTween;
        }

        public static Tween FadeIn(this CanvasGroup group, float time, float alpha = 1f)
        {
            Tween fadeInTween = new Tween(0, alpha, time, true, EasingType.Linear, (newAlpha) =>
            {
                group.alpha = newAlpha;
            });

            return fadeInTween;
        }

        public static Tween FadeOut(this CanvasGroup group, float time)
        {
            Tween fadeOutTween = new Tween(group.alpha, 0f, time, true, EasingType.Linear, (newAlpha) =>
            {
                group.alpha = newAlpha;
            });

            return fadeOutTween;
        }

        public static Tween FadeOut(float time, params UnityEngine.UI.Graphic[] graphics)
        {
            Tween fadeOutTween = new Tween(1f, 0, time, true, EasingType.Linear, (newAlpha) =>
            {
                graphics.ForEach((graphic) =>
                {
                    Color newColor = graphic.color;
                    newColor.a = newAlpha;
                    graphic.color = newColor;
                });
            });

            return fadeOutTween;
        }

        public static Tween FadeIn(this Material material, float time)
        {
            Tween fadeInTween = new Tween(0f, 1f, time, true, EasingType.Linear, (newAlpha) =>
            {
                Color newColor = material.color;
                newColor.a = newAlpha;
                material.color = newColor;
            });

            return fadeInTween;
        }

        public static Tween FadeOut(this Material material, float time)
        {
            Tween fadeOutTween = new Tween(material.color, Color.clear, time, true, EasingType.Linear, (newColor) =>
            {
                material.color = newColor;
            });

            return fadeOutTween;
        }
        #endregion
#region Collections
        public static void Shuffle(this Array array)
        {
            //for (int i = 0; i < array.Rank; i++)
            //{
            //    for (int j = array.GetLength(i); j > 0; j--)
            //    {
            //        int r = UnityEngine.Random.Range(0, i);
            //        var temp = array.GetValue(i);
            //        array.SetValue(array.GetValue(r), i);
            //        array.SetValue(temp, r);
            //    }
            //}
            for (int i = 0; i < array.Length; i++)
            {
                int r = UnityEngine.Random.Range(0, i);
                var temp = array.GetValue(i);
                array.SetValue(array.GetValue(r), i);
                array.SetValue(temp, r);
            }
        }

        public static IEnumerable<TResult> Zip<T1, T2, TResult>(this IEnumerable<T1> collection1, IEnumerable<T2> collection2, Func<T1,T2,TResult> callback)
        {
            var e1 = collection1.GetEnumerator();
            var e2 = collection2.GetEnumerator();

            while (e1.MoveNext() && e2.MoveNext())
            {
                yield return callback(e1.Current, e2.Current);
            }
        }

        public static void Iterate<T1, T2>(this IEnumerable<T1> collection1, IEnumerable<T2> collection2, UpdateAction<T1, T2> callback)
        {
            var e1 = collection1.GetEnumerator();
            var e2 = collection2.GetEnumerator();

            while (e1.MoveNext() && e2.MoveNext())
            {
                callback(e1.Current, e2.Current);
            }
        }

        public static void Iterate<T1, T2, T3>(this IEnumerable<T1> collection1, IEnumerable<T2> collection2, IEnumerable<T3> collection3, 
                                               UpdateAction<T1, T2, T3> callback)
        {
            var e1 = collection1.GetEnumerator();
            var e2 = collection2.GetEnumerator();
            var e3 = collection3.GetEnumerator();

            while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
            {
                callback(e1.Current, e2.Current, e3.Current);
            }
        }

        public static void ForEach<T>(this IEnumerable<T> collection, UpdateAction<T> callback)
        {
            var e = collection.GetEnumerator();
            e.MoveNext();

            callback += delegate
            {
                if (e.MoveNext())
                {
                    callback(e.Current);
                }
                else
                    return;
            };

            callback(e.Current);
        }
        #endregion

        public static string RemoveDiacritics(this string text)
        {
            string formD = text.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();

            foreach (char ch in formD)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(ch);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(ch);
                }
            }

            return sb.ToString().Normalize(NormalizationForm.FormC);
        }

        public static AnimationCurve GetAnimationCurve(this EasingType curve)
        {
            return CoreManager.Instance.GetCurve(curve);
        }

        public static void SetRenderingMode(this Material material, RenderingMode renderingMode)
        {
            switch (renderingMode)
            {
                case RenderingMode.Opaque:
                    material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    material.SetInt("_ZWrite", 1);
                    material.DisableKeyword("_ALPHATEST_ON");
                    material.DisableKeyword("_ALPHABLEND_ON");
                    material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    material.renderQueue = -1;
                    break;
                case RenderingMode.Cutout:
                    material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    material.SetInt("_ZWrite", 1);
                    material.EnableKeyword("_ALPHATEST_ON");
                    material.DisableKeyword("_ALPHABLEND_ON");
                    material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    material.renderQueue = 2450;
                    break;
                case RenderingMode.Fade:
                    material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                    material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    material.SetInt("_ZWrite", 0);
                    material.DisableKeyword("_ALPHATEST_ON");
                    material.EnableKeyword("_ALPHABLEND_ON");
                    material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    material.renderQueue = 3000;
                    break;
                case RenderingMode.Transparent:
                    material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    material.SetInt("_ZWrite", 0);
                    material.DisableKeyword("_ALPHATEST_ON");
                    material.DisableKeyword("_ALPHABLEND_ON");
                    material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                    material.renderQueue = 3000;
                    break;
            }
        }
    }
}
