﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace ChronosFramework
{
    public enum InputType { KeyDown, KeyUp, KeyHold}

    public static class InputManager
    {
        static Dictionary<KeyCode, UpdateAction> keyHoldActions = new Dictionary<KeyCode, UpdateAction>();
        static Dictionary<KeyCode, UpdateAction> keyDownActions = new Dictionary<KeyCode, UpdateAction>();
        static Dictionary<KeyCode, UpdateAction> keyUpActions = new Dictionary<KeyCode, UpdateAction>();

        public static void AddInputAction(KeyCode key, InputType type, UpdateAction action)
        {
            var inputActions = new Dictionary<KeyCode, UpdateAction>();

            switch (type)
            {                
                case InputType.KeyDown:
                    inputActions = keyDownActions;
                    break;
                case InputType.KeyUp:
                    inputActions = keyUpActions;
                    break;
                case InputType.KeyHold:
                    inputActions = keyHoldActions;
                    break;
            }

            if (inputActions.ContainsKey(key))
            {
                inputActions[key] += action;
            }
            else
            {
                if (keyDownActions.Count == 0 && keyUpActions.Count == 0 && keyHoldActions.Count == 0)
                {
                    EventManager.AddToUpdate(GetInput);
                }

                inputActions.Add(key, action);
            }
        }

        public static void RemoveInputAction(UpdateAction action)
        {
            bool found = false;

            KeyCode foundKey = 0;

            UpdateAction foundAction;

            if (keyHoldActions.TryGetValue(keyHoldActions.Keys.SingleOrDefault((a) => {foundKey = a;
                return keyHoldActions[a].GetInvocationList().Contains(action);}), out foundAction))
            {
                found = true;
                keyHoldActions[foundKey] -= action;
                if (keyHoldActions[foundKey] == null)
                {
                    keyHoldActions.Clear();
                }
            }

            if (keyDownActions.TryGetValue(keyDownActions.Keys.SingleOrDefault((a) => {foundKey = a;
                return keyDownActions[a].GetInvocationList().Contains(action);}), out foundAction))
            {
                found = true;
                keyDownActions[foundKey] -= action;
                if (keyDownActions[foundKey] == null)
                {
                    keyDownActions.Clear();
                }
            }

            if (keyUpActions.TryGetValue(keyUpActions.Keys.SingleOrDefault((a) => {foundKey = a;
                return keyUpActions[a].GetInvocationList().Contains(action);}), out foundAction))
            {
                found = true;
                keyUpActions[foundKey] -= action;
                if (keyUpActions[foundKey] == null)
                {
                    keyUpActions.Clear();
                }
            }

            if (!found)
            {
                Debug.LogWarning(action + " could not be removed because it was not found.");
            }
        }

        static void GetInput()
        {
            keyDownActions.Keys.ForEach((k) =>
            {
                if (Input.GetKeyDown(k))
                {
                    keyDownActions[k]();
                }
            });

            keyUpActions.Keys.ForEach((k) =>
            {
                if (Input.GetKeyUp(k))
                {
                    keyUpActions[k]();
                }
            });

            keyHoldActions.Keys.ForEach((k) =>
            {
                if (Input.GetKey(k))
                {
                    keyHoldActions[k]();
                }
            });
        }
    }
}
