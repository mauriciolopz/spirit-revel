﻿using UnityEngine;
using System.Collections;

namespace ChronosFramework
{
    public class Timer
    {
        public event UpdateAction OnComplete;

        public bool isLoop { get; set; }
        public float duration { get; set; }

        UpdateAction Update;
        
        float elapsedTime;
        
        public Timer(float duration, UpdateAction callback)
        {
            this.duration = duration;

            OnComplete += callback;
            
            Update = delegate
            {
                elapsedTime += Time.deltaTime;

                if (elapsedTime >= this.duration)
                {
                    OnComplete();

                    if (isLoop)
                    {
                        elapsedTime = 0;
                    }
                    else
                    {
                        Update.RemoveFromUpdate();
                    }
                }
            };

            Update.AddToUpdate();
        }

        public void Start()
        {
            Update.AddToUpdate();
        }

        public void Pause()
        {
            Update.RemoveFromUpdate();
        }

        public void Stop()
        {
            Update.RemoveFromUpdate();

            elapsedTime = 0;
        }
    }
}
