﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuSphere : MonoBehaviour {

    [SerializeField] Transform rotationPivot;
    [SerializeField] bool isPivot;
    [SerializeField] float speed;
    [SerializeField] float angularSpeed;
    const float nearest = 0.5f;
    const float farthest = 3.5f;

	
	void Update () {

        Orbit();
	}

    public void Orbit() {

        if(!isPivot) {

            float distance = Random.Range(-angularSpeed, angularSpeed) * Time.deltaTime;
            transform.localPosition = transform.localPosition + (distance * transform.localPosition.normalized);
            if(Vector3.Distance(transform.position, rotationPivot.position) > farthest) {
                transform.position = rotationPivot.transform.position + ((transform.position - rotationPivot.position).normalized * farthest);
            }
            else if(Vector3.Distance(transform.position, rotationPivot.position) < nearest) {
                transform.position = rotationPivot.transform.position + ((transform.position - rotationPivot.position).normalized * nearest);
            }
            if(transform.parent == null) {
                transform.SetParent(rotationPivot);
            }

            transform.RotateAround(rotationPivot.transform.position, Vector3.forward, speed * Time.deltaTime);
        }
        else {

            transform.position += transform.up * speed * Time.deltaTime;
            if(transform.position.y > 5 || transform.position.y < -5 || transform.position.x > 7.5f || transform.position.x < -7.5f) {
                for(int i = transform.childCount-1; i >= 0; i--) {
                    transform.GetChild(i).SetParent(null);
                }
                transform.up = -transform.position;
            }

            transform.eulerAngles += Vector3.forward * angularSpeed * Time.deltaTime;
        }
    }
}
