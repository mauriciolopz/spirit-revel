﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiritChaseFollower : MonoBehaviour {

    bool selfDestructing = false;

	void Update () {
		
        if(transform.parent == null && !selfDestructing) {
            selfDestructing = true;
            Destroy(gameObject, 1.0f);
        }
	}
}
