﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;
using UnityEngine.UI;

public class Lion : Singleton<Lion> {

    [SerializeField] GameObject mane;
    [SerializeField] float postAttackDelay;
    [SerializeField] float hp;
    [SerializeField] GameObject hpBar;
    [SerializeField] float maneHp;
    [SerializeField] float timeToRecoverMane;
    [SerializeField] Material whiteMaterial;
    [SerializeField] Material vertexColorMaterial;
    [SerializeField] SkinnedMeshRenderer originalSkin;
    [SerializeField] GameObject owlCurse;
    Animator animator;
    Timer attackTimer;
    float initialTimeToAttack = 1.25f;
    float timeEllapsedToRecoverMane;
    //Timer owlDrainDamageTimer;
    Vector3 initialPosition;
    float initialHp;
    float initialManeHp;
    bool hasMane = true;
    public bool HasMane { get { return hasMane; } }
    bool finishedLastAttack = false;
    bool canAttack = false;
    bool battleStarted = false;
    bool touchDealsDamage = false;
    float recoverManeDelayMultiplier = 1.0f;
    public bool IsActive;

    [Header("Majestic Roar")]
    [SerializeField] GameObject majesticRoar;
    [SerializeField] GameObject majesticRoarIndicator;
    [SerializeField] float mr_delayTime;
    [SerializeField] float mr_comboInterval;
    [SerializeField] float mr_postDelayTime;
    [SerializeField] int mr_totalShots;
    int mr_releasedShots = 0;

    [Header("Spirit Chase")]
    [SerializeField] GameObject spiritChaseSphere;
    [SerializeField] float[] sc_timeForSpheres;
    int sc_launchedSpheres = 0;

    [Header("Natures Wrath")]
    [SerializeField] GameObject naturesWrath;
    [SerializeField] float nw_delayTime;
    [SerializeField] float nw_timeToDisappear;
    [SerializeField] int nw_fieldsToRise;

    [Header("Frenzy Hunt")]
    [SerializeField] Transform frenzyHuntWaypointsParent;
    [SerializeField] float fh_speed;
    [SerializeField] float fh_breathTime;
    int fh_incomingWaypoint = 0;

    [Header("Kings Descent")]
    [SerializeField] GameObject kd_impactZone;
    [SerializeField] GameObject kd_impactZoneIndicator;
    [SerializeField] float kd_riseTime;
    [SerializeField] float kd_fallTime;
    [SerializeField] float kd_riseFallDelay;

    [Header("Hide Seeker")]
    [SerializeField] float hs_shortJumpTime;
    [SerializeField] float hs_longJumpTime;
    [SerializeField] float hs_bitesDelay;
    [SerializeField] float hs_shortLongJumpDelay;
    [SerializeField] float hs_shortDistance;
    [SerializeField] float hs_longDistance;
    [SerializeField] int hs_minBites;
    [SerializeField] int hs_maxBites;
    int hs_nBites = -1;
    int hs_bitesUsed = 0;

    [Header("Sounds")]
    [SerializeField] AudioClip mrSound;
    [SerializeField] AudioClip scSound;
    [SerializeField] AudioClip nwSound;
    [SerializeField] AudioClip[] fhSounds;
    [SerializeField] AudioClip kdSound;
    [SerializeField] AudioClip[] hsSounds;
    AudioSource thisAudioSource;


	void Start () {

        animator = GetComponent<Animator>();
        thisAudioSource = GetComponent<AudioSource>();
        initialHp = hp;
        initialManeHp = maneHp;
        initialPosition = transform.position;
        frenzyHuntWaypointsParent.SetParent(null);
        naturesWrath.transform.SetParent(null);
        kd_impactZone.transform.SetParent(null);
        kd_impactZoneIndicator.transform.SetParent(null);
    }
	
	void Update () {

        if (!IsActive)
            return;

        if(IsActive && !hpBar.activeInHierarchy) {
            ShowHPBar();
        }

        if(!battleStarted && !canAttack) {
            battleStarted = true;
            attackTimer = new Timer(initialTimeToAttack, () => canAttack = true);
        }

        if(!hasMane) {

            timeEllapsedToRecoverMane += (Time.deltaTime * recoverManeDelayMultiplier);

            if(finishedLastAttack && timeEllapsedToRecoverMane > timeToRecoverMane) {
                transform.position = initialPosition;
                transform.eulerAngles = new Vector3(0.0f, 180.0f, 0.0f);
                mane.SetActive(true);
                hasMane = true;
                timeEllapsedToRecoverMane = 0.0f;
            }
        }

        if(canAttack) {
            Attack();
            canAttack = false;
            finishedLastAttack = false;
        }
	}

    void ShowHPBar() {

        hpBar.SetActive(true);
        CanvasGroup canvasGroup = hpBar.GetComponent<CanvasGroup>();

        System.Action<float> FadeAlpha = (alpha) => {
            canvasGroup.alpha = alpha;
        };

        new Tween(0.0f, 1.0f, 0.75f, false, EasingType.Linear, FadeAlpha);
    }

    void Attack() {

        //int chosenAttack = Random.Range(0, 3);
        int chosenAttack = 2;

        if(hasMane) {
            if (chosenAttack == 0) UseMajesticRoar();
            else if (chosenAttack == 1) UseSpiritChase();
            else if (chosenAttack == 2) UseNaturesWrath();
        }
        else {
            if (chosenAttack == 0) UseFrenzyHunt();
            else if (chosenAttack == 1) UseKingsDescent();
            else if (chosenAttack == 2) UseHideSeeker();
        }
    }

    void UseMajesticRoar() {

        transform.LookAt(Player.Instance.transform.position);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
        majesticRoarIndicator.SetActive(true);
        animator.SetTrigger("majesticRoared");

        mr_releasedShots++;

        new Timer(mr_delayTime, () => {

            if(this == null || !hasMane) return;
            majesticRoar.SetActive(true);
            majesticRoarIndicator.SetActive(false);
            thisAudioSource.clip = mrSound;
            thisAudioSource.Play();

            new Timer(mr_postDelayTime, () => {

                if(this == null || !hasMane) return;
                majesticRoar.SetActive(false);

                new Timer(mr_comboInterval, () => {

                    if(this == null || !hasMane) return;
                    if(mr_releasedShots < mr_totalShots) {
                        UseMajesticRoar();
                    }
                    else {
                        mr_releasedShots = 0;
                        finishedLastAttack = true;
                        transform.eulerAngles = new Vector3(0, 180, 0);
                        if(attackTimer != null) {
                            attackTimer.Stop();
                        }
                        attackTimer = new Timer(postAttackDelay, () => canAttack = true);
                    }
                });
            });
        });
    }

    void UseSpiritChase() {

        float arcDistance = 4.0f;
        sc_launchedSpheres = 0;

        animator.SetBool("naturesWrathed", true);
        thisAudioSource.clip = scSound;
        thisAudioSource.Play();

        for (int i = 0; i < sc_timeForSpheres.Length; i++) {
            
            GameObject sphere = Instantiate(spiritChaseSphere);
            sphere.GetComponent<SpiritChaseSphere>().enabled = false;
            float angle = (270 + (i * 45)) * Mathf.Deg2Rad;
            sphere.transform.position = transform.position + (Vector3.up * 3.0f) + new Vector3(Mathf.Sin(angle) * arcDistance, Mathf.Cos(angle) * arcDistance, 0);

            new Timer(sc_timeForSpheres[i], () => {

                if(this == null) return;
                sc_launchedSpheres++;
                if(sphere == null) return;

                sphere.GetComponent<SpiritChaseSphere>().enabled = true;
                sphere.GetComponent<SpiritChaseSphere>().Speed = 16 + (sc_launchedSpheres * 6);
                sphere.GetComponent<SpiritChaseSphere>().AngularSpeed = Mathf.Max(90 - (sc_launchedSpheres * 15), 45);
            });
        }

        new Timer(sc_timeForSpheres[4], () => {
            finishedLastAttack = true;
            animator.SetBool("naturesWrathed", false);
        });
        attackTimer = new Timer(postAttackDelay + sc_timeForSpheres[4], () => canAttack = true);
    }

    void UseNaturesWrath() {

        animator.SetBool("naturesWrathed", true);
        thisAudioSource.clip = nwSound;
        thisAudioSource.Play();
        naturesWrath.SetActive(true);
        int nChildren = naturesWrath.transform.childCount;

        for(int i = 0; i < nw_fieldsToRise; i++) {
            int randomIndex = Random.Range(0, nChildren-i);
            naturesWrath.transform.GetChild(randomIndex).gameObject.SetActive(true);
            naturesWrath.transform.GetChild(randomIndex).eulerAngles += Vector3.up * (90 * Random.Range(0, 4));
            naturesWrath.transform.GetChild(randomIndex).localScale = new Vector3(naturesWrath.transform.GetChild(randomIndex).localScale.x, naturesWrath.transform.GetChild(randomIndex).localScale.y, 0.05f);
            Shake(naturesWrath.transform.GetChild(randomIndex));
            naturesWrath.transform.GetChild(randomIndex).SetAsLastSibling();
        }

        attackTimer = new Timer(nw_delayTime + nw_timeToDisappear + postAttackDelay, () => canAttack = true);

        new Timer(nw_delayTime, () => {

            if(this == null) return;
            for(int i = 0; i < nw_fieldsToRise; i++) {
                Transform spikeGroup = naturesWrath.transform.GetChild(nChildren - i - 1);
                spikeGroup.GetChild(0).GetComponent<Collider>().enabled = true;
                new Tween(spikeGroup.localScale.z, 1.0f, 0.13f, false, EasingType.EaseIn, (float zScale) => {
                    spikeGroup.localScale = new Vector3(spikeGroup.localScale.x, spikeGroup.localScale.y, zScale);
                });
            }

            new Timer(nw_timeToDisappear, () => {

                if(this == null) return;
                naturesWrath.SetActive(false);
                for(int i = 0; i < nw_fieldsToRise; i++) {
                    naturesWrath.transform.GetChild(nChildren - i - 1).GetChild(0).GetComponent<Collider>().enabled = false;
                    naturesWrath.transform.GetChild(nChildren - i - 1).gameObject.SetActive(false);
                }
                finishedLastAttack = true;
                animator.SetBool("naturesWrathed", false);
            });
        });
    }

    void UseFrenzyHunt() {

        animator.SetTrigger("frenzyHunting");
        thisAudioSource.clip = fhSounds[Random.Range(0, fhSounds.Length)];
        thisAudioSource.Play();
        Vector3 nextWaypoint = frenzyHuntWaypointsParent.GetChild(fh_incomingWaypoint).position;

        if(!touchDealsDamage) {
            if(Vector3.Distance(transform.position, nextWaypoint) < 3.0f) {
                fh_incomingWaypoint++;
                nextWaypoint = frenzyHuntWaypointsParent.GetChild(fh_incomingWaypoint).position;
            }
        }
        touchDealsDamage = true;
        System.Action<Vector3> MoveToWaypoint = (routePoint) => {
            if(this == null) return;
            transform.position = routePoint;
        };
        
        if(Vector3.Distance(transform.position, nextWaypoint) < 0.25f) {
            fh_incomingWaypoint++;
            nextWaypoint = frenzyHuntWaypointsParent.GetChild(fh_incomingWaypoint).position;
        }
        transform.LookAt(nextWaypoint);
        float tweenTime = Vector3.Distance(transform.position, nextWaypoint) / fh_speed;
        animator.speed = 0.67f / tweenTime;
        Tween advance = new Tween(transform.position, nextWaypoint, tweenTime, true, EasingType.Linear, MoveToWaypoint);
        
        advance.OnComplete += () => {
            
            if(this == null) return;
            animator.SetTrigger("huntBreak");
            new Timer(fh_breathTime, () => { 

                if(this == null) return;
                fh_incomingWaypoint++;
                if(fh_incomingWaypoint < frenzyHuntWaypointsParent.childCount) {
                    UseFrenzyHunt();
                }
                else {
                    fh_incomingWaypoint = 0;
                    touchDealsDamage = false;
                    finishedLastAttack = true;
                    transform.eulerAngles = new Vector3(0, 180, 0);
                    animator.speed = 1.0f;
                    attackTimer = new Timer(postAttackDelay, () => canAttack = true);
                }
            });
        };
    }

    void UseKingsDescent() {

        animator.SetTrigger("kingsRising");

        System.Action<Vector3> Translate = (routePoint) => {
            if (this == null) return;
            transform.position = routePoint;
        };

        Vector3 targetPoint = Player.Instance.transform.position + (Vector3.up * 1.0f);
        Vector3 jumpPoint = transform.position + ((targetPoint - transform.position) * 0.5f) + (Vector3.up * 5.0f);
        transform.LookAt(jumpPoint);
        kd_impactZone.transform.position = targetPoint - (Vector3.up * 0.95f);
        kd_impactZoneIndicator.transform.position = targetPoint - (Vector3.up * 0.95f);
        kd_impactZoneIndicator.SetActive(true);

        Tween rise = new Tween(transform.position, jumpPoint, kd_riseTime, true, EasingType.Linear, Translate);
        rise.OnComplete += () => {

            if (this == null) return;
            new Timer(kd_riseFallDelay, () => {
                if (this == null) return;
                animator.SetTrigger("kingsDescending");
                transform.LookAt(targetPoint);
                Tween fall = new Tween(transform.position, targetPoint, kd_fallTime, true, EasingType.Linear, Translate);
                new Timer(kd_fallTime*0.8f, () => {
                    if(this == null) return;
                    thisAudioSource.clip = kdSound;
                    thisAudioSource.Play();
                });
                fall.OnComplete += () => {
                    if(this == null) return;
                    transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
                    kd_impactZoneIndicator.SetActive(false);
                    kd_impactZone.SetActive(true);
                    finishedLastAttack = true;
                    new Timer(0.2f, () => kd_impactZone.SetActive(false));
                    attackTimer = new Timer(postAttackDelay, () => canAttack = true);
                };
            });
        };
    }

    void UseHideSeeker() {

        if (hs_nBites == -1) {
            hs_nBites = Random.Range(hs_minBites, hs_maxBites+1);
            hs_bitesUsed = 0;
        }

        System.Action<Vector3> Advance = (routePoint) => {
            transform.position = routePoint;
        };

        touchDealsDamage = true;
        Vector3 directionToPlayer = (Player.Instance.transform.position - transform.position).normalized;
        Vector3 targetPoint = transform.position + directionToPlayer * hs_shortDistance;
        transform.LookAt(targetPoint);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

        animator.SetTrigger("shortHideSeeked");
        thisAudioSource.clip = hsSounds[Random.Range(0, hsSounds.Length)];
        thisAudioSource.Play();
        Tween shortAdvance = new Tween(transform.position, targetPoint, hs_shortJumpTime, true, EasingType.Linear, Advance);
        shortAdvance.OnComplete += () => {

            if (this == null) return;
            touchDealsDamage = false;
            new Timer(hs_shortLongJumpDelay, () => {

                if (this == null) return;
                touchDealsDamage = true;
                directionToPlayer = (Player.Instance.transform.position - transform.position).normalized;
                targetPoint = transform.position + directionToPlayer * hs_longDistance;
                transform.LookAt(targetPoint);
                transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

                animator.SetTrigger("longHideSeeked");
                thisAudioSource.clip = fhSounds[Random.Range(0, fhSounds.Length)];
                thisAudioSource.Play();
                Tween longAdvance = new Tween(transform.position, targetPoint, hs_shortJumpTime, true, EasingType.Linear, Advance);
                longAdvance.OnComplete += () => {

                    if(this == null) return;
                    hs_bitesUsed++;
                    if (hs_bitesUsed < hs_nBites) {
                        touchDealsDamage = false;
                        new Timer(hs_bitesDelay, UseHideSeeker);
                    }
                    else {
                        hs_bitesUsed = 0;
                        hs_nBites = -1;
                        finishedLastAttack = true;
                        animator.SetBool("hideSeeking", false);
                        attackTimer = new Timer(postAttackDelay, () => canAttack = true);
                    }
                };
            });
        };
    }

    public void ManeTakeDamage(int damage) {

        maneHp -= damage;
        //Color maneColor = mane.GetComponent<Renderer>().material.color;
        mane.GetComponent<Renderer>().material = whiteMaterial;
        new Timer(0.05f, () => {
            if(this == null) return;
            //mane.GetComponent<Renderer>().material.color = maneColor;
            mane.GetComponent<Renderer>().material = vertexColorMaterial;
        });

        if(maneHp <= 0) {
            LoseMane();
        }
    }

    void LoseMane() {
        
        if(attackTimer != null) {
            attackTimer.Stop();
            attackTimer = new Timer(3.0f, () => {
                if (this == null) return;
                canAttack = true;
            });
        }
        maneHp = initialManeHp;
        animator.SetTrigger("lostMane");
        mane.SetActive(false);
        hasMane = false;
        majesticRoarIndicator.SetActive(false);
        majesticRoar.SetActive(false);
    }

    public void TakeDamage(int damage) {

        hp -= damage;
        Color originalColor = Color.white;
        hpBar.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(hpBar.transform.GetChild(1).GetComponent<RectTransform>().sizeDelta.x * hp / initialHp, hpBar.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y);
        
        originalSkin.material = whiteMaterial;

        new Timer(0.05f, () => {
            if (this == null) return;
            originalSkin.material = vertexColorMaterial;
        });

        if (hp <= 0) {
            Die();
        }
    }

    void Die() {

        UIManager.Instance.ShowProvisoryEndScreen();
        Destroy(gameObject);
    }

    public void Reset() {
        
        transform.position = initialPosition;
        IsActive = false;
        hpBar.SetActive(false);
        hp = initialHp;
        maneHp = initialManeHp;
        mane.SetActive(true);
        hasMane = true;
        timeEllapsedToRecoverMane = 0.0f;
        gameObject.SetActive(false);
        if(attackTimer != null) {
            attackTimer.Stop();
        }
        mr_releasedShots = 0;
        touchDealsDamage = false;
        hpBar.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(hpBar.transform.GetChild(1).GetComponent<RectTransform>().sizeDelta.x * hp / initialHp, hpBar.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y);
    }

    public void OwlCurseStart(float timeCursed) {

        owlCurse.SetActive(true);
        fh_speed /= 3.0f;
        hs_longJumpTime *= 3.0f;
        hs_shortJumpTime *= 3.0f;
        kd_fallTime *= 3.0f;
        kd_riseTime *= 3.0f;
        recoverManeDelayMultiplier = 0.0f;

        new Timer(timeCursed, () => {
            if (this == null)
                return;
            OwlCurseEnd();
        });
    }

    public void OwlCurseEnd() {

        fh_speed *= 3;
        hs_longJumpTime /= 3;
        hs_shortJumpTime /= 3;
        kd_fallTime /= 3.0f;
        kd_riseTime /= 3.0f;
        recoverManeDelayMultiplier = 1.0f;
        owlCurse.SetActive(false);
    }

    void OnCollisionEnter(Collision other) {

        if (touchDealsDamage && other.transform.GetComponent<Player>()) {
            other.transform.GetComponent<Player>().TakeDamage(1);
        }
    }

    void Shake(Transform target) {

        Debug.Log(target);
        System.Action<Vector3> GoToDirection = (position) => {
            target.position = position;
        };
        const float max = 0.2f;
        Vector3 randomDirection = new Vector3(Random.Range(-max, max), 0.0f, Random.Range(-max, max));
        randomDirection = randomDirection.normalized * 0.4f;
        Tween firstTween = new Tween(target.position, target.position + randomDirection, nw_delayTime * 0.25f, false, EasingType.ElasticInOut, GoToDirection);

        firstTween.OnComplete += () => {

            new Tween(target.position, target.position - randomDirection, nw_delayTime * 0.25f, false, EasingType.ElasticInOut, GoToDirection);
        };
    }

    /*void SufferOwlDrain(float timeToTakeDamage) {

        if (this == null) return;
        TakeDamage(1);
        owlDrainDamageTimer = new Timer(timeToTakeDamage, () => {
            SufferOwlDrain(timeToTakeDamage);
        });
    }

    public void OwlDrainStart(float timeToTakeDamage) {

        owlDrainDamageTimer = new Timer(timeToTakeDamage, () => {
            SufferOwlDrain(timeToTakeDamage);
        });
        fh_speed /= 3.0f;
        hs_longJumpTime *= 3.0f;
        hs_shortJumpTime *= 3.0f;
        kd_fallTime *= 3.0f;
        kd_riseTime *= 3.0f;
        recoverManeDelayMultiplier = 0.0f;
    }

    public void OwlDrainEnd() {

        owlDrainDamageTimer.Stop();
        fh_speed *= 3;
        hs_longJumpTime /= 3;
        hs_shortJumpTime /= 3;
        kd_fallTime /= 3.0f;
        kd_riseTime /= 3.0f;
        recoverManeDelayMultiplier = 1.0f;
    }*/
}
