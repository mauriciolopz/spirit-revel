﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class SpiritChaseSphere : MonoBehaviour {
    
    public float Speed;
    public float AngularSpeed;
    const float angleToPlayerSnap = 3.0f;

    void Start () {

        transform.LookAt(Player.Instance.transform.position + Vector3.up);
	}
	
	void Update () {

        Move();
        new Timer(5.0f, () => {
            if(this == null) return;
            Destroy(gameObject);
        });

        if(transform.position.y < -1.0f) {
            transform.GetChild(1).SetParent(null);
            Destroy(gameObject);
        }
	}

    void Move() {

        Vector2 directionSelfToPlayer = new Vector2(Player.Instance.transform.position.x - transform.position.x, Player.Instance.transform.position.z - transform.position.z);
        Vector2 lookingDirection = new Vector2(transform.forward.x, transform.forward.z);
        float angleToPlayerVector = Vector2.Angle(directionSelfToPlayer, lookingDirection);
        
        Vector2 rightDirection = new Vector2(transform.right.x, transform.right.z) ;
        float angleRightToPlayer = Vector2.Angle(directionSelfToPlayer, rightDirection);
        float orientation = (angleRightToPlayer > 90) ? -1 : 1;

        if(angleToPlayerVector > angleToPlayerSnap) {

            Vector3 anglesAux = transform.eulerAngles;
            anglesAux.y += AngularSpeed * Time.deltaTime * orientation;
            transform.eulerAngles = anglesAux;
        }
        
        Vector3 velocity = transform.forward * Speed * Time.deltaTime;
        transform.position += new Vector3(velocity.x, velocity.y, velocity.z);
    }

    void OnTriggerEnter(Collider other) {

        if(other.GetComponent<Player>()) {
            other.GetComponent<Player>().TakeDamage(1);
            Destroy(gameObject);
        }
        else if(other.gameObject.layer == LayerMask.NameToLayer("Wall") || other.gameObject.layer == LayerMask.NameToLayer("Ground")) {
            Destroy(gameObject);
        }
    }
}
