﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class OwlFreezeExplosion : MonoBehaviour {

    public int damage;
    public float timeFrozen;
    [SerializeField] float lifetime;

    [Header("Animation")]
    [SerializeField] GameObject particle;
    [SerializeField] float outterLimit;
    [SerializeField] float innerLimit;
    [SerializeField] float spawnInterval;
    [SerializeField] float speed;
    [SerializeField] float size;
    [SerializeField] int particlesInPack;
    [SerializeField] int totalPacks;
    float ellapsedSpawnInterval = 300.0f;
    int packsSpawned = 0;
    public float Speed { get { return speed; } }
    
    
    void Start() {

        Destroy(gameObject, 3.0f);
    }

	void Update () {
        
        ellapsedSpawnInterval += Time.deltaTime;
        if(ellapsedSpawnInterval > spawnInterval && packsSpawned < totalPacks) {

            packsSpawned++;

            for(int i = 0; i < particlesInPack; i++) {

                float angle = (i * 360.0f) / (float)particlesInPack;
                Vector3 position = transform.position;

                /*GameObject newOutterParticle = Instantiate(particle);
                position.x = Mathf.Sin(angle * Mathf.Deg2Rad) * outterLimit;
                position.z = Mathf.Cos(angle * Mathf.Deg2Rad) * outterLimit;
                newOutterParticle.transform.position = transform.position + position;
                newOutterParticle.transform.eulerAngles = new Vector3(0, angle, 90);
                newOutterParticle.transform.localScale *= size;
                newOutterParticle.GetComponent<OwlFreezeParticles>().Speed = speed;
                newOutterParticle.GetComponent<OwlFreezeParticles>().Expansive = false;*/

                GameObject newInnerParticle = Instantiate(particle);
                newInnerParticle.transform.position = transform.position;
                newInnerParticle.transform.eulerAngles = new Vector3(0, angle, 90);
                newInnerParticle.transform.localScale *= size;
                newInnerParticle.GetComponent<OwlFreezeParticles>().Speed = speed;
                newInnerParticle.GetComponent<OwlFreezeParticles>().Lifetime = lifetime;
                newInnerParticle.GetComponent<OwlFreezeParticles>().Expansive = true;
            }
            ellapsedSpawnInterval = 0.0f;
        }
	}

	void OnTriggerEnter(Collider other) {

        if (other.GetComponent<Enemy>()) {

            Enemy enemyHit = other.GetComponent<Enemy>();
            enemyHit.Freeze(timeFrozen);
            enemyHit.TakeDamage(damage, false);
        }
    }
}
