﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;
using UnityEngine.UI;

public enum PlayerSkill { NONE, OWL, BEAR, PEACOCK, CAT, WALRUS };
public enum TargetType { NONE, CIRCLE, TARGET, CONE }

public class Player : Singleton<Player> {

    [Header("References")]
    [SerializeField] SkinnedMeshRenderer meshRenderer;
    [SerializeField] LayerMask groundLayer;
    [SerializeField] LayerMask enemiesLayer;
    [SerializeField] LayerMask targettingLayer;
    //[SerializeField] LayerMask waterLayer;
    [SerializeField] LayerMask waterAndGroundLayer;
    [SerializeField] GameObject bow;

    [Header("Attributes")]
    [SerializeField] int HP;
    public int Health { get { return HP; } }
    int maxHP;
    [SerializeField] float speed;
    [SerializeField] float dashSpeed;
    [SerializeField] float dashTime;
    [SerializeField] float arrowCooldown = 0.6f;
    float arrowEllapsedCooldown = 0.0f;
    [SerializeField] float dashCooldown = 2.5f;
    float dashEllapsedCooldown = 0.0f;
    [SerializeField] float peacockCooldown = 8.0f;
    [SerializeField] float catCooldown = 15.0f;
    [SerializeField] float bearCooldown = 7.0f;
    [SerializeField] float owlRange;
    [SerializeField] float owlCooldown = 12.0f;
    [SerializeField] float walrusRange;
    [SerializeField] float walrusCooldown = 1.5f;

    [Header("Control")]
    [SerializeField] float timeStaggered;
    [SerializeField] float takeDamageInvincibilityTime;
    Rigidbody thisRigidbody;
    Animator animator;
    WindReceiver windReceiver;
    Vector3 velocity;
    Vector3 moveTargetPoint;
    float normalSpeed;
    bool movingBeforeDash = false;
    bool movingBeforeHit = false;
    bool moving = false;
    bool canMove = true;
    bool canControl = true;
    public bool CanControl { set { canControl = value; } get { return canControl; } }
    bool isInCutscene = false;
    public bool IsInCutscene { set { isInCutscene = value; } }
    Vector3 previousGroundPosition;
    Vector3 fallingPosition;
    Timer previousPositionTimer;
    const float intervalToSavePreviousPosition = 0.5f;
    const float timeToRedefineMoveMouseHold = 0.5f;
    float ellapsedTimeToRedefineMove = 0.0f;
    float ellapsedTimeStuckInFall = 0.0f;
    bool falling = false;
    bool isDead = false;
    bool staggered = false;
    bool stackedMoveInput = false;

    [Header("Interactions")]
    [SerializeField] GameObject holdingLion;
    [SerializeField] Material invincibilityMaterial;
    Transform placeToGo;
    Transform checkpointToSave;
    int contactingOrbIndex = -1;
    bool canTakeOrb = false;
    bool canTalk = false;
    NPC talkingNPC;
    bool canSaveCheckpoint = false;
    bool canBreakBarrier = false;
    public bool CanBreakBarrier { set { canBreakBarrier = value; } }

    [Header("Skills")]
    [SerializeField] bool smartCast = false;
    public bool SmartCast { set { smartCast = value; } }
    [SerializeField] PlayerSkill skillOnE;
    [SerializeField] PlayerSkill skillOnR;
    [SerializeField] GameObject pe_feather;
    [SerializeField] int pe_nFeathers;
    [SerializeField] float ca_timeInvisible;
    [SerializeField] Material ca_invisibleMaterial;
    [SerializeField] float ca_speed;
    const float ca_alphaSubtraction = 0.7f;
    Material originalMaterial;
    bool canBeDetected = true;
    public bool CanBeDetected { get { return canBeDetected; } }
    [SerializeField] GameObject arrow;
    [SerializeField] Transform arrowSpawnReference;
    [SerializeField] GameObject be_bearWave;
    [SerializeField] GameObject wa_freezeExplosion;
    [SerializeField] float wa_freezeTime;
    //[SerializeField] Color ow_completeChargeColor;
    [SerializeField] GameObject ow_curse;
    [SerializeField] float ow_cursedTime;
    //[SerializeField] GameObject de_drain;
    //[SerializeField] float ow_timeToDamage;
    //[SerializeField] float ow_timeToHeal;
    //Timer ow_healTimer;
    //[SerializeField] int de_maxStacks;
    //[SerializeField] int de_stacksPerDrain;
    //[SerializeField] float de_stackingPerSecond;
    //[SerializeField] int de_stacksSpent;
    //[SerializeField] float de_angleForArrows;
    //int de_stacks;
    //GameObject de_enemyDraining;
    Color originalBowColor;
    Tween fadeBowTween;
    Timer fadeBowTimer;
    //const float de_maxStack = 1.0f;
    //float de_stacked;
    //bool de_cancelChannel = false;
    //public bool CanceledDeerChannel { get { return de_cancelChannel; } }
    //bool isChargingSkill;

    [Header("Targetting")]
    [SerializeField] GameObject targettingRangeCircle;
    [SerializeField] GameObject targetCircle;
    [SerializeField] GameObject targetTarget;
    [SerializeField] GameObject targetCone;
    [SerializeField] Text targetText;
    [SerializeField] Texture2D normalCursor;
    [SerializeField] Texture2D targetCursor;
    Vector3 targetPoint;
    TargetType eTargetType;
    TargetType rTargetType;
    float targetRange;
    bool targettingSkill = false;
    bool targetOutOfRange = false;
    char targettingSkillButton;
    bool castTargettedE = false;
    bool castTargettedR = false;

    [Header("Sounds")]
    AudioSource thisAudioSource;
    [SerializeField] AudioClip[] attackSounds;
    [SerializeField] AudioClip[] takeHitSounds;
    [SerializeField] AudioClip skillFailSound;
    [SerializeField] AudioClip owlCastSound;
    [SerializeField] AudioClip bearCastSound;
    AudioSource UIAudioSource;

    [Header("SkillsFlags")]
    UpdateAction useSkillOnE;
    UpdateAction useSkillOnR;
    float eCooldown = 0.0f;
    float eEllapsedCooldown = 0.0f;
    float rCooldown = 0.0f;
    float rEllapsedCooldown = 0.0f;
    float eRange;
    float rRange;
    bool canUseQ = true;
    bool canUseW = true;
    bool canUseE = true;
    bool canUseR = true;
    bool skillWasUsed = false;


    void Start () {

        thisRigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        windReceiver = GetComponent<WindReceiver>();
        thisAudioSource = GetComponent<AudioSource>();
        UIAudioSource = UIManager.Instance.GetComponent<AudioSource>();
        originalMaterial = meshRenderer.material;
        originalBowColor = bow.GetComponent<MeshRenderer>().material.color;
        maxHP = HP;
        normalSpeed = speed;

        //if(!GameManager.Instance.Loading) {
            DefineSkills();
            UIManager.Instance.ChangeSkills(skillOnE, skillOnR);
        //}

        previousPositionTimer = new Timer(intervalToSavePreviousPosition, () => {
            RaycastHit rayHit;
            if(Physics.Raycast(transform.position, -Vector3.up, out rayHit, 1.0f, groundLayer)) {
                if(rayHit.point.y > 0.38f) {
                    previousGroundPosition = transform.position - (Vector3.up * 0.3f);
                }
            }
        });
        previousPositionTimer.isLoop = true;
    }

    public void SetSkills(PlayerSkill onE, PlayerSkill onR) {

        skillOnE = onE;
        skillOnR = onR;
        DefineSkills();
    }

    void DefineSkills() {

        eRange = -1;
        eTargetType = TargetType.NONE;
        if(skillOnE == PlayerSkill.NONE) {
            useSkillOnE = null;
            eCooldown = 0.0f;
        }
        else if(skillOnE == PlayerSkill.PEACOCK) {
            useSkillOnE = UseSkillPeacock;
            eCooldown = peacockCooldown;
        }
        else if(skillOnE == PlayerSkill.CAT) {
            useSkillOnE = UseSkillCat;
            eCooldown = catCooldown;
        }
        else if(skillOnE == PlayerSkill.BEAR) {
            useSkillOnE = UseSkillBear;
            eCooldown = bearCooldown;
            eTargetType = TargetType.CONE;
        }
        else if(skillOnE == PlayerSkill.OWL) {
            useSkillOnE = UseSkillOwlCurse;
            eCooldown = owlCooldown;
            eRange = owlRange;
            eTargetType = TargetType.TARGET;
        }
        else if(skillOnE == PlayerSkill.WALRUS) {
            useSkillOnE = UseSkillWalrus;
            eCooldown = walrusCooldown;
            eRange = walrusRange;
            eTargetType = TargetType.CIRCLE;
        }

        rRange = -1;
        rTargetType = TargetType.NONE;
        if(skillOnR == PlayerSkill.NONE) {
            useSkillOnR = null;
            rCooldown = 0.0f;
        }
        else if(skillOnR == PlayerSkill.PEACOCK) {
            useSkillOnR = UseSkillPeacock;
            rCooldown = peacockCooldown;
        }
        else if(skillOnR == PlayerSkill.CAT) {
            useSkillOnR = UseSkillCat;
            rCooldown = catCooldown;
        }
        else if(skillOnR == PlayerSkill.BEAR) {
            useSkillOnR = UseSkillBear;
            rCooldown = bearCooldown;
            rTargetType = TargetType.CONE;
        }
        else if(skillOnR == PlayerSkill.OWL) {
            useSkillOnR = UseSkillOwlCurse;
            rCooldown = owlCooldown;
            rRange = owlRange;
            rTargetType = TargetType.TARGET;
        }
        else if(skillOnR == PlayerSkill.WALRUS) {
            useSkillOnR = UseSkillWalrus;
            rCooldown = walrusCooldown;
            rRange = walrusRange;
            rTargetType = TargetType.CIRCLE;
        }
    }
	
	void Update () {

        if(isDead) {
            return;
        }
        CheckFalling();

        if(!canMove || !canControl) {
            moving = false;
            animator.SetBool("running", false);
        }
        if(moving) {
            Move();
        }
        if(targettingSkill) {
            TargetSkill();
        }
        if(!GameManager.Instance.GameIsPaused) {
            if(canControl && !staggered) {
                ReadInputs();
                thisRigidbody.velocity = new Vector3(velocity.x, thisRigidbody.velocity.y, velocity.z);
            }
            else {
                ReadTargetMoveInput();
            }
        }
        /*if(de_drain.activeSelf) {
            OwlDraining();
        }*/
        if(windReceiver.IsBeingDragged) {
            Vector3 velocityWithWind = velocity + windReceiver.WindForce;
            thisRigidbody.velocity = thisRigidbody.velocity = new Vector3(velocityWithWind.x, thisRigidbody.velocity.y, velocityWithWind.z);
            //de_cancelChannel = true;
        }
        TrackCooldowns();

        if(canControl && (placeToGo || (canTalk && talkingNPC.CanBeTalkedTo) || canSaveCheckpoint || (canBreakBarrier && GameManager.Instance.BarrierDamaged))) {
            UIManager.Instance.ShowInteractionPopup(true, Enemy.EnemiesDetectedPlayer.Count == 0);
        }
        else {
            UIManager.Instance.ShowInteractionPopup(false, false);
        }
    }

    void ReadInputs() {

        // TEST
        if(GameManager.Instance.DeveloperMode) {

            if(Input.GetKeyDown(KeyCode.G)) {
                DefineMoveTargetPoint();
                TurnTargetsOff();
                transform.position = moveTargetPoint;
            }
            if(Input.GetKeyDown(KeyCode.D)) {
                SetNextSkillOnE();
                UIManager.Instance.ChangeSkills(skillOnE, skillOnR);
            }
            if(Input.GetKeyDown(KeyCode.F)) {
                SetNextSkillOnR();
                UIManager.Instance.ChangeSkills(skillOnE, skillOnR);
            }
            if(Input.GetKeyDown(KeyCode.S)) {
                smartCast = !smartCast;
            }
            if(Input.GetKeyDown(KeyCode.A)) {
                eCooldown = 0.5f;
                rCooldown = 0.5f;
            }
        }
        //----------
        if(ellapsedTimeToRedefineMove < timeToRedefineMoveMouseHold) {
            ellapsedTimeToRedefineMove += Time.deltaTime;
        }
        if(Input.GetMouseButtonDown(1) || (Input.GetMouseButton(1) && ellapsedTimeToRedefineMove >= timeToRedefineMoveMouseHold)) {
            ellapsedTimeToRedefineMove = 0.0f;
            DefineMoveTargetPoint();
            TurnTargetsOff();
            targettingSkill = false;
            Cursor.SetCursor(normalCursor, Vector2.zero, CursorMode.Auto);

            targetText.gameObject.SetActive(false);
        }
        if((Input.GetKeyDown(KeyCode.Q) || (Input.GetMouseButtonDown(0) && !targettingSkill))) {

            if(canUseQ) {
                Shoot();
                canUseQ = false;
                arrowEllapsedCooldown = 0.0f;
                UIManager.Instance.PutOnCooldown(0, arrowCooldown);
                //de_cancelChannel = true;
                if(!canBeDetected) {
                    EndCat();
                }
            }
        }
        if(targettingSkill && Input.GetMouseButtonDown(0)) {
            UseTargetSkill();
            TurnTargetsOff();
            targettingSkill = false;
            Cursor.SetCursor(normalCursor, Vector2.zero, CursorMode.Auto);
            targetText.gameObject.SetActive(false);
            //de_cancelChannel = true;
            if(!canBeDetected) {
                EndCat();
            }
        }
        if(Input.GetKeyDown(KeyCode.W)) {

            if(canUseW) {
                Dash();
                canUseW = false;
                dashEllapsedCooldown = 0.0f;
                UIManager.Instance.PutOnCooldown(1, dashCooldown);
                //de_cancelChannel = true;
            }
            else {
                UIAudioSource.clip = skillFailSound;
                UIAudioSource.Play();
            }
        }
        if (Input.GetKeyDown(KeyCode.E) || castTargettedE) {

            if(canUseE) {

                if (eTargetType != TargetType.NONE && !smartCast && !castTargettedE) {
                    targettingSkill = true;
                    Cursor.SetCursor(targetCursor, new Vector2(targetCursor.width * 0.5f, targetCursor.height * 0.5f), CursorMode.Auto);
                    targettingSkillButton = 'e';
                    DefineTargetSkillText();
                    TurnTargetsOff();
                    if (eRange > 0) {
                        targettingRangeCircle.transform.localScale = new Vector3(eRange, 0.01f, eRange) * 4.0f;
                        targettingRangeCircle.SetActive(true);
                    }
                }
                else if(useSkillOnE != null) {
                    useSkillOnE();
                }

                castTargettedE = false;
                /*if(isChargingSkill) {
                    canUseE = false;
                }*/

                if (skillWasUsed) {
                    //de_cancelChannel = true;
                    skillWasUsed = false;
                    canUseE = false;
                    eEllapsedCooldown = 0.0f;
                    UIManager.Instance.PutOnCooldown(2, eCooldown);
                    if(!canBeDetected) {
                        EndCat();
                    }
                }
            }
            else {

                UIAudioSource.clip = skillFailSound;
                UIAudioSource.Play();
            }
        }
        if(Input.GetKeyDown(KeyCode.R) || castTargettedR) {

            if(canUseR) {

                if (rTargetType != TargetType.NONE && !smartCast && !castTargettedR) {
                    targettingSkill = true;
                    Cursor.SetCursor(targetCursor, new Vector2(targetCursor.width * 0.5f, targetCursor.height * 0.5f), CursorMode.Auto);
                    targettingSkillButton = 'r';
                    DefineTargetSkillText();
                    TurnTargetsOff();
                    if (rRange > 0) {
                        targettingRangeCircle.transform.localScale = new Vector3(rRange, 0.01f, rRange) * 4.0f;
                        targettingRangeCircle.SetActive(true);
                    }
                }
                else if(useSkillOnR != null) {
                    useSkillOnR();
                }

                castTargettedR = false;
                /*if(isChargingSkill) {
                    canUseR = false;
                }*/

                if (skillWasUsed) {
                    //de_cancelChannel = true;
                    skillWasUsed = false;
                    canUseR = false;
                    rEllapsedCooldown = 0.0f;
                    UIManager.Instance.PutOnCooldown(3, rCooldown);
                    if(!canBeDetected) {
                        EndCat();
                    }
                }
            }
            else {

                UIAudioSource.clip = skillFailSound;
                UIAudioSource.Play();
            }
        }
        if(Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl)) {
            Interact();
            UIManager.Instance.ShowInteractionPopup(false, false);
        }
        if(Input.GetKey(KeyCode.Space)) {
            CameraBehavior.Instance.CentralizePlayer();
        }
        if(Input.GetKeyDown(KeyCode.Tab)) {
            UIManager.Instance.ShowMinimap(true);
        }
        else if(Input.GetKeyUp(KeyCode.Tab)) {
            UIManager.Instance.ShowMinimap(false);
        }
    }

    void ReadTargetMoveInput() {

        if(Input.GetMouseButtonDown(1)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rayInfo;
            if(Physics.Raycast(ray, out rayInfo, 100.0f, waterAndGroundLayer)) {
                moveTargetPoint = rayInfo.point;
                stackedMoveInput = true;
            }
        }
    }

#region FOR TESTING
    void SetNextSkillOnE() {

        if(skillOnE == PlayerSkill.PEACOCK) {
            skillOnE = PlayerSkill.CAT;
        }
        else if(skillOnE == PlayerSkill.CAT) {
            skillOnE = PlayerSkill.BEAR;
        }
        else if(skillOnE == PlayerSkill.BEAR) {
            skillOnE = PlayerSkill.OWL;
        }
        else if(skillOnE == PlayerSkill.OWL) {
            skillOnE = PlayerSkill.WALRUS;
        }
        else if(skillOnE == PlayerSkill.WALRUS) {
            skillOnE = PlayerSkill.PEACOCK;
        }

        DefineSkills();
    }

    void SetNextSkillOnR() {
        
        if(skillOnR == PlayerSkill.PEACOCK) {
            skillOnR = PlayerSkill.CAT;
        }
        else if(skillOnR == PlayerSkill.CAT) {
            skillOnR = PlayerSkill.BEAR;
        }
        else if(skillOnR == PlayerSkill.BEAR) {
            skillOnR = PlayerSkill.OWL;
        }
        else if(skillOnR == PlayerSkill.OWL) {
            skillOnR = PlayerSkill.WALRUS;
        }
        else if(skillOnR == PlayerSkill.WALRUS) {
            skillOnR = PlayerSkill.PEACOCK;
        }

        DefineSkills();
    }
#endregion

    void DefineMoveTargetPoint() {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayInfo;
        if(Physics.Raycast(ray, out rayInfo, 100.0f, waterAndGroundLayer)) {
            transform.LookAt(rayInfo.point);
            transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
            moveTargetPoint = rayInfo.point;
            moving = true;
            animator.SetBool("running", true);
        }
    }
    void Move() {

        Vector3 direction = (moveTargetPoint - transform.position).normalized;
        direction.y = 0;
        velocity = direction * speed;

        if(Vector3.Distance(Utilities.Vector3to2(transform.position), Utilities.Vector3to2(moveTargetPoint)) < 0.6f) {
            Stop();
        }

        thisRigidbody.velocity = new Vector3(velocity.x, thisRigidbody.velocity.y, velocity.z);
        //de_cancelChannel = true;
    }

    void Shoot() {
        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayInfo;
        if(Physics.Raycast(ray, out rayInfo, 100.0f, waterAndGroundLayer | targettingLayer)) {

            Stop();
            animator.SetTrigger("attacked");
            Vector3 lookAtPoint = rayInfo.point;
            lookAtPoint.y = transform.position.y;
            transform.LookAt(lookAtPoint);
            
            bow.GetComponent<MeshRenderer>().material.color = new Color(originalBowColor.r, originalBowColor.g, originalBowColor.b, 0);
            bow.gameObject.SetActive(true);
            System.Action<float> FadeBow = (alpha) => {
                bow.GetComponent<MeshRenderer>().material.color = new Color(originalBowColor.r, originalBowColor.g, originalBowColor.b, alpha);
            };
            if(fadeBowTween != null) {
                fadeBowTween.Stop();
            }
            if(fadeBowTimer != null) {
                fadeBowTimer.Stop();
            }
            new Tween(0.0f, 0.6f, 0.15f, false, EasingType.Linear, FadeBow);
            fadeBowTimer = new Timer(0.6f, () => {
                fadeBowTween = new Tween(0.6f, 0.0f, 0.15f, false, EasingType.Linear, FadeBow);
                fadeBowTween.OnComplete += () => bow.SetActive(false);
            });

            canControl = false;
            Vector3 enemyTarget = rayInfo.collider.transform.position + Vector3.up;

            new Timer(0.4f, () => {

                canControl = true;
                GameObject newArrow = Instantiate(arrow);
                newArrow.transform.position = arrowSpawnReference.position;
                newArrow.transform.eulerAngles += new Vector3(0, transform.eulerAngles.y, 0);
                if(rayInfo.collider.gameObject.layer == Mathf.Log(targettingLayer.value, 2) && !rayInfo.collider.transform.parent.GetComponent<Lion>()) {
                    newArrow.transform.LookAt(enemyTarget);
                    newArrow.transform.eulerAngles += Vector3.right * 90.0f;
                }
                thisAudioSource.clip = attackSounds[Random.Range(0, attackSounds.Length)];
                thisAudioSource.Play();

                /*if (de_stacked >= de_maxStack) {

                    de_stacked = 0.0f;
                    newArrow.transform.localScale *= 2.0f;
                    newArrow.GetComponent<Arrow>().IsSuperArrow = true;
                }*/

                /*if(de_stacks > 0) {

                for(int i = 0; i < de_stacks; i++) {

                    float direction = i%2 == 0 ? -1 : 1;
                    float angle = de_angleForArrows + (de_angleForArrows * i/2);
                    angle *= direction;

                    GameObject angledArrow = Instantiate(arrow);
                    angledArrow.transform.position = arrowSpawnReference.position;
                    angledArrow.transform.eulerAngles += new Vector3(0, transform.eulerAngles.y + angle, 0);
                }

                de_stacks -= de_stacksSpent;
            }*/
            });
        }
    }

    void Dash() {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayInfo;
        if(Physics.Raycast(ray, out rayInfo, 100.0f, waterAndGroundLayer)) {

            Vector3 lookAtPoint = rayInfo.point;
            transform.LookAt(lookAtPoint);
            transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);

            Vector3 direction = (rayInfo.point - transform.position).normalized;
            direction.y = 0;
            velocity = direction * dashSpeed;
            movingBeforeDash = moving;
            moving = false;
            animator.SetTrigger("dashed");
            animator.SetBool("running", false);
            canControl = false;

            Vector3 startPointToMoveDirection = (moveTargetPoint - transform.position).normalized;
            float angle = Vector2.Angle(new Vector2(startPointToMoveDirection.x, startPointToMoveDirection.z), new Vector2(direction.x, direction.z));

            new Timer(dashTime, () => {
                velocity = Vector3.zero;
                canControl = true;

                if(/*(movingBeforeDash && angle < 30.0f) || */stackedMoveInput) {
                    stackedMoveInput = false;
                    animator.SetBool("running", true);
                    lookAtPoint = moveTargetPoint;
                    transform.LookAt(lookAtPoint);
                    transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
                    moving = true;
                }
            });
        }
    }

    void UseSkillPeacock() {

        Stop();
        animator.SetTrigger("peacocked");
        skillWasUsed = true;
        canControl = false;

        float angleOffset = 360 / pe_nFeathers;
        float positionOffset = 2.0f;

        for(int i = 0; i < pe_nFeathers; i++) {

            GameObject feather = Instantiate(pe_feather);
            Vector3 posAux = transform.position;
            posAux.x += (positionOffset * -Mathf.Cos(i * angleOffset * Mathf.Deg2Rad));
            posAux.y += 1.0f;
            posAux.z += (positionOffset * Mathf.Sin(i * angleOffset * Mathf.Deg2Rad));
            feather.transform.position = posAux;
            feather.transform.eulerAngles += new Vector3(0, i*angleOffset, 0);
            feather.GetComponent<MeshRenderer>().material.color = new Color(Random.Range(0.3f, 0.6f), Random.Range(0.0f, 0.2f), Random.Range(0.5f, 0.7f));
            feather.GetComponent<Collider>().enabled = false;
            new Timer(0.5f, () => {
                if(this == null) {
                    return;
                }
                feather.GetComponent<Arrow>().enabled = true;
                feather.GetComponent<Collider>().enabled = true;
                canControl = true;
            });
        }
    }

    void UseSkillCat() {

        Stop();
        skillWasUsed = true;
        new Timer(0.01f, () => {
            animator.SetTrigger("peacocked");
            canBeDetected = false;

            //meshRenderer.material.color -= new Color(0, 0, 0, ca_alphaSubtraction);
            meshRenderer.material = ca_invisibleMaterial;
            speed = ca_speed;
            new Timer(ca_timeInvisible, EndCat);
        });
    }

    void EndCat() {

        //meshRenderer.material.color += new Color(0, 0, 0, ca_alphaSubtraction);
        speed = normalSpeed;
        meshRenderer.material = originalMaterial;
        canBeDetected = true;
    }

    void UseSkillBear() {
        
        skillWasUsed = true;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayInfo;
        if(Physics.Raycast(ray, out rayInfo, 100.0f, waterAndGroundLayer)) {

            Stop();
            thisAudioSource.clip = bearCastSound;
            thisAudioSource.Play();
            animator.SetTrigger("beared");
            Vector3 lookAtPoint = rayInfo.point;
            lookAtPoint.y = transform.position.y;
            transform.LookAt(lookAtPoint);
            Vector3 direction = (rayInfo.point - transform.position).normalized;
            direction.y = 0;

            be_bearWave.transform.position = arrowSpawnReference.position;
            be_bearWave.GetComponent<BearWave>().Reset();
            be_bearWave.SetActive(true);
        }
    }

    void UseSkillWalrus() {
        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayInfo;
        if(Physics.Raycast(ray, out rayInfo, 100.0f, groundLayer) && Vector3.Distance(rayInfo.point, transform.position) <= walrusRange) {

            Stop();
            animator.SetTrigger("owled");
            Vector3 lookAtPoint = rayInfo.point;
            lookAtPoint.y = transform.position.y;
            transform.LookAt(lookAtPoint);
            skillWasUsed = true;
            GameObject freezeExplosion = Instantiate(wa_freezeExplosion);
            freezeExplosion.transform.position = rayInfo.point + new Vector3(0.0f, 2.5f, 0.0f);
        }
        else {

            skillWasUsed = false;
            UIAudioSource.clip = skillFailSound;
            UIAudioSource.Play();
        }
    }

    /*void OwlHeal() {

        HP = Mathf.Min(HP + 1, maxHP);
        UIManager.Instance.ChangeHP(HP);
        ow_healTimer = new Timer(ow_timeToHeal, () => {
            OwlHeal();
        });
    }*/

    public void OwlHeal() {

        HP = Mathf.Min(HP + 1, maxHP);
        UIManager.Instance.ChangeHP(HP);

        OwlAbsorption owlAbsorption = ow_curse.GetComponent<OwlAbsorption>();
        owlAbsorption.AbsorptionComplete.transform.SetParent(ow_curse.transform);
        owlAbsorption.AbsorptionComplete.SetActive(false);
        owlAbsorption.AbsorptionComplete.transform.localPosition = Vector3.zero;
        owlAbsorption.AbsorptionSphere.SetActive(false);
    }

    void UseSkillOwlCurse() {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayInfo;
        if(Physics.Raycast(ray, out rayInfo, 100.0f, targettingLayer) && Vector3.Distance(rayInfo.point, transform.position) <= owlRange) {

            thisAudioSource.clip = owlCastSound;
            thisAudioSource.Play();
            animator.SetTrigger("owled");
            Vector3 lookAtPoint = rayInfo.point;
            lookAtPoint.y = transform.position.y;
            transform.LookAt(lookAtPoint);
            ow_curse.SetActive(true);
            
            if(rayInfo.collider.GetComponent<Enemy>()) {
                ow_curse.transform.SetParent(rayInfo.collider.transform);
                rayInfo.collider.transform.GetComponent<Enemy>().OwlCurseStart(ow_curse, ow_cursedTime);
            }
            else if(rayInfo.collider.transform.parent != null && rayInfo.collider.transform.parent.GetComponent<Enemy>()) {
                ow_curse.transform.SetParent(rayInfo.collider.transform.parent);
                rayInfo.collider.transform.parent.GetComponent<Enemy>().OwlCurseStart(ow_curse, ow_cursedTime);
            }
            else if(rayInfo.collider.transform.parent.GetComponent<Lion>() && !Lion.Instance.HasMane) {
                //ow_curse.transform.SetParent(Lion.Instance.transform);
                Lion.Instance.OwlCurseStart(ow_cursedTime);
            }
            
            skillWasUsed = false;
            if(useSkillOnE == UseSkillOwlCurse) {
                eEllapsedCooldown = 0.0f;
                UIManager.Instance.PutOnCooldown(2, eCooldown);
            }
            else if(useSkillOnR == UseSkillOwlCurse) {
                rEllapsedCooldown = 0.0f;
                UIManager.Instance.PutOnCooldown(3, rCooldown);
            }
            
            /*Tween drainTween = new Tween(rayInfo.point, arrowSpawnReference.position, 0.6f, false, EasingType.EaseIn, (Vector3 value) => {
                de_drain.transform.position = value;
            });

            drainTween.OnComplete += () => {
                de_stacks += de_stacksPerDrain;
                de_stacks = Mathf.Min(de_stacks, de_maxStacks);
                de_drain.SetActive(false);
            };*/

            Stop();
        }
        else {

            skillWasUsed = false;
            UIAudioSource.clip = skillFailSound;
            UIAudioSource.Play();
        }
    }

    /*void UseSkillOwlDrain() {
        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayInfo;
        if(Physics.Raycast(ray, out rayInfo, 100.0f, enemiesLayer) && Vector3.Distance(rayInfo.point, transform.position) <= owlRange) {

            animator.SetTrigger("owled");
            Vector3 lookAtPoint = rayInfo.point;
            lookAtPoint.y = transform.position.y;
            transform.LookAt(lookAtPoint);
            de_drain.SetActive(true);
            de_drain.GetComponent<DeerDrainAnimation>().Target = rayInfo.collider.transform;
            isChargingSkill = true;
            de_cancelChannel = false;

            ow_healTimer = new Timer(ow_timeToHeal, () => {
                OwlHeal();
            });

            if(rayInfo.collider.GetComponent<Enemy>()) {
                de_enemyDraining = rayInfo.collider.gameObject;
                de_enemyDraining.GetComponent<Enemy>().DeerDrainStart(ow_timeToDamage);
            }
            else if(rayInfo.collider.transform.parent != null && rayInfo.collider.transform.parent.GetComponent<Enemy>()) {
                de_enemyDraining = rayInfo.collider.transform.parent.gameObject;
                de_enemyDraining.GetComponent<Enemy>().DeerDrainStart(ow_timeToDamage);
            }
            else if(rayInfo.collider.GetComponent<Lion>() && !Lion.Instance.HasMane) {
                de_enemyDraining = Lion.Instance.gameObject;
                Lion.Instance.OwlDrainStart(ow_timeToDamage);
            }
            
            /*Tween drainTween = new Tween(rayInfo.point, arrowSpawnReference.position, 0.6f, false, EasingType.EaseIn, (Vector3 value) => {
                de_drain.transform.position = value;
            });

            drainTween.OnComplete += () => {
                de_stacks += de_stacksPerDrain;
                de_stacks = Mathf.Min(de_stacks, de_maxStacks);
                de_drain.SetActive(false);
            };*/

            //Stop();
        //}
    //}*/
    //void OwlDraining() {

        /*de_stacked += de_stackingPerSecond * Time.deltaTime;
        if(de_stacked > de_maxStack) {
            de_stacked = de_maxStack;
            bow.SetActive(true);
            bow.GetComponent<MeshRenderer>().material.color = ow_completeChargeColor;
        }*/
        
        /*if(de_enemyDraining.GetComponent<Enemy>() && de_enemyDraining.GetComponent<Enemy>().IsInvincible) {
            de_cancelChannel = true;
        }

        if(de_cancelChannel || (de_enemyDraining != null && de_enemyDraining.GetComponent<Enemy>() && de_enemyDraining.GetComponent<Enemy>().IsDying)) {
            
            ow_healTimer.Stop();
            if (de_enemyDraining != null) {
                if(de_enemyDraining.GetComponent<Enemy>()) {
                    de_enemyDraining.GetComponent<Enemy>().DeerDrainEnd();
                }
                else if(de_enemyDraining.GetComponent<Lion>()) {
                    Lion.Instance.OwlDrainEnd();
                }
            }
            de_cancelChannel = false;
            isChargingSkill = false;
            de_drain.SetActive(false);
            skillWasUsed = false;
            if(useSkillOnE == UseSkillOwl) {
                eEllapsedCooldown = 0.0f;
                UIManager.Instance.PutOnCooldown(2, eCooldown);
            }
            else if(useSkillOnR == UseSkillOwl) {
                rEllapsedCooldown = 0.0f;
                UIManager.Instance.PutOnCooldown(3, rCooldown);
            }
        }
    }*/

    void DefineTargetSkillText() {

        targetText.gameObject.SetActive(true);
        TargetType targetType = (targettingSkillButton == 'e') ? eTargetType : rTargetType;

        if (targetType == TargetType.CIRCLE) {
            targetText.text = "Selecione a área alvo";
        }
        else if(targetType == TargetType.TARGET) {
            targetText.text = "Selecione o inimigo alvo";
        }
        else if(targetType == TargetType.CONE) {
            targetText.text = "Selecione a direção alvo";
        }

        targetText.transform.GetChild(0).GetComponent<Text>().text = targetText.text;
    }

    void TargetSkill() {

        TargetType targetType;
        float range;

        if(targettingSkillButton == 'e') {
            targetType = eTargetType;
            range = eRange;
        }
        else {
            targetType = rTargetType;
            range = rRange;
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayInfo;

        if (targetType == TargetType.CIRCLE) {
            
            if(Physics.Raycast(ray, out rayInfo, 100.0f, groundLayer)) {
                
                if(Vector3.Distance(transform.position, rayInfo.point) > range) {
                    targetCircle.SetActive(false);
                    targetOutOfRange = true;
                }
                else {
                    targetCircle.SetActive(true);
                    targetPoint = rayInfo.point;
                    targetCircle.transform.position = targetPoint;
                    targetOutOfRange = false;
                }
            }
        }
        else if(targetType == TargetType.TARGET) {

            if(Physics.Raycast(ray, out rayInfo, 100.0f, groundLayer)) {

                if(Vector3.Distance(transform.position, rayInfo.point) > range) {
                    targetTarget.SetActive(false);
                    targetOutOfRange = true;
                }
                else {
                    targetTarget.SetActive(true);
                    targetPoint = rayInfo.point;
                    targetTarget.transform.position = targetPoint;
                    targetOutOfRange = false;
                }
            }
        }
        else if(targetType == TargetType.CONE) {

            if(Physics.Raycast(ray, out rayInfo, 100.0f, groundLayer)) {

                targetCone.SetActive(true);
                targetPoint = rayInfo.point;
                targetCone.transform.right = -Utilities.GroundedVector3(targetPoint - transform.position);
                targetOutOfRange = false;
            }
        }
    }
    void UseTargetSkill() {

        TurnTargetsOff();

        if(!targetOutOfRange) {

            if(targettingSkillButton == 'e' && useSkillOnE != null) {
                castTargettedE = true;
            }
            else if(targettingSkillButton == 'r' && useSkillOnR != null) {
                castTargettedR = true;
            }
        }
    }
    void TurnTargetsOff() {

        targettingRangeCircle.SetActive(false);
        targetCircle.SetActive(false);
        targetTarget.SetActive(false);
        targetCone.SetActive(false);
    }

    void TrackCooldowns() {

        arrowEllapsedCooldown += Time.deltaTime;
        dashEllapsedCooldown += Time.deltaTime;
        eEllapsedCooldown += Time.deltaTime;
        rEllapsedCooldown += Time.deltaTime;

        if(arrowEllapsedCooldown > arrowCooldown) {
            canUseQ = true;
        }
        if(dashEllapsedCooldown > dashCooldown) {
            canUseW = true;
        }
        if(eEllapsedCooldown > eCooldown/* && !isChargingSkill*/) {
            canUseE = true;
        }
        if(rEllapsedCooldown > rCooldown/* && !isChargingSkill*/) {
            canUseR = true;
        }
    }

    public void TakeDamage(int damage) {

        if (gameObject.layer == LayerMask.NameToLayer("PlayerNoEnemy"))
            return;

        gameObject.layer = LayerMask.NameToLayer("PlayerNoEnemy");
        if(HP - damage > 0) {
            meshRenderer.material = invincibilityMaterial;
        }
        new Timer(takeDamageInvincibilityTime, () => {
            gameObject.layer = LayerMask.NameToLayer("Player");
            meshRenderer.material = originalMaterial;
        });

        thisAudioSource.clip = takeHitSounds[Random.Range(0, takeHitSounds.Length)];
        thisAudioSource.Play();
        HP = Mathf.Max(HP - damage, 0);
        movingBeforeHit = moving;
        moving = false;
        thisRigidbody.velocity = new Vector3(0.0f, thisRigidbody.velocity.y, 0.0f);
        Stagger();
        GetComponent<WindReceiver>().ResetWindForce();
        UIManager.Instance.ChangeHP(HP);

        if (HP <= 0) {

            Stop();
            GetComponent<Collider>().enabled = false;
            thisRigidbody.useGravity = false;
            isDead = true;
            animator.SetTrigger("dead");
            new Timer(2.5f, () => {
                if (this == null) {
                    return;
                }

                if(GameManager.Instance.transform.childCount > 0) {
                    UnityEngine.SceneManagement.SceneManager.LoadScene("BossFight");
                    return;
                }

                GameManager.Instance.ReturnToCheckpoint(() => {
                    HP = maxHP;
                    UIManager.Instance.ChangeHP(HP);
                    canControl = true;
                    isDead = false;
                    moveTargetPoint = transform.position;
                    GetComponent<Collider>().enabled = true;
                    thisRigidbody.useGravity = true;
                    animator.SetTrigger("backToLife");
                });
            });
        }
        else {
            animator.SetTrigger("tookDamage");
        }
        /*de_cancelChannel = true;
        if(de_drain.activeSelf) {
            OwlDraining();
        }*/
    }

    void Stagger() {

        if(staggered)
            return;

        staggered = true;
        new Timer(timeStaggered, () => {
            if(this == null)
                return;
            staggered = false;
            if(movingBeforeHit) {
                moving = true;
                transform.LookAt(moveTargetPoint);
            }
        });
    }

    void CheckFalling() {

        if (transform.position.y < 0.3f) {

            if (!falling) {
                fallingPosition = transform.position;
                animator.SetTrigger("fell");
                falling = true;
                windReceiver.enabled = false;
                canControl = false;
            }

            ellapsedTimeStuckInFall += Time.deltaTime;

            if (transform.position.y < -8.0f || ellapsedTimeStuckInFall > 2.0f) {

                ellapsedTimeStuckInFall = 0.0f;
                TakeDamage(1);
                if (HP > 0) {
                    //RaycastHit rayInfo;
                    //Vector3 direction = (previousGroundPosition - fallingPosition).normalized;
                    //if(Physics.Raycast(fallingPosition, direction, out rayInfo, 10.0f, groundLayer)) {
                        transform.position = previousGroundPosition;//rayInfo.point + (Vector3.up * 1.5f) + (direction);
                        falling = false;
                        canControl = true;
                        windReceiver.enabled = true;
                        Stop();
                    //}
                }
            }
        }
        else if(falling && !isInCutscene) {
            ellapsedTimeStuckInFall = 0.0f;
            falling = false;
            canControl = true;
            windReceiver.enabled = true;
        }
    }

    void Stop() {

        animator.SetBool("running", false);
        moving = false;
        velocity = Vector3.zero;
    }

    void Interact() {

        Stop();
        if(Enemy.EnemiesDetectedPlayer.Count > 0) {
            return;
        }

        if(canTalk && talkingNPC.CanBeTalkedTo) {
            canControl = false;
            UIManager.Instance.StartConversation(talkingNPC);
            UIManager.Instance.ShowInteractionPopup(false, false);
        }
        else if(canSaveCheckpoint) {
            animator.SetTrigger("owled");
            GameManager.Instance.SaveCheckpoint(checkpointToSave.GetSiblingIndex(), checkpointToSave.position);
            canSaveCheckpoint = false;
            UIManager.Instance.ShowInteractionPopup(false, false);
            HP = maxHP;
            UIManager.Instance.ChangeHP(maxHP);
        }
        /*else if(canTakeOrb) {
            animator.SetTrigger("owled");
            //GameManager.Instance.TakeOrb(contactingOrbIndex);
            UIManager.Instance.PromptPowerChange((PlayerSkill)contactingOrbIndex + 1);
        }*/
        else if(placeToGo != null) {
            GameManager.Instance.EnterAltar(placeToGo);
            CameraBehavior.Instance.CentralizePlayer();
        }
        else if(canBreakBarrier && GameManager.Instance.BarrierDamaged) {
            GameManager.Instance.BreakBarrier();
        }
    }

    public void LoseLion() {
        Destroy(holdingLion);
    }

    public void SavePlayerInfo() {
        PlayerPrefs.SetInt("skillOnE", (int)skillOnE);
        PlayerPrefs.SetInt("skillOnR", (int)skillOnR);
        //PlayerPrefs.SetInt("inAltar", 0); // during TEST
        PlayerPrefs.SetInt("smartCast", (smartCast) ? 1 : 0);
    }

    public void LoadPlayerInfo() {
        skillOnE = (PlayerSkill)PlayerPrefs.GetInt("skillOnE");
        skillOnR = (PlayerSkill)PlayerPrefs.GetInt("skillOnR");
        smartCast = (PlayerPrefs.GetInt("smartCast") == 1);
        DefineSkills();
        UIManager.Instance.ChangeSkills(skillOnE, skillOnR);
    }

    void OnTriggerEnter(Collider other) {

        if(other.tag == "AltarEntrance") {

            placeToGo = other.transform.GetComponent<Passage>().GoToPoint;
        }
        /*else if(other.GetComponent<PowerOrb>()) {

            canTakeOrb = true;
            contactingOrbIndex = other.GetComponent<PowerOrb>().Index;
        }*/
        else if(other.GetComponent<NPC>() && other.GetComponent<NPC>().CanBeTalkedTo) {
            canTalk = true;
            talkingNPC = other.GetComponent<NPC>();
        }
        else if(other.tag == "Checkpoint") {
            checkpointToSave = other.transform;
            canSaveCheckpoint = true;
        }
        else if(other.GetComponent<BarrierAnimation>() && other.GetComponent<BarrierAnimation>().IsDamaged) {
            canBreakBarrier = true;
        }

        if(other.tag == "OwlHeal") {
            OwlHeal();
        }
    }
    void OnTriggerExit(Collider other) {

        if(other.tag == "AltarEntrance") {

            placeToGo = null;
        }
        /*else if(other.GetComponent<PowerOrb>()) {

            canTakeOrb = false;
            contactingOrbIndex = -1;
        }*/
        else if(other.GetComponent<NPC>()) {
            canTalk = false;
            talkingNPC = null;
        }
        else if(other.tag == "Checkpoint") {
            checkpointToSave = null;
            canSaveCheckpoint = false;
        }
        else if(other.GetComponent<BarrierAnimation>()) {
            canBreakBarrier = false;
        }
    }
}
