﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwlFreezeParticles : MonoBehaviour {

	float speed;
    public float Speed { set { speed = value; } }
    public float Lifetime;
    Vector3 originPosition;

    [SerializeField] float outterToInnerSpeed;
    [SerializeField] float innerToOutterSpeed;
    [SerializeField] float angularAcceleration;
    float angularSpeed;
    public bool Expansive;
	
    void Start() {
        Destroy(gameObject, Lifetime);
        angularSpeed = Expansive ? innerToOutterSpeed : outterToInnerSpeed;
        angularAcceleration = Expansive ? -angularAcceleration : angularAcceleration;
    }

	void Update () {

        transform.position += transform.up * speed * Time.deltaTime;
        angularSpeed += angularAcceleration * Time.deltaTime;
        transform.eulerAngles += new Vector3(0, angularSpeed, 0) * Time.deltaTime;
    }
}
