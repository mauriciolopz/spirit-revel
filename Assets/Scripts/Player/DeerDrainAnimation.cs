﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class DeerDrainAnimation : Singleton<DeerDrainAnimation> {

    //public enum AnimationType { LINEAR, CIRCLE, DNA }

    [SerializeField] GameObject particle;
    [SerializeField] float spawnInterval;
    [SerializeField] float speed;
    [SerializeField] float size;
    [SerializeField] float distanceToFade;
    public Transform Target;
    float ellapsedSpawnInterval = 0.0f;
    bool sendIncreasing = true;
    public float Speed { get { return speed; } }
    

	void Update () {

        transform.position = Target.position;

        ellapsedSpawnInterval += Time.deltaTime;
        if(ellapsedSpawnInterval > spawnInterval) {
            GameObject newParticle = Instantiate(particle);
            newParticle.transform.position = transform.position;
            newParticle.transform.localScale *= size;
            Vector3 originalParticleScale = newParticle.transform.localScale;
            newParticle.GetComponent<DeerDrainParticle>().Speed = speed;
            newParticle.GetComponent<DeerDrainParticle>().IncreasingAngularSpeed = sendIncreasing;
            sendIncreasing = !sendIncreasing;
            ellapsedSpawnInterval = 0.0f;

            float distanceToPlayer = Vector3.Distance(transform.position, Player.Instance.transform.position);
            if (distanceToPlayer < distanceToFade) {
                newParticle.GetComponent<DeerDrainParticle>().Speed = speed * distanceToPlayer / distanceToFade;
            }
        }
	}
}
