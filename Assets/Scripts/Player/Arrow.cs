﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class Arrow : MonoBehaviour {

    Vector3 startPosition;
    Rigidbody thisRigidbody;
    [SerializeField] float speed;
    [SerializeField] int damage;
    [SerializeField] float angularSpeed;
    [SerializeField] float range;
    [SerializeField] AudioClip[] hitSounds;
    AudioSource thisAudioSource;
    bool isSuperArrow = false;
    public bool IsSuperArrow { set { isSuperArrow = value; } }

	void Start () {

        startPosition = transform.position;
        thisRigidbody = GetComponent<Rigidbody>();
        thisAudioSource = GetComponent<AudioSource>();
    }
	
	void Update () {

        thisRigidbody.velocity = transform.up * speed;
        transform.eulerAngles += new Vector3(0, angularSpeed, 0) * Time.deltaTime;
        
        if((!isSuperArrow && Vector3.Distance(startPosition, transform.position) > range) || (isSuperArrow && Vector3.Distance(startPosition, transform.position) > range * 2.0f)) {

            Destroy(gameObject);
        }
	}

    void PlayHitSound(Vector3 position) {

        GameObject soundObject = new GameObject();
        soundObject.transform.position = position;
        soundObject.AddComponent<AudioSource>();
        AudioSource audioSource = soundObject.GetComponent<AudioSource>();
        audioSource.clip = hitSounds[Random.Range(0, hitSounds.Length)];
        audioSource.spatialBlend = 1.0f;
        audioSource.rolloffMode = AudioRolloffMode.Linear;
        audioSource.maxDistance = 100.0f;
        audioSource.Play();
        Destroy(soundObject, 0.2f);
    }

    void OnTriggerEnter(Collider other) {

        if (other.GetComponent<Enemy>()) {

            PlayHitSound(other.transform.position);
            if (!isSuperArrow) {
                other.transform.GetComponent<Enemy>().TakeDamage(damage, true);
                if(Mathf.Abs(angularSpeed) < 0.5f) {
                    Destroy(gameObject);
                }
            }
            else {
                other.transform.GetComponent<Enemy>().TakeDamage(damage * 2, true);
            }
        }
        else if (other.GetComponent<Lion>() && !other.GetComponent<Lion>().HasMane) {

            PlayHitSound(other.transform.position);
            if (!isSuperArrow) {
                other.transform.GetComponent<Lion>().TakeDamage(damage);
                if(Mathf.Abs(angularSpeed) < 0.5f) {
                    Destroy(gameObject);
                }
            }
            else {
                other.transform.GetComponent<Lion>().TakeDamage(damage * 2);
            }
        }
        else if(other.GetComponent<Lion>() && other.GetComponent<Lion>().HasMane) {

            PlayHitSound(other.transform.position);
            if (!isSuperArrow && Mathf.Abs(angularSpeed) < 0.5f) {
                Destroy(gameObject);
            }
        }
        else if(other.tag == "Mane" && Mathf.Abs(angularSpeed) < 0.5f) {

            PlayHitSound(other.transform.position);
            other.transform.parent.GetComponent<Lion>().ManeTakeDamage(1);
            Destroy(gameObject);
        }
        else if(other.gameObject.layer == LayerMask.NameToLayer("Wall") && Mathf.Abs(angularSpeed) < 0.5f) {

            PlayHitSound(other.transform.position);
            Destroy(gameObject);
        }
    }
}
