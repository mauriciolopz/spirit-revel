﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearWave : MonoBehaviour {

    public int damage;
    public float speed;
    public float lifetime;
    public float knockbackIntensity;
    public Vector3 sizeSmall;
    public Vector3 growth;
    float ellapsedLifetime = 0.0f;
    Vector3 forward;
    

	public void Reset () {

        forward = transform.up;
        GetComponent<Rigidbody>().velocity = forward * speed;
        transform.localScale = sizeSmall;
    }
	
	void Update () {

        ellapsedLifetime += Time.deltaTime;

        float completionPercent = ellapsedLifetime / lifetime;
        transform.localScale = sizeSmall + (growth * completionPercent);

        if(ellapsedLifetime > lifetime) {

            ellapsedLifetime = 0.0f;
            gameObject.SetActive(false);
        }
	}

    void OnTriggerEnter(Collider other) {

        if(other.GetComponent<Enemy>()) {

            Enemy enemyHit = other.GetComponent<Enemy>();
            enemyHit.Knockback(forward * knockbackIntensity);
            enemyHit.TakeDamage(damage, false);
        }
        else if(other.GetComponent<EnemyProjectile>() || other.GetComponent<SpiritChaseSphere>()) {

            Destroy(other.gameObject);
        }
    }
}
