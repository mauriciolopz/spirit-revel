﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class OwlAbsorption : MonoBehaviour {

    [SerializeField] float speed;
    public GameObject AbsorptionComplete;
    public GameObject AbsorptionSphere;
    bool moveSphere = false;
	
	/*void Update () {
		
        if(moveSphere) {

            AbsorptionComplete.transform.position += (Player.Instance.transform.position - transform.position).normalized * speed * Time.deltaTime;
        }
	}*/

    public void CompleteAbsorption() {

        AbsorptionComplete.transform.SetParent(null);
        AbsorptionComplete.SetActive(true);
        transform.SetParent(Player.Instance.transform);
        transform.localPosition = Vector3.zero;
        gameObject.SetActive(false);
        new Timer(0.25f, () => {
            if(this == null)
                return;
            AbsorptionSphere.SetActive(true);
            //moveSphere = true;
        });

        new Timer(10.0f, () => {
            AbsorptionSphere.SetActive(false);
            AbsorptionComplete.transform.SetParent(transform);
            AbsorptionComplete.SetActive(false);
            AbsorptionComplete.transform.localPosition = Vector3.zero;
        });
    }
}
