﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class CameraBehavior : Singleton<CameraBehavior> {

    public float boundsDistanceToMove;
    public float speed;
    Vector2 playerBounds;
    bool lockCamera = false;
    public bool LockCamera { get { return lockCamera; } set { lockCamera = value; } }
    public bool LockCameraForCutscene;
    const float maxDistanceToPlayer = 20.0f;
    const float maxDistanceToPlayerUp = 15.0f;
    Vector3 viewCenterPoint;
    Vector3 distanceToGround = new Vector3(0, -29.0f, 17.5f);

    void Start () {

        Cursor.lockState = CursorLockMode.Confined;
        playerBounds = new Vector2(200, 300);
	}
	
	void Update () {

        if(Input.GetKeyDown(KeyCode.L)) {
            lockCamera = !lockCamera;
        }

        if(!lockCamera && !LockCameraForCutscene) {
            Move();
        }
        else {
            CentralizePlayer();
        }
	}

    void Move() {
        
        int horizontalMove = (Input.mousePosition.x < boundsDistanceToMove) ? -1 : 
                            ((Input.mousePosition.x > Screen.width - boundsDistanceToMove) ? 1 : 0);

        int verticalMove = (Input.mousePosition.y < boundsDistanceToMove) ? -1 :
                          ((Input.mousePosition.y > Screen.height - boundsDistanceToMove) ? 1 : 0);

        if(horizontalMove != 0 || verticalMove != 0) {

            //Bounds camera to player surroundings (screen based)
            /*Vector2 playerScreenPosition = Camera.main.WorldToScreenPoint(Player.Instance.transform.position);

            if((horizontalMove == -1 && playerScreenPosition.x + playerBounds.x >= Screen.width) || (horizontalMove == 1 && playerScreenPosition.x - playerBounds.x <= 0)) {
                horizontalMove = 0;
            }

            if((verticalMove == -1 && playerScreenPosition.y + playerBounds.y >= Screen.height) || (verticalMove == 1 && playerScreenPosition.y <= 0)) {
                verticalMove = 0;
            }*/
            //-----------------------------------

            // Smooth camera to player surroundings
            viewCenterPoint = transform.position + distanceToGround;

            float speedX = speed;
            if (horizontalMove == -1) {
                speedX = Mathf.Max(-speed, -speed * ((maxDistanceToPlayer + (viewCenterPoint.x - Player.Instance.transform.position.x)) / maxDistanceToPlayer));
            }
            else if (horizontalMove == 1) {
                speedX = Mathf.Min(speed, speed * ((maxDistanceToPlayer - (viewCenterPoint.x - Player.Instance.transform.position.x)) / maxDistanceToPlayer));
            }
            speedX = Mathf.Abs(speedX);

            float speedY = speed;
            if (verticalMove == -1) {
                speedY = Mathf.Max(-speed, -speed * ((maxDistanceToPlayerUp + (viewCenterPoint.z - Player.Instance.transform.position.z)) / maxDistanceToPlayerUp));
            }
            else if (verticalMove == 1) {
                speedY = Mathf.Min(speed, speed * ((maxDistanceToPlayerUp - (viewCenterPoint.z - Player.Instance.transform.position.z)) / maxDistanceToPlayerUp));
            }
            speedY = Mathf.Abs(speedY);

            //Bounds camera to player surroundings (3D based)
            if((horizontalMove == -1 && viewCenterPoint.x - Player.Instance.transform.position.x < -maxDistanceToPlayer) || (horizontalMove == 1 && viewCenterPoint.x - Player.Instance.transform.position.x > maxDistanceToPlayer)) {
                transform.position = new Vector3(((maxDistanceToPlayer + 1) * horizontalMove) + Player.Instance.transform.position.x, transform.position.y, transform.position.z);
                horizontalMove = 0;
            }

            if((verticalMove == -1 && viewCenterPoint.z - Player.Instance.transform.position.z < -maxDistanceToPlayer) || (verticalMove == 1 && viewCenterPoint.z - Player.Instance.transform.position.z > maxDistanceToPlayerUp)) {
                float zPos = (verticalMove == -1) ? (-maxDistanceToPlayer - 1) : (maxDistanceToPlayerUp + 1);
                transform.position = new Vector3(transform.position.x, transform.position.y, zPos + Player.Instance.transform.position.z - distanceToGround.z);
                verticalMove = 0;
            }
            //------------------------------------

            transform.position += new Vector3(horizontalMove * speedX, 0.0f, verticalMove * speedY) * Time.deltaTime;
        }
    }


    public void CentralizePlayer() {

        transform.position = Player.Instance.transform.position - distanceToGround;
    }


    public void FocusOnPoint(Vector3 point) {

        transform.position = point - distanceToGround;
    }
}
