﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeerDrainParticle : MonoBehaviour {

    float speed;
    public float Speed { set { speed = value; } }

    [SerializeField] float maxAngularSpeed;
    [SerializeField] float angularAcceleration;
    float angularSpeed = 0.0f;
    bool increasingAngularSpeed = true;
    public bool IncreasingAngularSpeed { set { increasingAngularSpeed = value; } }
	
    void Start() {
        angularSpeed = 0;
    }

	void Update () {

        transform.position += transform.up * speed * Time.deltaTime;

        if(increasingAngularSpeed) {
            angularSpeed += Random.Range(0, angularAcceleration) * Time.deltaTime;
            if(angularSpeed > maxAngularSpeed) {
                angularSpeed = maxAngularSpeed;
                increasingAngularSpeed = false;
            }
        }
        else {
            angularSpeed -= Random.Range(0, angularAcceleration) * Time.deltaTime;
            if(angularSpeed < -maxAngularSpeed) {
                angularSpeed = -maxAngularSpeed;
                increasingAngularSpeed = true;
            }
        }

        transform.up = Player.Instance.transform.position - transform.position + new Vector3(0.0f, 2.25f, 0.0f);
        transform.eulerAngles += new Vector3(0, angularSpeed, 0);

        /*if(Player.Instance.CanceledDeerChannel) {
            speed = DeerDrainAnimation.Instance.Speed;
        }*/

        if(Vector3.Distance(transform.position, Player.Instance.transform.position) < 3.0f) {
            Destroy(gameObject);
        }
    }
}
