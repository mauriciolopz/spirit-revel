﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class EnemiesGroup : MonoBehaviour {

    bool detectedPlayer;
    Enemy[] enemies;


    void Start() {

        enemies = transform.GetComponentsInChildren<Enemy>(true);
    }

    void DefineInactivesID() {

        for(int i = 0; i < enemies.Length; i++) {
            if(!enemies[i].gameObject.activeInHierarchy) {
                enemies[i].IdInPreset = enemies[i].transform.GetSiblingIndex() + ((transform.GetSiblingIndex() + 1) * 1000);
            }
        }
    }

    public void DetectPlayer() {

        detectedPlayer = true;
        if(enemies == null) {
            return;
        }

        foreach(Enemy e in enemies) {

            if (e == null)
                continue;

            if (!e.gameObject.activeInHierarchy) {
                EnemyAppear(e);
            }
            e.DetectedPlayer = true;
            e.PlayerDetectedByParent = false;
            e.UpdateDetectionList(true);
        }
    }

    void EnemyAppear(Enemy enemy) {

        enemy.gameObject.SetActive(true);
        enemy.enabled = false;
        enemy.OriginalSkin.gameObject.SetActive(false);
        enemy.DeathSkin.gameObject.SetActive(true);
        enemy.DeathSkin.transform.GetChild(0).gameObject.SetActive(false);

        GameObject showUpParticles = Instantiate(GameManager.Instance.EnemiesShowUpParticles);
        showUpParticles.transform.SetParent(enemy.DeathSkin.transform);
        showUpParticles.transform.localPosition = Vector3.zero;
        showUpParticles.transform.localScale = Vector3.one;
        showUpParticles.transform.position += Vector3.up * 8.0f;
        showUpParticles.GetComponent<AudioSource>().clip = GameManager.Instance.EnemiesSpawnSounds[Random.Range(0, 2)];
        showUpParticles.GetComponent<AudioSource>().Play();

        Tween deathTween = new Tween(enemy.DeathSkin.material.color - new Color(0.0f, 0.0f, 0.0f, 1.0f), enemy.DeathSkin.material.color, 1.25f, false, EasingType.Linear, (Color value) => {
            if (this == null)
                return;
            enemy.DeathSkin.material.color = value;
        });

        new Timer(1.25f, () => {
            enemy.enabled = true;
            enemy.OriginalSkin.gameObject.SetActive(true);
            enemy.DeathSkin.transform.GetChild(0).gameObject.SetActive(true);
            enemy.DeathSkin.gameObject.SetActive(false);
            //showUpParticles.transform.SetParent(null);
            //showUpParticles.transform.localScale = Vector3.one;
            Destroy(showUpParticles);
        });
    }

    void OnTriggerStay(Collider other) {
        
        if(other.gameObject.layer == LayerMask.NameToLayer("Player")) {

            if(!detectedPlayer && Player.Instance.CanBeDetected) {

                DetectPlayer();
                Destroy(GetComponent<Collider>());
            }
        }
    }
}
