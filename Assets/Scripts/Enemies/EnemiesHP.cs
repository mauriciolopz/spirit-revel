﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesHP : MonoBehaviour {

    [SerializeField] RectTransform hpBarParent;
    [SerializeField] int hpPerLine;

    Transform enemy;
    Vector3 relativePosition;


    void Update() {

        if(enemy == null && transform.parent != null) {
            enemy = transform.parent;
            relativePosition = transform.position - enemy.position;
            transform.SetParent(null);
        }
        else if(enemy != null) {
            transform.position = enemy.position + relativePosition;
        }
    }
    
	public void DefineHP(int HP) {
        
        Vector2 barDimensions = hpBarParent.parent.GetComponent<RectTransform>().sizeDelta;
        barDimensions.y = barDimensions.y + (int)((HP - 1) * barDimensions.y / hpPerLine);
        hpBarParent.parent.GetComponent<RectTransform>().sizeDelta = barDimensions;

        for(int i = 1; i < HP; i++) {

            GameObject newPiece = Instantiate(hpBarParent.GetChild(0).gameObject);
            newPiece.transform.SetParent(hpBarParent);
            newPiece.GetComponent<RectTransform>().localPosition = new Vector3(newPiece.transform.position.x, newPiece.transform.position.y, 0);
            newPiece.GetComponent<RectTransform>().localEulerAngles = Vector3.zero;
            newPiece.GetComponent<RectTransform>().localScale = Vector3.one;
        }
    }
}
