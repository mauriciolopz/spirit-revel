﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class Armadillo : Enemy {

    [SerializeField] float angularSpeed;
    [SerializeField] float rechargeTime;
    [SerializeField] float minTimeRolling;
    [SerializeField] float maxTimeRolling;
    [SerializeField] AudioClip[] hitSounds;
    float timeRolling;
    float ellapsedTimeRolling = 0.0f;
    const float angleToPlayerSnap = 3.0f;
    bool isChasing = true;
    bool hadDetectedPlayer = false;


    void Start() {

        thisAudioSource.volume = 1.0f;
        thisAudioSource.minDistance = 50.0f;
    }

    protected override void FixedUpdate() {
        
        if(transform.position.y < -5.0f) {
            Die();
        }
        if (transform.position.y < 0.2f) {
            return;
        }
        base.FixedUpdate();
    }

    protected override void ActionPattern() {
        
        if(!hadDetectedPlayer && detectedPlayer) {
            hadDetectedPlayer = true;
            animator.SetTrigger("startedRolling");
            timeRolling = Random.Range(minTimeRolling, maxTimeRolling);
        }

        if(isChasing) {

            Move();
            ellapsedTimeRolling += Time.deltaTime;
            if(ellapsedTimeRolling > timeRolling) {
                ellapsedTimeRolling = 0.0f;
                Recharge();
            }
        }
    }

    protected override void Move() {

        Vector2 directionSelfToPlayer = new Vector2(Player.Instance.transform.position.x - transform.position.x, Player.Instance.transform.position.z - transform.position.z);
        Vector2 lookingDirection = new Vector2(transform.forward.x, transform.forward.z);
        float angleToPlayerVector = Vector2.Angle(directionSelfToPlayer, lookingDirection);
        
        Vector2 armadilloRightDirection = new Vector2(transform.right.x, transform.right.z) ;
        float angleArmadilloRightToPlayer = Vector2.Angle(directionSelfToPlayer, armadilloRightDirection);
        float orientation = (angleArmadilloRightToPlayer > 90) ? -1 : 1;

        if(angleToPlayerVector > angleToPlayerSnap) {

            Vector3 anglesAux = transform.eulerAngles;
            anglesAux.y += angularSpeed * Time.deltaTime * orientation;
            transform.eulerAngles = anglesAux;
        }
        
        Vector3 velocity = transform.forward * speed;
        thisRigidbody.velocity = new Vector3(velocity.x, thisRigidbody.velocity.y, velocity.z);
    }

    void Recharge() {

        isChasing = false;
        animator.SetTrigger("collided");

        new Timer(rechargeTime, () => {
            if (this == null)
                return;
            animator.SetTrigger("startedRolling");
            transform.LookAt(Player.Instance.transform.position);
            new Timer(0.1f, () => {
                if (this == null) return;
                isChasing = true;
            });
        });
    }


    void OnCollisionEnter(Collision other) {

        if(other.gameObject.GetComponent<Player>() && isChasing) {

            thisAudioSource.clip = hitSounds[Random.Range(0, 2)];
            thisAudioSource.Play();
            other.gameObject.GetComponent<Player>().TakeDamage(damage);
            Recharge();
        }
    }
}
