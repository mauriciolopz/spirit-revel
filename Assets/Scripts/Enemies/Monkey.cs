﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class Monkey : Enemy {
    
    public GameObject stone;
    public Transform stoneSpawnReference;
    public LayerMask obstacleLayer;
    public LayerMask waterGroundLayer;

    public float minDistanceToMove;
    public float maxDistanceToMove;
    public float timeToMoveAfterAttack;
    [SerializeField] AudioClip[] attackSounds;
    float timeMovingAfterAttack = 30.0f;
    Vector3 moveTargetPoint;
    const float moveDistanceToObstacles = 1.5f;
    bool moveDefined = false;
    bool inRangeToMove = true;
    bool firstAttacked = false;


    void Start() {

        acting = true;
    }

    protected override void FixedUpdate() {
        
        if(transform.position.y < -5.0f) {
            Die();
        }
        if (transform.position.y < 0.2f) {
            return;
        }
        base.FixedUpdate();

        if(detectedPlayer && !firstAttacked) {
            firstAttacked = true;
            new Timer(0.2f, () => {
                if(this == null) return;
                acting = false;
            });
        }
    }
    
    protected override void ActionPattern() {

        if(timeMovingAfterAttack > timeToMoveAfterAttack) {

            moveDefined = false;
            timeMovingAfterAttack = 0.0f;
            Attack();
        }
        else if(inRangeToMove) {

            if(!moveDefined) {

                DefineMove();
                moveDefined = true;
            }
            Move();
        }
        timeMovingAfterAttack += Time.deltaTime;
    }

    protected override void Move() {
        
        Vector3 direction = (moveTargetPoint - transform.position).normalized;
        direction.y = 0;
        thisRigidbody.velocity = (direction * speed) + new Vector3(0, thisRigidbody.velocity.y, 0);

        if(Vector3.Distance(transform.position, moveTargetPoint) < 0.3f) {
            thisRigidbody.velocity = new Vector3(0, thisRigidbody.velocity.y, 0);
        }
        animator.SetFloat("speed", thisRigidbody.velocity.x + thisRigidbody.velocity.z);
    }

    void DefineMove() {

        Vector3 direction = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        Vector2 playerDirection = new Vector2(Player.Instance.transform.position.x - transform.position.x, Player.Instance.transform.position.z - transform.position.z);
        float angleToPlayerDirection = Vector2.Angle(direction, playerDirection);
        if(angleToPlayerDirection < 45.0f && angleToPlayerDirection > -45.0f) {
            direction = -direction;
        }
        direction = direction.normalized;

        float distanceToMove = Random.Range(minDistanceToMove, maxDistanceToMove);
        RaycastHit hitInfo;

        direction = new Vector3(direction.x, 0, direction.y);

        if(Physics.Raycast(transform.position, direction, out hitInfo, distanceToMove, obstacleLayer)) {
            moveTargetPoint = hitInfo.point - (direction * moveDistanceToObstacles);
        }
        else {
            moveTargetPoint = transform.position + (direction * distanceToMove);
            if(Physics.Raycast(moveTargetPoint + Vector3.up, Vector3.down, out hitInfo, 5.0f, waterGroundLayer) && hitInfo.point.y < -0.25f) {
                moveTargetPoint = transform.position;
            }
        }

        transform.LookAt(moveTargetPoint);
        transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
    }

    protected override void Attack() {

        animator.SetFloat("speed", 0.0f);
        base.Attack();
        inRangeToMove = (Vector3.Distance(transform.position, Player.Instance.transform.position) < distanceToDetectPlayer * 1.5f);

        new Timer(0.35f, () => {
            if (this == null)
                return;
            thisAudioSource.clip = attackSounds[Random.Range(0, attackSounds.Length)];
            thisAudioSource.Play();
            GameObject newStone = Instantiate(stone, stoneSpawnReference.position, stoneSpawnReference.transform.rotation);
        });
    }
}
