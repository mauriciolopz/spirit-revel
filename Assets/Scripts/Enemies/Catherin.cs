﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class Catherin : Enemy {

    [SerializeField] float angleToBeBackAttack;
    [SerializeField] float timeToAttackPlayersBack;
    [SerializeField] float aggressiveRangeToAttack;
    [SerializeField] float aggressiveSpeed;
    [SerializeField] float attackSpeed;
    [SerializeField] float timeToEndAttack;
    [SerializeField] GameObject shield;
    [SerializeField] GameObject attackCollider;
    [SerializeField] LayerMask groundLayer;

    float timeLookingPlayersBack = 0.0f;
    float cautiousRangeToAttack;
    float cautiousSpeed;
    float originalKnockbackRecoverySpeed;
    bool isInAggressiveMode = false;
    bool returnedToCautiousAfterAttack = true;


    void Start() {

        originalKnockbackRecoverySpeed = knockbackRecoverSpeed;
        cautiousRangeToAttack = distanceToAttack;
        cautiousSpeed = speed;
        thisRigidbody.useGravity = false;
        thisRigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        isInvincible = true;
    }

    protected override void FixedUpdate() {

#if (UNITY_EDITOR)
        if (ClickPlace.Instance != null) {
            return;
        }
#endif

        if(transform.position.y < -5.0f) {
            Die();
        }

        base.FixedUpdate();
        
        if(navMeshAgent.enabled && thisRigidbody.useGravity) {
            thisRigidbody.useGravity = false;
        }
        else if(!navMeshAgent.enabled && !thisRigidbody.useGravity) {
            thisRigidbody.useGravity = true;
        }

        if(thisRigidbody.useGravity) {
            if(Physics.Raycast(transform.position + Vector3.up*0.3f, Vector3.down, 3.0f, groundLayer)) {
                thisRigidbody.velocity = new Vector3(thisRigidbody.velocity.x, 0.0f, thisRigidbody.velocity.z);
            }
        }
    }

    protected override void ActionPattern() {

        if(!returnedToCautiousAfterAttack) {
            ChangeToAggressiveMode(false);
            returnedToCautiousAfterAttack = true;
        }

        if (!isInAggressiveMode) {
            DetectPlayersBack();
        }

        base.ActionPattern();
    }

    protected override void Move() {

        if(navMeshAgent.enabled) {

            animator.SetTrigger("detectedPlayer");
            navMeshAgent.speed = speed;
            navMeshAgent.acceleration = 200.0f;
            transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
            navMeshAgent.SetDestination(Player.Instance.transform.position);
            //base.Move();
        }
    }

    protected override void Attack() {

        if(!isInAggressiveMode) {
            ChangeToAggressiveMode(true);
        }
        base.Attack();
        thisRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        navMeshAgent.enabled = false;
        thisRigidbody.velocity += transform.forward * attackSpeed;
        
        new Timer(timeToEndAttack, () => {
            if (this == null) return;
            
            shallReenableNavmesh = Physics.Raycast(transform.position, Vector3.down, 3.0f, groundLayer);

            if(shallReenableNavmesh) {
                if(!beingKnocked) {
                    thisRigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
                }
                thisRigidbody.velocity = new Vector3(0, thisRigidbody.velocity.y, 0);
                navMeshAgent.enabled = true;
            }
        });
        returnedToCautiousAfterAttack = false;
    }

    public override void TakeDamage(int damage, bool staggers) {

        if (!isInAggressiveMode) {
            Evade();
        }
        else {
            base.TakeDamage(damage, staggers);
        }
    }

    void DetectPlayersBack() {

        Vector2 playerForward2D = new Vector2(Player.Instance.transform.forward.x, Player.Instance.transform.forward.z);
        Vector2 selfToPlayerDirection2D = new Vector2(transform.position.x - Player.Instance.transform.position.x, transform.position.z - Player.Instance.transform.position.z);
        
        float angleToBack = Vector2.Angle(playerForward2D, selfToPlayerDirection2D);

        if(angleToBack > angleToBeBackAttack) {
            timeLookingPlayersBack += Time.deltaTime;
            transform.LookAt(Player.Instance.transform.position);
            animator.SetBool("preparing", true);
            speed = 0.0f;
            navMeshAgent.speed = 0.0f;
        }
        else {
            timeLookingPlayersBack = 0.0f;
            animator.SetBool("preparing", false);
            speed = cautiousSpeed;
        }

        if(timeLookingPlayersBack > timeToAttackPlayersBack) {

            animator.SetBool("preparing", false);
            animator.SetTrigger("agressive");
            ChangeToAggressiveMode(true);
        }
    }

    void ChangeToAggressiveMode(bool aggressiveMode) { 

        if(aggressiveMode) {
            
            isInAggressiveMode = true;
            isInvincible = false;
            canBeFrozen = true;
            knockbackRecoverSpeed = originalKnockbackRecoverySpeed;
            speed = aggressiveSpeed;
            distanceToAttack = aggressiveRangeToAttack;
        }
        else {

            animator.SetTrigger("recharged");
            isInAggressiveMode = false;
            isInvincible = true;
            canBeFrozen = false;
            knockbackRecoverSpeed = 9999;
            speed = cautiousSpeed;
            distanceToAttack = cautiousRangeToAttack;
            timeLookingPlayersBack = 0.0f;
        }
    }

    void Evade() {

        shield.SetActive(true);
        new Timer(0.15f, () => {
            if (this == null)
                return;
            shield.SetActive(false);
        });
    }

    /*public override void DeerDrainStart(float timeToTakeDamage) {

        if(isInvincible) {
            Evade();
        }
        base.DeerDrainStart(timeToTakeDamage);
    }*/

    public override void OwlCurseStart(GameObject curse, float curseTime) {

        if(isInvincible) {
            Evade();
        }
        base.OwlCurseStart(curse, curseTime);
    }

    public void ToggleAttackCollider() {
        
        attackCollider.SetActive(!attackCollider.activeInHierarchy);
    }
}
