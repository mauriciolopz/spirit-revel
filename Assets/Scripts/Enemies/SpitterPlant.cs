﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class SpitterPlant : Enemy {
    
    [SerializeField] GameObject spit;
    [SerializeField] Transform spitSpawnReference;
    [SerializeField] GameObject bulb;
    [SerializeField] Material bulbDeathMaterial;

    
    protected override void Attack() {

        // Base attack, just moving head instead of all body
        /*Transform headBone = spitSpawnReference.parent;
        headBone.LookAt(Player.Instance.transform);
        headBone.eulerAngles = new Vector3(headBone.eulerAngles.x, 0.0f, 0.0f);
        acting = true;
        animator.SetTrigger("attacked");

        UpdateAction onFinishAttack = () => {
            if (this == null)
                return;
            acting = false;
        };
        new Timer(attackDelay, onFinishAttack);*/
        //----------------------------
        base.Attack();

        new Timer(0.25f, () => {
            if(this == null) return;
            GameObject newSpit = Instantiate(spit, spitSpawnReference.position, spitSpawnReference.transform.rotation);
        });
    }

    protected override void Die() {

        SkinnedMeshRenderer bulbSkin = bulb.GetComponent<SkinnedMeshRenderer>();
        bulbSkin.material = bulbDeathMaterial;
        Tween deathTween = new Tween(bulbSkin.material.color, bulbSkin.material.color - new Color(0.0f, 0.0f, 0.0f, 1.0f), 2.00f, false, EasingType.Linear, (Color value) => {
            if (this == null)
                return;
            bulbSkin.material.color = value;
        });
        base.Die();
    }
}
