﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using ChronosFramework;

public class Kobold : Enemy {

    [SerializeField] float startAttackDelay;
    [SerializeField] float jumpVelocity;
    [SerializeField] float jumpSpeedDeceleration;
    [SerializeField] GameObject attackCollider;
    [SerializeField] LayerMask groundLayer;
    [SerializeField] bool startsAttacking;
    [SerializeField] AudioClip[] attackSounds;
    bool isMoving;

    Timer attackTimer;
    const float jumpEndTime = 0.4f;
    float originalJumpVelocity;
    float ellapsedTimeJumping = 0.0f;
    bool isJumping = false;


    void Start() {
        
        originalJumpVelocity = jumpVelocity;
        thisRigidbody.useGravity = false;
        thisRigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
    }

    protected override void FixedUpdate() {

#if (UNITY_EDITOR)
        if (ClickPlace.Instance != null) {
            return;
        }
#endif

        isMoving = false;
        
        if(transform.position.y < -5.0f) {
            Die();
        }

        if(startsAttacking && detectedPlayer && !acting) {
            Attack();
            GameManager.Instance.SuicidalKoboldPlatformFall();
        }

        base.FixedUpdate();
        animator.SetBool("moving", isMoving);
        
        if(isJumping) {
            AttackJump();
        }
        
        if(navMeshAgent.enabled && thisRigidbody.useGravity) {
            thisRigidbody.useGravity = false;
            thisRigidbody.velocity = new Vector3(0.0f, 0.0f, 0.0f);
        }
        else if(!navMeshAgent.enabled && !thisRigidbody.useGravity) {
            thisRigidbody.useGravity = true;
        }

        if(thisRigidbody.useGravity) {
            if(Physics.Raycast(transform.position + Vector3.up*0.3f, Vector3.down, 3.0f, groundLayer)) {
                thisRigidbody.velocity = new Vector3(thisRigidbody.velocity.x, 0.0f, thisRigidbody.velocity.z);
            }
        }
    }

    protected override void Attack() {
        
        isMoving = false;
        thisRigidbody.velocity = new Vector3(0, thisRigidbody.velocity.y, 0);
        base.Attack();
        attackTimer = new Timer(startAttackDelay, () => {
            if (this == null)
                return;
            thisAudioSource.clip = attackSounds[Random.Range(0, attackSounds.Length)];
            thisAudioSource.Play();
            isJumping = true;
            thisRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            ellapsedTimeJumping = 0.0f;
            jumpVelocity = originalJumpVelocity;
            navMeshAgent.enabled = false;
        });
    }

    void AttackJump() {

        Vector3 velocity = transform.forward * jumpVelocity;
        thisRigidbody.velocity = new Vector3(velocity.x, thisRigidbody.velocity.y, velocity.z);

        ellapsedTimeJumping += Time.deltaTime;
        if(ellapsedTimeJumping > jumpEndTime) {
            jumpVelocity = Mathf.Max(0.0f, originalJumpVelocity - (jumpSpeedDeceleration * ellapsedTimeJumping));
        }

        if (jumpVelocity < 0.02f) {

            isJumping = false;

            shallReenableNavmesh = Physics.Raycast(transform.position, Vector3.down, 3.0f, groundLayer);

            if(shallReenableNavmesh) {
                if(!beingKnocked) {
                    thisRigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
                }
                thisRigidbody.velocity = new Vector3(0, thisRigidbody.velocity.y, 0);
                navMeshAgent.enabled = true;
            }
        }
    }

    protected override void Move() {
        
        if(navMeshAgent.enabled) {

            navMeshAgent.speed = speed;
            navMeshAgent.acceleration = 200.0f;
            transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
        
            navMeshAgent.SetDestination(Player.Instance.transform.position);
            //base.Move();
            isMoving = true;
        }
    }

    public override void TakeDamage(int damage, bool staggers) {

        animator.SetTrigger("tookDamage");
        if(beingKnocked) {
            if(attackTimer != null) {
                attackTimer.Stop();
            }
            isJumping = false;
            thisRigidbody.velocity = new Vector3(0, thisRigidbody.velocity.y, 0);
        }
        else if(!isJumping) {
            thisRigidbody.velocity = new Vector3(0, thisRigidbody.velocity.y, 0);
        }
        base.TakeDamage(damage, staggers);
    }

    public void ToggleAttackCollider() {
        
        attackCollider.SetActive(!attackCollider.activeInHierarchy);
    }
}
