﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour {

	Rigidbody thisRigidbody;
    public float speed;
    public int damage;
    public float range;
    const float distanceToDisappear = 100.0f;
    Vector3 startingPosition;

	void Start () {

        thisRigidbody = GetComponent<Rigidbody>();
        startingPosition = transform.position;
        transform.eulerAngles = new Vector3(-90.0f, transform.eulerAngles.y, transform.eulerAngles.z);
    }
	
	void Update () {

        thisRigidbody.velocity = -transform.up * speed;

        if (Vector3.Distance(startingPosition, transform.position) > range) {

            Destroy(gameObject);
        }
	}

    void OnTriggerEnter(Collider other) {

        if (other.GetComponent<Player>()) {
            
            other.transform.GetComponent<Player>().TakeDamage(damage);
            if(Player.Instance.Health == 0) {
                Vector3 lookAtPosition = startingPosition;
                lookAtPosition.y = other.transform.position.y;
                other.transform.LookAt(lookAtPosition);
            }
            Destroy(gameObject);
        }
        else if(other.gameObject.layer == LayerMask.NameToLayer("Wall")) {
            Destroy(gameObject);
        }
    }
}
