﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {

    [SerializeField] protected int HP;
    [SerializeField] protected int damage;
    public int Damage { get { return damage; } }
    [SerializeField] protected float speed;
    [SerializeField] protected float distanceToDetectPlayer;
    [SerializeField] protected float distanceToAttack;
    [SerializeField] protected float attackDelay;
    [SerializeField] protected float takeHitStaggerTime;
    [SerializeField] protected float knockbackRecoverSpeed;
    [SerializeField] protected float slowPotency;
    [SerializeField] protected bool hasTrueVision;
    [SerializeField] protected bool canBeFrozen;
    [SerializeField] GameObject freezeBlock;
    [SerializeField] SkinnedMeshRenderer originalSkin;
    [SerializeField] SkinnedMeshRenderer deathSkin;
    [SerializeField] AudioClip[] deathSounds;

    public SkinnedMeshRenderer OriginalSkin { get { return originalSkin; } }
    public SkinnedMeshRenderer DeathSkin { get { return deathSkin; } }

    protected Rigidbody thisRigidbody;
    protected Animator animator;
    protected NavMeshAgent navMeshAgent;
    protected AudioSource thisAudioSource;
    protected float hpCanvasDistance = 4.6f;
    protected bool shallReenableNavmesh = true;
    //protected Vector3 velocity;
    protected bool detectedPlayer = false;
    public bool DetectedPlayer { set { detectedPlayer = value; } }
    protected bool isInvincible = false;
    public bool IsInvincible { get { return isInvincible; } }
    protected bool acting = false;
    bool playerDetectedByParent = false;
    public bool PlayerDetectedByParent { set { playerDetectedByParent = value; } }
    bool staggered = false;
    protected bool beingKnocked = false;
    bool isDying = false;
    public bool IsDying { get { return isDying; } }
    Timer deerDrainDamageTimer;
    Vector3 knockbackVelcoity;
    Timer freezeTimer;
    Canvas hpCanvas;
    Timer hpCanvasTimer;
    GameObject owlCurse;
    bool isCursed = false;
    public int idInPreset;
    public int IdInPreset { get { return idInPreset; } set { IdInPreset = value; } }

    public static List<int> EnemiesDetectedPlayer = new List<int>();


    void Awake () {

#if (UNITY_EDITOR)
        if (ClickPlace.Instance != null) {
            return;
        }
#endif

        thisRigidbody = GetComponent<Rigidbody>();
        thisAudioSource = GetComponent<AudioSource>();
        thisAudioSource.volume = 0.65f;
        animator = GetComponent<Animator>();
        if(GetComponent<NavMeshAgent>()) {
            navMeshAgent = GetComponent<NavMeshAgent>();
        }

        if(transform.parent != null && transform.parent.GetComponent<EnemiesGroup>()) {
            playerDetectedByParent = true;
        }

        DefineID();
        if(slowPotency <= 0.01f) {
            slowPotency = 3.0f;
        }

        //Take off on new level design
        GameObject targettingCollider = Instantiate(GameManager.Instance.EnemiesTargettingCollider, transform.position, transform.rotation);
        targettingCollider.transform.SetParent(transform);
        //----------------------------
    }

    void DefineID() {

        idInPreset = transform.GetSiblingIndex();

        if(transform.parent) {
            if (transform.parent.parent) {
                idInPreset = idInPreset + ((transform.parent.GetSiblingIndex() + 1) * 1000);
            }
        }
    }
	
	protected virtual void FixedUpdate () {

#if (UNITY_EDITOR)
        if (ClickPlace.Instance != null) {
            return;
        }
#endif
        if(transform.position.y < -5.0f) {
            Die();
        }

        if (isDying)
            return;

        DetectPlayer();
		
        if(detectedPlayer && !acting && !staggered && !beingKnocked) {

            ActionPattern();
        }
        else if(beingKnocked) {

            RecoverFromKnockback();
        }
	}

    protected virtual void ActionPattern() {

        if(Vector3.Distance(Player.Instance.transform.position, transform.position) < distanceToAttack) {

            Attack();
        }
        else if(speed > 0.0f) {

            Move();
        }
    }

    void DetectPlayer() {

        if(!playerDetectedByParent && !detectedPlayer && Player.Instance.CanBeDetected && Vector3.Distance(Player.Instance.transform.position, transform.position) < distanceToDetectPlayer) {

            detectedPlayer = true;
            if(transform.parent && transform.parent.GetComponent<EnemiesGroup>()) {
                transform.parent.GetComponent<EnemiesGroup>().DetectPlayer();
            }
            UpdateDetectionList(detectedPlayer);
        }
        else if(!Player.Instance.CanBeDetected && !hasTrueVision) {

            detectedPlayer = false;
            UpdateDetectionList(detectedPlayer);
        }

        if(detectedPlayer && Vector3.Distance(transform.position, Player.Instance.transform.position) > 70.0f) {
            detectedPlayer = false;
            UpdateDetectionList(false);
            UndetectPlayer();
        }
    }

    protected virtual void UndetectPlayer() {}

    protected virtual void Move() {

        transform.LookAt(Player.Instance.transform);
        transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
        Vector3 velocity = speed * (Player.Instance.transform.position - transform.position).normalized;
        thisRigidbody.velocity = new Vector3(velocity.x, thisRigidbody.velocity.y, velocity.z);
    }

    protected virtual void Attack() {

        transform.LookAt(Player.Instance.transform);
        transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
        acting = true;
        animator.SetTrigger("attacked");
        if(navMeshAgent) {
            navMeshAgent.speed = 0.0f;
        }

        UpdateAction onFinishAttack = () => {
            if (this == null)
                return;
            acting = false;
        };
        new Timer(attackDelay, onFinishAttack);
    }

    public virtual void TakeDamage(int damage, bool staggers) {

        if(!detectedPlayer) {
            detectedPlayer = true;
            UpdateDetectionList(detectedPlayer);
            if(transform.parent && transform.parent.GetComponent<EnemiesGroup>()) {
                transform.parent.GetComponent<EnemiesGroup>().DetectPlayer();
            }
        }

        if (!isInvincible) {

            HP -= damage;

            if (HP <= 0) {
                Die();
            }
            else {
                ShowHPCanvas();
                if (staggers) {
                    Stagger(takeHitStaggerTime);
                }
            }
        }
    }

    void ShowHPCanvas() {

        if(hpCanvasTimer != null) {
            hpCanvasTimer.Stop();
            if(hpCanvas != null) {
                Destroy(hpCanvas.gameObject);
            }
        }
        
        hpCanvas = Instantiate(UIManager.Instance.EnemiesHPCanvas);
        hpCanvas.transform.SetParent(transform);
        hpCanvas.transform.localPosition = new Vector3(0.0f, hpCanvasDistance, 0.0f);
        
        hpCanvas.GetComponent<EnemiesHP>().DefineHP(HP);

        hpCanvasTimer = new Timer(1.25f, () => {
            if(this == null) {
                return;
            }
            if(hpCanvas != null) {
                Destroy(hpCanvas.gameObject);
                hpCanvas = null;
            }
        });
    }

    protected virtual void Die() {

        if (isDying)
            return;

        if(isCursed) {
            owlCurse.GetComponent<OwlAbsorption>().CompleteAbsorption();
            OwlCurseEnd();
        }

        GameManager.Instance.AddToDefeatedEnemies(idInPreset);
        UpdateDetectionList(false);
        thisAudioSource.clip = deathSounds[Random.Range(0, deathSounds.Length)];
        thisAudioSource.Play();

        isDying = true;
        thisRigidbody.useGravity = false;
        thisRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        GetComponents<Collider>().ForEach((c) => { Destroy(c); });
        for(int i = 0; i < transform.childCount; i++) {
            if(transform.GetChild(i).GetComponent<Collider>()) {
                Destroy(transform.GetChild(i).GetComponent<Collider>());
            }
        }
        originalSkin.gameObject.SetActive(false);
        deathSkin.gameObject.SetActive(true);

        Tween deathTween = new Tween(deathSkin.material.color, deathSkin.material.color - new Color(0.0f, 0.0f, 0.0f, 1.0f), 1.25f, false, EasingType.Linear, (Color value) => {
            if(this == null)
                return;
            deathSkin.material.color = value;
        });
        deathTween.OnComplete += () => {
            new Timer(1.0f, () => {
                if (this == null)
                    return;
                Destroy(gameObject);
            });
        };
    }

    public void Stagger(float timeStaggered) {

        if(staggered)
            return;

        staggered = true;
        new Timer(timeStaggered, () => {
            if(this == null)
                return;
            staggered = false;
        });
    }

    public void Freeze(float timeFrozen) {

        if (!canBeFrozen)
            return;
        
        freezeBlock.SetActive(true);
        thisRigidbody.velocity = Vector3.zero + new Vector3(0, thisRigidbody.velocity.y, 0);
        animator.speed = 0;
        if(navMeshAgent) {
            navMeshAgent.speed = 0.0f;
        }
        float originalMass = thisRigidbody.mass;
        thisRigidbody.mass = 1000000;
        staggered = true;

        freezeTimer = new Timer(timeFrozen, () => {
            if(this == null)
                return;
            animator.speed = 1;
            thisRigidbody.mass = originalMass;
            staggered = false;
            if(freezeBlock != null) {
                freezeBlock.SetActive(false);
            }
        });
    }

    public void Knockback(Vector3 force) {

        if(knockbackRecoverSpeed > 9000) {
            return;
        }

        beingKnocked = true;
        if(navMeshAgent) {
            navMeshAgent.enabled = false;
            thisRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        }
        thisRigidbody.velocity = force;
        knockbackVelcoity = force;
    }

    void RecoverFromKnockback() {
        
        if(knockbackRecoverSpeed > 9000) {

            beingKnocked = false;
            if (navMeshAgent) {
                navMeshAgent.enabled = true;
                thisRigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
            }
            return;
        }

        float dirX = knockbackVelcoity.x < 0 ? -1 : 1;
        float dirZ = knockbackVelcoity.z < 0 ? -1 : 1;
        Vector3 slowAmount = knockbackVelcoity.normalized * Time.deltaTime * knockbackRecoverSpeed;

        float absKnockbackX = Mathf.Abs(knockbackVelcoity.x);
        float absKnockbackZ = Mathf.Abs(knockbackVelcoity.z);
        float absSlowX = Mathf.Abs(slowAmount.x);
        float absSlowZ = Mathf.Abs(slowAmount.z);
        float x = (absKnockbackX - Mathf.Min(absSlowX, absKnockbackX)) * dirX;
        float z = (absKnockbackZ - Mathf.Min(absSlowZ, absKnockbackZ)) * dirZ;

        knockbackVelcoity = new Vector3(x, thisRigidbody.velocity.y, z);
        thisRigidbody.velocity = knockbackVelcoity;

        if (thisRigidbody.velocity.magnitude < 0.5f) {

            if(navMeshAgent && shallReenableNavmesh) {
                navMeshAgent.enabled = true;
                thisRigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
            }
            beingKnocked = false;
        }
    }

    public void UpdateDetectionList(bool add) {

        if(GetComponent<Blowfish>()) {
            return;
        }

        if(add && !isDying && !EnemiesDetectedPlayer.Contains(idInPreset)) {
            EnemiesDetectedPlayer.Add(idInPreset);
        }
        else if(EnemiesDetectedPlayer.Contains(idInPreset)) {
            EnemiesDetectedPlayer.Remove(idInPreset);
        }
    }

    public virtual void OwlCurseStart(GameObject curse, float curseTime) {

        owlCurse = curse;
        curse.transform.localPosition = Vector3.up;
        //animator.speed = 1.0f / slowPotency;
        isCursed = true;

        detectedPlayer = true;
        if(transform.parent && transform.parent.GetComponent<EnemiesGroup>()) {
            transform.parent.GetComponent<EnemiesGroup>().DetectPlayer();
        }
        UpdateDetectionList(detectedPlayer);
        speed = speed / slowPotency;
        
        new Timer(curseTime, () => {
            if(this == null)
                return;
            OwlCurseEnd();    
        });
    }

    public void OwlCurseEnd() {

        speed = speed * slowPotency;
        //animator.speed = 1.0f;
        isCursed = false;
        owlCurse.SetActive(false);
        owlCurse.transform.SetParent(Player.Instance.transform);
    }

    void OnDestroy() {
        UpdateDetectionList(false);
    }

    /*void SufferOwlDrain(float timeToTakeDamage) {

        if (this == null) return;
        TakeDamage(1, false);
        deerDrainDamageTimer = new Timer(timeToTakeDamage, () => {
            SufferOwlDrain(timeToTakeDamage);
        });
    }

    public virtual void DeerDrainStart(float timeToTakeDamage) {

        deerDrainDamageTimer = new Timer(timeToTakeDamage, () => {
            SufferOwlDrain(timeToTakeDamage);
        });
        detectedPlayer = true;
        if(transform.parent && transform.parent.GetComponent<EnemiesGroup>()) {
            transform.parent.GetComponent<EnemiesGroup>().DetectPlayer();
        }
        UpdateDetectionList(detectedPlayer);
        speed = speed / slowPotency;
    }

    public void DeerDrainEnd() {

        speed = speed * slowPotency;
        deerDrainDamageTimer.Stop();
    }*/
}
