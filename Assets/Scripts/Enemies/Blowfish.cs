﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class Blowfish : Enemy {

    public GameObject vaccumArea;
    public float vaccumTime;
    public float postVaccumRevealedTime;
    public float timeHidden;
    public float transitionTime;
    public float revealToVaccumTime;

    [Header("Sounds")]
    [SerializeField] AudioClip suckSound;
    [SerializeField] AudioClip hideSound;
    [SerializeField] AudioClip revealSound;

    Vector3 revealedPosition;
    Timer startVaccumTimer;
    Timer endVaccumTimer;
    Timer hideTimer;
    Tween revealTween;
    bool waiting = true;
    bool fisrtDetectedPlayer = false;


    void Start() {

#if (UNITY_EDITOR)
        if (ClickPlace.Instance != null) {
            return;
        }
#endif

        transform.position = new Vector3(transform.position.x, -0.3f, transform.position.z);
        Hide();
        revealedPosition = transform.position;
    }

    protected override void ActionPattern() {
        
        if(detectedPlayer) {

            if(!fisrtDetectedPlayer) {
                fisrtDetectedPlayer = true;
                Reveal();
            }
            if (!waiting) {
                Attack();
            }
        }
    }

    protected override void UndetectPlayer() {

        fisrtDetectedPlayer = false;
        acting = false;
        waiting = true;
        vaccumArea.SetActive(false);
        if (revealTween != null) revealTween.Stop();
        if (startVaccumTimer != null) startVaccumTimer.Stop();
        if (hideTimer != null) hideTimer.Stop();
        if (endVaccumTimer != null) endVaccumTimer.Stop();
        animator.SetBool("sucking", false);
        Hide();
    }

    protected override void Attack() {

        transform.LookAt(Player.Instance.transform);
        transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
        acting = true;

        animator.SetBool("sucking", true);
        thisAudioSource.clip = suckSound;
        thisAudioSource.Play();
        vaccumArea.SetActive(true);

        endVaccumTimer = new Timer(vaccumTime, () => {
            if (this == null)
                return;
            acting = false;
            waiting = true;
            vaccumArea.SetActive(false);
            animator.SetBool("sucking", false);
            thisAudioSource.Stop();
            hideTimer = new Timer(postVaccumRevealedTime, Hide);
        });
    }

    public override void TakeDamage(int damage, bool staggers) {

        if(isInvincible) {
            return;
        }
        base.TakeDamage(damage, staggers);
        acting = false;
        waiting = true;
        vaccumArea.SetActive(false);
        if(revealTween != null) revealTween.Stop();
        if(startVaccumTimer != null) startVaccumTimer.Stop();
        if(hideTimer != null) hideTimer.Stop();
        if(endVaccumTimer != null) endVaccumTimer.Stop();
        animator.SetBool("sucking", false);
        Hide();
    }

    void Hide() {

        if (this == null)
            return;

        if(HP > 0) {
            thisAudioSource.clip = hideSound;
            thisAudioSource.Play();
        }
        GetComponent<Collider>().enabled = false;
        isInvincible = true;
        animator.SetBool("hiding", true);
        animator.SetTrigger("hideEnter");
        Vector3 hiddenPosition = new Vector3(transform.position.x, -2.0f, transform.position.z);
        Tween hideTween = new Tween(transform.position, hiddenPosition, transitionTime, false, EasingType.Linear, (position) => {
            if (this == null)
                return;
            transform.position = position;
        });

        if(detectedPlayer) {
            hideTween.OnComplete += () => {
                if (this == null)
                    return;
                new Timer(timeHidden, Reveal);
            };
        }
    }

    void Reveal() {

        if (this == null)
            return;

        thisAudioSource.clip = revealSound;
        thisAudioSource.Play();
        animator.SetBool("hiding", false);
        revealTween = new Tween(transform.position, revealedPosition, transitionTime, false, EasingType.Linear, (position) => {
            if (this == null)
                return;
            transform.position = position;
        });

        revealTween.OnComplete += () => {
            if (this == null)
                return;
            GetComponent<Collider>().enabled = true;
            isInvincible = false;
            
            startVaccumTimer = new Timer(revealToVaccumTime, () => {
                if (this == null)
                    return;

                waiting = false;
            });
        };
    }
}
