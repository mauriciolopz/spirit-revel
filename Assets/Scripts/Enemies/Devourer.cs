﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class Devourer : Enemy {

    [SerializeField] GameObject prop;
    [SerializeField] GameObject attackCollider;
    [SerializeField] float startAttackDelay;
    [SerializeField] float jumpVelocity;
    [SerializeField] float jumpSpeedDeceleration;
    [SerializeField] float jumpEndTime;
    [SerializeField] AudioClip idleSound;
    [SerializeField] AudioClip attackSound;

    float originalJumpVelocity;
    float ellapsedTimeJumping = 0.0f;
    bool isJumping = false;
    bool isRevealed = false;


    void Start() {

        thisAudioSource.volume = 0.35f;
        originalJumpVelocity = jumpVelocity;
    }

    protected override void FixedUpdate() {

        if(transform.position.y < -5.0f) {
            Die();
        }
        if (transform.position.y < 0.2f) {
            return;
        }
        if(isJumping) {
            AttackJump();
        }
        base.FixedUpdate();
    }

    protected override void ActionPattern() {
        
        if(detectedPlayer && !isRevealed) {

            Destroy(prop);
            animator.SetTrigger("detectedPlayer");
            thisAudioSource.clip = idleSound;
            thisAudioSource.Play();
            isRevealed = true;
            transform.LookAt(Player.Instance.transform);
            transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
            acting = true;
            new Timer(attackDelay*0.6f, () => {
                if (this == null)
                    return;
                acting = false;
            });
        }
        else {

            base.ActionPattern();
        }
    }

    protected override void Attack() {

        thisRigidbody.velocity = Vector3.zero;
        base.Attack();
        new Timer(startAttackDelay, () => {
            if (this == null)
                return;
            thisAudioSource.clip = attackSound;
            thisAudioSource.Play();
            isJumping = true;
            ellapsedTimeJumping = 0.0f;
            jumpVelocity = originalJumpVelocity;
        });
    }

    void AttackJump() {

        bool animatingJump = true;
        thisRigidbody.velocity = transform.forward * jumpVelocity;

        ellapsedTimeJumping += Time.deltaTime;
        if(ellapsedTimeJumping > jumpEndTime) {
            jumpVelocity = Mathf.Max(0.0f, originalJumpVelocity - (jumpSpeedDeceleration * ellapsedTimeJumping));
        }

        if(jumpVelocity < 0.02f) {
            isJumping = false;
            animatingJump = false;
            thisRigidbody.velocity = Vector3.zero;
            new Timer(0.3f, () => {
                if (this == null)
                    return;
                transform.LookAt(Player.Instance.transform);
                transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
                thisAudioSource.clip = idleSound;
                thisAudioSource.Play();
            });
        }
        else if(jumpVelocity < originalJumpVelocity / 3.0f) {
            animatingJump = false;
        }

        animator.SetBool("jumping", animatingJump);
    }

    public void ToggleAttackCollider() {
        
        attackCollider.SetActive(!attackCollider.activeInHierarchy);
    }
}
