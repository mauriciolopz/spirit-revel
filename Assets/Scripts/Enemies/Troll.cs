﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class Troll : Enemy {

    [SerializeField] BoxCollider attackCollider;
    [SerializeField] GameObject hammer;
    //[SerializeField] GameObject hammerFallCollider;
    [SerializeField] Material hammerDeathMaterial;
    [SerializeField] LayerMask groundLayer;
    [SerializeField] AudioClip[] hitSounds;
    [SerializeField] AudioClip[] attackSounds;
    bool isMoving = false;

    void Start() {
        
        hpCanvasDistance = 9.0f;
        thisRigidbody.useGravity = false;
        thisRigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        thisAudioSource.volume = 1.0f;
        thisAudioSource.minDistance = 50.0f;
    }

    protected override void Move() {

        navMeshAgent.speed = speed;
        navMeshAgent.acceleration = 200.0f;
        transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
        navMeshAgent.SetDestination(Player.Instance.transform.position);
        //base.Move();
        isMoving = true;
    }

    protected override void Attack() {

        thisAudioSource.clip = attackSounds[Random.Range(0, 2)];
        thisAudioSource.Play();
        isMoving = false;
        base.Attack();
    }

    protected override void FixedUpdate() {

#if (UNITY_EDITOR)
        if (ClickPlace.Instance != null) {
            return;
        }
#endif

        if(transform.position.y < -5.0f) {
            Die();
        }
        if (transform.position.y < 0.2f) {
            return;
        }
        base.FixedUpdate();
        animator.SetBool("isMoving", isMoving);
        
        if(navMeshAgent.enabled && thisRigidbody.useGravity) {
            thisRigidbody.useGravity = false;
        }
        else if(!navMeshAgent.enabled && !thisRigidbody.useGravity) {
            thisRigidbody.useGravity = true;
        }

        if(thisRigidbody.useGravity) {
            if(Physics.Raycast(transform.position + Vector3.up*0.3f, Vector3.down, 3.0f, groundLayer)) {
                thisRigidbody.velocity = new Vector3(thisRigidbody.velocity.x, 0.0f, thisRigidbody.velocity.z);
            }
        }
    }

    protected override void Die() {

        SkinnedMeshRenderer hammerSkin = hammer.GetComponent<SkinnedMeshRenderer>();
        hammerSkin.material = hammerDeathMaterial;
        Tween deathTween = new Tween(hammerSkin.material.color, hammerSkin.material.color - new Color(0.0f, 0.0f, 0.0f, 1.0f), 1.25f, false, EasingType.Linear, (Color value) => {
            if (this == null)
                return;
            hammerSkin.material.color = value;
        });
        Destroy(attackCollider);
        /*hammer.transform.GetComponent<SkinnedMeshRenderer>().rootBone = null;
        hammer.transform.SetParent(hammerFallCollider.transform);
        hammerFallCollider.SetActive(true);
        hammerFallCollider.transform.SetParent(null);*/
        base.Die();
    }

    public void ToggleAttackCollider() {
        if(attackCollider != null) {
            attackCollider.enabled = !attackCollider.enabled;
        }
    }

    public void PlayHammerHitSound() {
        thisAudioSource.clip = hitSounds[Random.Range(0, 2)];
        thisAudioSource.Play();
    }
}
