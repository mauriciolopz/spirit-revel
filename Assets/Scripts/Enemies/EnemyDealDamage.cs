﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDealDamage : MonoBehaviour {
    
    void OnTriggerEnter(Collider other) {

        if(other.GetComponent<Player>()) {
            if(transform.parent && transform.parent.GetComponent<Enemy>()) {
                other.GetComponent<Player>().TakeDamage(transform.parent.GetComponent<Enemy>().Damage);
                    if(Player.Instance.Health == 0) {
                    Vector3 lookAtPosition = transform.parent.position;
                    lookAtPosition.y = other.transform.position.y;
                    other.transform.LookAt(lookAtPosition);
                }
            }
            else {
                other.GetComponent<Player>().TakeDamage(1);
            }
        }
    }
}
