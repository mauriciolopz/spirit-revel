﻿#if (UNITY_EDITOR)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using ChronosFramework;

public class ClickPlace : Singleton<ClickPlace> {

    enum EditingObject { ENEMY, PROPS }

    [Header("Placer")]
    public GameObject enemiesParent;
    public GameObject enemiesGroup;
    public GameObject groupIndicator;
    public GameObject belongingToGroupIndicator;
    public GameObject[] enemies;
    public GameObject[] phantomEnemies;
    public GameObject propsParent;
    public GameObject[] props;
    public GameObject[] phantomProps;
    public LayerMask waterAndGroundLayer;
    public Camera mainCamera;
    GameObject currentGroup;
    Color currentGroupColor;
    List<GameObject> enemiesPlaced = new List<GameObject>();
    List<GameObject> propsPlaced = new List<GameObject>();
    float rotationToPlace = 0.0f;
    int selectedIndex = 0;
    int maxIndex;
    bool makingGroup = false;
    bool placingGroup = false;
    bool savingPrompt = false;
    bool spawnsTurnedOff = false;
    EditingObject editingObject = EditingObject.ENEMY;

    [Header("UI")]
    public GameObject savePopup;
    public InputField prefabNameInput;
    public GameObject spawnTurnedOffFeedback;
    public GameObject makingGroupFeedback;


    void Update () {

        PreparePlacement();
        ReadInputs();
        if(editingObject == EditingObject.ENEMY) {
            phantomEnemies[selectedIndex].transform.eulerAngles = new Vector3(0.0f, rotationToPlace, 0.0f);
        }
        else if(editingObject == EditingObject.PROPS) {
            phantomProps[selectedIndex].transform.eulerAngles = new Vector3(0.0f, rotationToPlace, 0.0f);
        }
    }

    void PreparePlacement() {

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayHit;
        if (Physics.Raycast(ray, out rayHit, 300.0f, waterAndGroundLayer)) {
            if(editingObject == EditingObject.ENEMY) {
                phantomEnemies[selectedIndex].transform.position = rayHit.point;
            }
            else if(editingObject == EditingObject.PROPS) {
                phantomProps[selectedIndex].transform.position = rayHit.point;
            }
            groupIndicator.transform.position = rayHit.point;
        }

        if (Input.GetMouseButtonDown(0) && !savingPrompt) {

            if(editingObject == EditingObject.ENEMY) {
                ManageEnemyPlacement();
            }
            else if(editingObject == EditingObject.PROPS) {
                ManagePropPlacement();
            }
        }
    }

    void ManageEnemyPlacement() {

        if(!placingGroup) {

            GameObject enemyInstantiated = Instantiate(enemies[selectedIndex]);
            enemiesPlaced.Add(enemyInstantiated);
            enemyInstantiated.transform.position = phantomEnemies[selectedIndex].transform.position;
            enemyInstantiated.transform.eulerAngles = phantomEnemies[selectedIndex].transform.eulerAngles;

            if (!makingGroup) {
                enemyInstantiated.transform.SetParent(enemiesParent.transform);
            }
            else {
                enemyInstantiated.transform.SetParent(currentGroup.transform);
                GameObject belongingInstance = Instantiate(belongingToGroupIndicator);
                belongingInstance.transform.position = enemyInstantiated.transform.position + (Vector3.up * 10.0f);
                belongingInstance.GetComponent<MeshRenderer>().material.color = currentGroupColor;
                enemyInstantiated.SetActive(!spawnsTurnedOff);
            }
        }
        else {

            currentGroup = Instantiate(enemiesGroup);
            currentGroup.transform.position = groupIndicator.transform.position;
            currentGroup.transform.SetParent(enemiesParent.transform);
            placingGroup = false;
            groupIndicator.SetActive(false);
            phantomEnemies[selectedIndex].SetActive(true);
        }
    }

    void ManagePropPlacement() {

        GameObject propInstantiated = Instantiate(props[selectedIndex]);
        propsPlaced.Add(propInstantiated);
        propInstantiated.transform.position = phantomProps[selectedIndex].transform.position;
        propInstantiated.transform.eulerAngles = phantomProps[selectedIndex].transform.eulerAngles;
        propInstantiated.transform.SetParent(propsParent.transform);
    }

    void ReadInputs() {

        if(Input.GetKeyDown(KeyCode.A)) {
            if(editingObject == EditingObject.ENEMY) {
                phantomEnemies[selectedIndex].SetActive(false);
            }
            else if(editingObject == EditingObject.PROPS) {
                phantomProps[selectedIndex].SetActive(false);
            }
            selectedIndex--;
            if(selectedIndex < 0) {
                selectedIndex = maxIndex;
            }
            if(editingObject == EditingObject.ENEMY) {
                phantomEnemies[selectedIndex].SetActive(true);
            }
            else if(editingObject == EditingObject.PROPS) {
                phantomProps[selectedIndex].SetActive(true);
            }
        }

        if (Input.GetKeyDown(KeyCode.D)) {
            if(editingObject == EditingObject.ENEMY) {
                phantomEnemies[selectedIndex].SetActive(false);
            }
            else if(editingObject == EditingObject.PROPS) {
                phantomProps[selectedIndex].SetActive(false);
            }
            selectedIndex++;
            if(selectedIndex > maxIndex) {
                selectedIndex = 0;
            }
            if(editingObject == EditingObject.ENEMY) {
                phantomEnemies[selectedIndex].SetActive(true);
            }
            else if(editingObject == EditingObject.PROPS) {
                phantomProps[selectedIndex].SetActive(true);
            }
        }

        if(Input.GetKeyDown(KeyCode.S)) {
            savePopup.SetActive(true);
            savingPrompt = true;
        }

        if(Input.GetKeyDown(KeyCode.G)) {
            makingGroup = !makingGroup;
            makingGroupFeedback.SetActive(makingGroup);
            placingGroup = makingGroup;
            phantomEnemies[selectedIndex].SetActive(!makingGroup);
            groupIndicator.SetActive(makingGroup);
            if(makingGroup) {
                currentGroupColor = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
            }
            else if(currentGroup.transform.childCount == 0) {
                Destroy(currentGroup);
            }
        }

        if(Input.GetKeyDown(KeyCode.L)) {
            CreatorCamera.Instance.cameraLocked = !CreatorCamera.Instance.cameraLocked;
        }

        if(Input.GetKeyDown(KeyCode.Z)) {
            if(editingObject == EditingObject.ENEMY && enemiesPlaced.Count > 0) {
                Destroy(enemiesPlaced[enemiesPlaced.Count - 1]);
                enemiesPlaced.RemoveAt(enemiesPlaced.Count-1);
            }
            else if(editingObject == EditingObject.PROPS && propsPlaced.Count > 0) {
                Destroy(propsPlaced[propsPlaced.Count - 1]);
                propsPlaced.RemoveAt(propsPlaced.Count - 1);
            }
        }

        if(Input.GetKeyDown(KeyCode.X)) {
            spawnsTurnedOff = !spawnsTurnedOff;
            spawnTurnedOffFeedback.SetActive(spawnsTurnedOff);
        }

        if(Input.GetKey(KeyCode.Q)) {
            rotationToPlace -= 3.0f;
            if(rotationToPlace < 0.0f) {
                rotationToPlace = 360.0f;
            }
        }
        if(Input.GetKey(KeyCode.E)) {
            rotationToPlace += 3.0f;
            if(rotationToPlace > 360.0f) {
                rotationToPlace = 0.0f;
            }
        }

        if(Input.GetKeyDown(KeyCode.Alpha1)) {
            editingObject = EditingObject.ENEMY;
            phantomProps[selectedIndex].SetActive(false);
            selectedIndex = 0;
            phantomEnemies[selectedIndex].SetActive(true);
            maxIndex = enemies.Length-1;
        }
        if(Input.GetKeyDown(KeyCode.Alpha2)) {
            editingObject = EditingObject.PROPS;
            phantomEnemies[selectedIndex].SetActive(false);
            selectedIndex = 0;
            phantomProps[selectedIndex].SetActive(true);
            maxIndex = props.Length-1;
        }
    }

    #region UI

    public void CancelSave() {

        savePopup.SetActive(false);
        savingPrompt = false;
    }

    public void ConfirmSave() {
        
        string localPath;
        string propsLocalPath;
        if(prefabNameInput.text != "") {
            localPath = "Assets/Test/" + prefabNameInput.text + ".prefab";
            propsLocalPath = "Assets/Test/" + prefabNameInput.text + "(props).prefab";
        }
        else {
            localPath = "Assets/Test/NewEnemiesPreset.prefab";
            propsLocalPath = "Assets/Test/NewPropsPreset.prefab";
        }
        if(enemiesParent.transform.childCount > 0) {
            Object prefab = PrefabUtility.CreateEmptyPrefab(localPath);
            PrefabUtility.ReplacePrefab(enemiesParent, prefab, ReplacePrefabOptions.ConnectToPrefab);
        }
        if(propsParent.transform.childCount > 0) {
            Object propsPrefab = PrefabUtility.CreateEmptyPrefab(propsLocalPath);
            PrefabUtility.ReplacePrefab(propsParent, propsPrefab, ReplacePrefabOptions.ConnectToPrefab);
        }
        savePopup.SetActive(false);
        savingPrompt = false;
    }

    #endregion
}
#endif