﻿#if (UNITY_EDITOR)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPlaceholder : MonoBehaviour {

    public enum EnemyType { KOBOLD, MONKEY, BLOWFISH, DEVOURER, ARMADILLO, SPITTER, TROLL, CATHERIN }

    [SerializeField] EnemyType enemy;

    public void InstantiateEnemy () {

        GameObject newEnemy = Instantiate(EnemiesPlaceholderController.Instance.enemiesPrefabs[(int)enemy]);
        newEnemy.transform.parent = transform.parent;
        newEnemy.transform.position = transform.position;
        newEnemy.transform.eulerAngles = transform.eulerAngles;
        if(EnemiesPlaceholderController.Instance.createNewPlaceholders) {
            newEnemy.AddComponent<EnemyPlaceholder>();
            newEnemy.GetComponent<EnemyPlaceholder>().enemy = enemy;
        }
        newEnemy.SetActive(!(name.Length > 6 && name.Substring(0, 6) == "(Hide)"));
        Destroy(gameObject);
	}
}
#endif
