﻿#if (UNITY_EDITOR)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class CreatorCamera : Singleton<CreatorCamera> {

	public float boundsDistanceToMove;
    public float baseSpeed;
    float speed;
    public float zoomSpeed;
    public bool cameraLocked;
    Vector3 viewCenterPoint;
    Vector3 distanceToGround = new Vector3(0, -106.0f, 58.0f);
    
	
	void Update () {
        
        if(!cameraLocked) {
            Move();
        }
        Zoom();
	}

    void Move() {
        
        int horizontalMove = (Input.mousePosition.x < boundsDistanceToMove) ? -1 : 
                            ((Input.mousePosition.x > Screen.width - boundsDistanceToMove) ? 1 : 0);

        int verticalMove = (Input.mousePosition.y < boundsDistanceToMove) ? -1 :
                          ((Input.mousePosition.y > Screen.height - boundsDistanceToMove) ? 1 : 0);

        if(horizontalMove != 0 || verticalMove != 0) {

            transform.position += new Vector3(horizontalMove, 0.0f, verticalMove) * speed * Time.deltaTime;
        }
    }

    void Zoom() {

        if (Input.GetAxis("Mouse ScrollWheel") < 0) {
            GetComponent<Camera>().orthographicSize += zoomSpeed;
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0) {
            GetComponent<Camera>().orthographicSize -= zoomSpeed;
        }

        speed = baseSpeed * GetComponent<Camera>().orthographicSize * 0.1f;
    }


    public void FocusOnPoint(Vector3 point) {

        transform.position = point - distanceToGround;
    }
}
#endif
