﻿#if (UNITY_EDITOR)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;
using UnityEditor;

public class EnemiesPlaceholderController : Singleton<EnemiesPlaceholderController> {

    [SerializeField] GameObject enemiesParent;
    [SerializeField] GameObject[] enemiesParents;
    public GameObject[] enemiesPrefabs;
    public bool editAll;
    public bool createNewPlaceholders;
    int presetIndex = 0;

    void Start() {

        if(!editAll) {
            foreach(EnemyPlaceholder ep in enemiesParent.transform.GetComponentsInChildren<EnemyPlaceholder>()) {
                ep.InstantiateEnemy();
            }

            new Timer(2.0f, OnEndInstantiating);
        }
        else {
            enemiesParents[presetIndex].SetActive(true);
            foreach(EnemyPlaceholder ep in enemiesParents[presetIndex].transform.GetComponentsInChildren<EnemyPlaceholder>()) {
                ep.InstantiateEnemy();
            }
            new Timer(2.0f, OnEndInstantiating);
        }
    }

    void OnEndInstantiating() {

        string localPath;

        if(editAll) {
            localPath = "Assets/Prefabs/Stage/EnemiesStage" + presetIndex + ".prefab";
            Object prefab = PrefabUtility.CreateEmptyPrefab(localPath);
            PrefabUtility.ReplacePrefab(enemiesParents[presetIndex], prefab, ReplacePrefabOptions.ConnectToPrefab);
            Destroy(enemiesParents[presetIndex]);
            presetIndex++;
            if(presetIndex == enemiesParents.Length) {
                Debug.Break();
            }
            else {
                enemiesParents[presetIndex].SetActive(true);
                foreach(EnemyPlaceholder ep in enemiesParents[presetIndex].transform.GetComponentsInChildren<EnemyPlaceholder>()) {
                    ep.InstantiateEnemy();
                }
                new Timer(2.0f, OnEndInstantiating);
            }
        }
        else {
            localPath = "Assets/Test/NewEnemiesPreset.prefab";
            Object prefab = PrefabUtility.CreateEmptyPrefab(localPath);
            PrefabUtility.ReplacePrefab(enemiesParent, prefab, ReplacePrefabOptions.ConnectToPrefab);
            Debug.Break();
        }
    }
}
#endif
