﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTriggeredEvents : MonoBehaviour {

    enum Function { PLAYER_CONTROL, DEACTIVATE_NPC, ALPHA_FADE_IN, ALPHA_FADE_OUT, SHOW_PLAYER_UI, STEAL_LION, START_TALK, DESTROY_CS_BARRIER, START_LION, DESTROY_PARENT=1000  }

    [SerializeField] NPC associatedNPC;
    [SerializeField] List<Function> functionsToTrigger;
    int currentIndex = 0;

    void OnEnable() {

        functionsToTrigger.Sort();

        if(functionsToTrigger[currentIndex] == Function.PLAYER_CONTROL) {
            ReturnControlToPlayer();
            NextIndex();
        }
        if(functionsToTrigger[currentIndex] == Function.DEACTIVATE_NPC) {
            DeactivateNPC();
            NextIndex();
        }
        if(functionsToTrigger[currentIndex] == Function.ALPHA_FADE_IN) {
            AlphaFade(true);
            NextIndex();
        }
        if(functionsToTrigger[currentIndex] == Function.ALPHA_FADE_IN) {
            AlphaFade(false);
            NextIndex();
        }
        if(functionsToTrigger[currentIndex] == Function.SHOW_PLAYER_UI) {
            ShowPlayerUI();
            NextIndex();
        }
        if(functionsToTrigger[currentIndex] == Function.STEAL_LION) {
            StealLion();
            NextIndex();
        }
        if(functionsToTrigger[currentIndex] == Function.START_TALK) {
            StartTalk();
            NextIndex();
        }
        if(functionsToTrigger[currentIndex] == Function.DESTROY_CS_BARRIER) {
            DestroyCutsceneBarrier();
            NextIndex();
        }
        if(functionsToTrigger[currentIndex] == Function.START_LION) {
            StartLionBossFight();
            NextIndex();
        }

        // Needs to be last
        if(functionsToTrigger[currentIndex] == Function.DESTROY_PARENT) {
            DestroyParent();
            NextIndex();
        }
    }

    void ReturnControlToPlayer() {
        Player.Instance.CanControl = true;
        CameraBehavior.Instance.LockCameraForCutscene = false;
        UIManager.Instance.PlayerResourcesParent.SetActive(true);
    }

    void DeactivateNPC() {
        associatedNPC.CanBeTalkedTo = false;
    }

    void AlphaFade(bool fadeIn) {
        UIManager.Instance.AlphaMaskFullFade(fadeIn);
    }

    void ShowPlayerUI() {
        UIManager.Instance.PlayerResourcesParent.SetActive(true);
    }

    void StealLion() {
        GameManager.Instance.ShowOwlsLion(true);
        Player.Instance.LoseLion();
    }

    void StartTalk() {
        UIManager.Instance.StartConversation(associatedNPC);
    }

    void DestroyCutsceneBarrier() {
        Player.Instance.GetComponent<Animator>().applyRootMotion = true;
        Player.Instance.GetComponent<Animator>().applyRootMotion = false;
        Destroy(UIManager.Instance.WitchsLairCsBarrier);
    }

    void StartLionBossFight() {
        Lion.Instance.IsActive = true;
        CameraBehavior.Instance.CentralizePlayer();
        GameManager.Instance.DestroyLionCsObjects();
    }

    void DestroyParent() {
        Destroy(transform.parent.gameObject);
    }

    void NextIndex() {
        if(currentIndex < functionsToTrigger.Count-1) {
            currentIndex++;
        }
        else {
            currentIndex = 0;
        }
    }
}