﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class WindReceiver : MonoBehaviour {

    [SerializeField] float windSensibility;
    [SerializeField] bool objectMoves;
    Vector3 windForce;
    public Vector3 WindForce { get { return windForce; } }
    bool isBeingDragged = false;
    public bool IsBeingDragged { get { return isBeingDragged; } }

    Dictionary<Windzone, Vector3> actingWindForces;


    void Start() {

        actingWindForces = new Dictionary<Windzone, Vector3>();
    }


    void Update() {

        if(actingWindForces.Count > 0) {
            CheckTriggerTurnedOff();
            ApplyResultingWindForce();
        }
    }


    void ApplyResultingWindForce() {

        windForce = Vector3.zero;

        foreach(KeyValuePair<Windzone, Vector3> pair in actingWindForces) {
            
            windForce += pair.Value * windSensibility;
        }
    }


    void CheckTriggerTurnedOff() {

        List<Windzone> keysToDelete = new List<Windzone>();

        foreach(KeyValuePair<Windzone, Vector3> pair in actingWindForces) {

            if(!pair.Key.gameObject || !pair.Key.gameObject.activeSelf) {

                keysToDelete.Add(pair.Key);
            }
        }

        foreach(Windzone w in keysToDelete) {

            actingWindForces.Remove(w);
            
            if(actingWindForces.Count == 0) {

                windForce = Vector3.zero;
                isBeingDragged = false;
            }
        }
    }

    public void ResetWindForce() {

        isBeingDragged = false;
        actingWindForces = new Dictionary<Windzone, Vector3>();
        windForce = Vector3.zero;
    }

    void OnTriggerStay(Collider other) {

        if(other.GetComponent<Windzone>()) {

            Windzone contactWindzone = other.GetComponent<Windzone>();

            if(!actingWindForces.ContainsKey(contactWindzone)) {
                actingWindForces.Add(contactWindzone, contactWindzone.Displacement);
            }
            else {
                actingWindForces[contactWindzone] = contactWindzone.Displacement;
            }

            isBeingDragged = true;
        }
    }


    void OnTriggerExit(Collider other) {

        if(other.GetComponent<Windzone>()) {

            Windzone contactWindzone = other.GetComponent<Windzone>();
            if(actingWindForces.ContainsKey(contactWindzone)) {
                actingWindForces.Remove(contactWindzone);
            }

            if(actingWindForces.Count == 0) {

                windForce = Vector3.zero;
                isBeingDragged = false;
            }
        }
    }
}
