﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class MapTransformator : Singleton<MapTransformator> {

    [SerializeField] GameObject[] parts;
    [SerializeField] GameObject[] stageEnemiesPresets;
    [SerializeField] GameObject[] stagePropsPresets;
    GameObject currentEnemiesPreset;
    GameObject currentPropsPreset;
    bool[] initialActiveParts;
    int[] lastPartForLevel = { -1, 4, 8, 12, 13, 16, 20 };
    

    public void TransformMap(int stage, out Transform enemiesParent, bool backwards = false) {

        if(initialActiveParts == null) {
            initialActiveParts = new bool[parts.Length];
            for(int i = 0; i < initialActiveParts.Length; i++) {
                initialActiveParts[i] = parts[i].activeInHierarchy;
            }
        }

        Destroy(currentEnemiesPreset);
        currentEnemiesPreset = Instantiate(stageEnemiesPresets[stage]);
        enemiesParent = currentEnemiesPreset.transform;
        Destroy(currentPropsPreset);
        currentPropsPreset = Instantiate(stagePropsPresets[stage]);

        int lastIndex = lastPartForLevel[stage];

        for(int i = 0; i <= lastIndex; i++) {
            
            parts[i].SetActive(!initialActiveParts[i]);
            UIManager.Instance.ShowMinimapPiece(i, parts[i].activeInHierarchy);
        }

        if(backwards) {

            for(int i = lastIndex; i < parts.Length; i++) {

                parts[i].SetActive(initialActiveParts[i]);
                UIManager.Instance.ShowMinimapPiece(i, parts[i].activeInHierarchy);
            }
        }
    }
}
