﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using ChronosFramework;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    [SerializeField] CanvasGroup menuGroup;
    [SerializeField] Button loadBtn;
    [SerializeField] GameObject alphaMask;
    [SerializeField] MainMenuSphere[] entities;


    void Start() {
        
        if(!PlayerPrefs.HasKey("currentStage")) {
            loadBtn.enabled = false;
        }
    }

    Tween Fade() {

        /*alphaMask.SetActive(true);
        System.Action<Color> FadeColor = (color) => {
            alphaMask.GetComponent<Image>().color = color;
        };
        return new Tween(new Color(0, 0, 0, 0), new Color(0, 0, 0, 1), 0.8f, false, EasingType.Linear, FadeColor);*/
        System.Action<float> FadeColor = (alpha) => {
            menuGroup.alpha = alpha;
        };
        return new Tween(1.0f, 0.0f, 0.8f, false, EasingType.Linear, FadeColor);
    }

	public void GoToScene(string sceneName) {

        GameLoader.Instance.IsUsed = false;
        Tween alphaTween = Fade();
        alphaTween.OnComplete += () => {
            StartCoroutine(CallLoadingScreen(sceneName));
        };
    }

    public void LoadGame() {

        GameLoader.Instance.IsUsed = true;
        Tween alphaTween = Fade();
        alphaTween.OnComplete += () => {
            StartCoroutine(CallLoadingScreen("Main"));
        };
    }

    public void ExitGame() {

        Application.Quit();
    }

    IEnumerator CallLoadingScreen(string sceneName) {
        
        AsyncOperation sceneLoading = SceneManager.LoadSceneAsync(sceneName);

        while(!sceneLoading.isDone) {

            for(int i = 0; i < entities.Length; i++) {
                entities[i].Orbit();
            }

            yield return null;
        }
    }
}
