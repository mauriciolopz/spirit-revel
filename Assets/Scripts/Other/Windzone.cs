﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Windzone : MonoBehaviour {

    public Transform emitter;
    public float intensity;
    public bool drawsIn;

    Vector3 displaceDirection;
    public Vector3 Displacement { get { return displaceDirection * intensity; } }

	void OnTriggerStay(Collider other) {

        if(other.GetComponent<WindReceiver>()) {

            displaceDirection = (other.transform.position - emitter.position).normalized;
            displaceDirection.y = 0;

            if (drawsIn) {
                displaceDirection = displaceDirection * -1;
            }
        }
    }
}
