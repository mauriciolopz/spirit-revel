﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FallingPiece : MonoBehaviour {

	[SerializeField] GameObject navMeshWalls;

    void OnEnable() {

        navMeshWalls.SetActive(false);
    }

    void OnDisable() {

        navMeshWalls.SetActive(true);
    }
}
