﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;
using UnityEngine.Timeline;
using System.Linq;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager> {

    [Header("Game State")]
    [SerializeField] bool playInitialCs;
    public bool DeveloperMode;
    public bool Loading;
    public bool GameIsPaused;
    public bool ShowingFixedMinimap;
    [SerializeField] NPC[] progressNPCs; // 0-OWL, 1-BEAR, 2-PEACOCK, 3-CAT, 4-WALRUS, 5-OWL_REVISITED
    [SerializeField] Transform[] vigilStones;
    [SerializeField] GameObject peacockAltarEntrance;
    [SerializeField] GameObject peacockThirdSpeech;
    [SerializeField] GameObject witch;
    [SerializeField] GameObject witchPostCs;
    [SerializeField] GameObject lionsGroundEntities;
    [SerializeField] GameObject suicidalKoboldPlatform;
    AudioSource thisAudioSource;
    float musicVolume = 0.45f;
    float ellapsedTimeWithWrongMusic = 0.0f;
    bool suicidalKoboldJumped = false;
    bool hasShownFirstMap = false;
    int currentStage = 0;
    public int CurrentStage { get { return currentStage; } }
    int nextOrbIndex = 0;
    int heldOrb = -1;
    int lastPowerUnlocked = -1;
    string checkpointsUsed = "";
    bool barrierDamaged = false;
    public bool BarrierDamaged { get { return barrierDamaged; } }
    bool playedWitchCs = false;
    Transform enemiesParent;
    public bool EnemiesParentActive { set { enemiesParent.gameObject.SetActive(value); } }
    List<int> defeatedEnemies = new List<int>();
    List<int> defeatedEnemiesUpToCheckpoint = new List<int>();
    bool saveWhenClosePowerChange = false;
    public bool SaveWhenClosePowerChange { get { return saveWhenClosePowerChange; } set { saveWhenClosePowerChange = value; } }

    [Header("Altars")]
    [SerializeField] GameObject[] altars;
    [SerializeField] Transform[] altarsGrounds;
    [SerializeField] Transform reunionRoomEntryPoint;
    [SerializeField] Transform witchsLair;
    [SerializeField] GameObject witchsHouseEntrance;
    [SerializeField] GameObject lionsPortal;
    [SerializeField] Transform lionsArena;
    [SerializeField] Transform checkpointsParent;
    Vector3 checkpointSavedPosition;

    [Header("Other")]
    public GameObject EnemiesShowUpParticles;
    public AudioClip[] EnemiesSpawnSounds;
    public GameObject EnemiesTargettingCollider;


    void Start() {
        
        if(transform.childCount > 0) {
            CameraBehavior.Instance.CentralizePlayer();
            return;
        }

        thisAudioSource = GetComponent<AudioSource>();
        Physics.gravity = new Vector3(0.0f, -25.0f, 0.0f);
        suicidalKoboldPlatform.SetActive(false);
        
        if(GameLoader.Instance != null) {
            Loading = GameLoader.Instance.IsUsed;
        }

        if (!Loading) {

            Player.Instance.transform.position = altarsGrounds[0].transform.position + Vector3.forward*4.0f;
            CameraBehavior.Instance.CentralizePlayer();
            checkpointSavedPosition = Player.Instance.transform.position;
            MapTransformator.Instance.TransformMap(currentStage, out enemiesParent);
            progressNPCs[0].CanBeTalkedTo = true;
            progressNPCs[0].nextPart.transform.GetChild(0).gameObject.SetActive(true);
            witchsLair.GetChild(0).gameObject.SetActive(true);

            if(!DeveloperMode) {
                playInitialCs = true;
                CameraBehavior.Instance.LockCamera = true;
                Player.Instance.SmartCast = false;
                Player.Instance.SetSkills(PlayerSkill.NONE, PlayerSkill.NONE);
                UIManager.Instance.ChangeSkills(PlayerSkill.NONE, PlayerSkill.NONE);
            }

            if (playInitialCs) {
                UIManager.Instance.PlayStartingScene();
            }
            UIManager.Instance.ChangeMinimapObjective(false);
        }
        else {
            UIManager.Instance.SetAlphaMaskBlack();
            progressNPCs[0].CanBeTalkedTo = false;
            progressNPCs[0].nextPart.transform.GetChild(0).gameObject.SetActive(false);
            witchsLair.GetChild(0).gameObject.SetActive(false);
            hasShownFirstMap = true;
            LoadGame();
        }

        for(int i = 0; i < checkpointsParent.childCount; i++) {
            checkpointsParent.GetChild(i).GetChild(0).transform.eulerAngles = Vector3.zero;
        }
    }

    void Update() {

        if(transform.childCount > 0) {
            return;
        }

        if(Player.Instance.transform.position.z < -350.0f && thisAudioSource.isPlaying) {
            ellapsedTimeWithWrongMusic += Time.deltaTime;
            if(ellapsedTimeWithWrongMusic > 0.75f) {
                ellapsedTimeWithWrongMusic = 0.0f;
                thisAudioSource.volume = 0.0f;
                thisAudioSource.Pause();
            }
        }
        else if(Player.Instance.transform.position.z > -350.0f && !thisAudioSource.isPlaying) {
            ellapsedTimeWithWrongMusic += Time.deltaTime;
            if(ellapsedTimeWithWrongMusic > 0.75f) {
                ellapsedTimeWithWrongMusic = 0.0f;
                thisAudioSource.volume = musicVolume;
                thisAudioSource.Play();
            }
        }
        else if(ellapsedTimeWithWrongMusic > 0.01f) {
            ellapsedTimeWithWrongMusic = 0.0f;
        }

        // FOR DEBUGGING
        if(Input.GetKey(KeyCode.Q) && Input.GetKeyDown(KeyCode.M)) {
            DeveloperMode = !DeveloperMode;
        }

        if (DeveloperMode) {

            if(Input.GetKeyDown(KeyCode.Alpha1)) {
                Player.Instance.transform.position = altarsGrounds[1].position;
                CameraBehavior.Instance.CentralizePlayer();
            }
            else if(Input.GetKeyDown(KeyCode.Alpha2)) {
                Player.Instance.transform.position = altarsGrounds[2].position;
                CameraBehavior.Instance.CentralizePlayer();
            }
            else if(Input.GetKeyDown(KeyCode.Alpha3)) {
                Player.Instance.transform.position = altarsGrounds[3].position;
                CameraBehavior.Instance.CentralizePlayer();
            }
            else if(Input.GetKeyDown(KeyCode.Alpha4)) {
                Player.Instance.transform.position = altarsGrounds[4].position;
                CameraBehavior.Instance.CentralizePlayer();
            }
            else if(Input.GetKeyDown(KeyCode.Alpha5)) {
                Player.Instance.transform.position = altarsGrounds[5].position;
                CameraBehavior.Instance.CentralizePlayer();
            }
            else if(Input.GetKeyDown(KeyCode.Alpha6)) {
                Player.Instance.transform.position = altarsGrounds[0].position;
                CameraBehavior.Instance.CentralizePlayer();
            }
            else if(Input.GetKeyDown(KeyCode.Alpha7)) {
                Player.Instance.transform.position = witchsLair.position + Vector3.left * 30.0f;
                CameraBehavior.Instance.CentralizePlayer();
            }
            else if(Input.GetKeyDown(KeyCode.Alpha8)) {
                Player.Instance.transform.position = lionsArena.position;
                CameraBehavior.Instance.CentralizePlayer();
            }
            else if(Input.GetKeyDown(KeyCode.P)) {
                LoadGame();
            }
        }
        // ----------------------------------
    }

    public void TakeNextOrb() {

        int orbIndex = nextOrbIndex;
        checkpointSavedPosition = altarsGrounds[(currentStage%5)+1].position;
        currentStage++;
        defeatedEnemies = new List<int>();
        defeatedEnemiesUpToCheckpoint = new List<int>();
        checkpointsUsed = "";
        for(int i = 0; i < checkpointsParent.childCount; i++) {
            ParticleSystem p1 = checkpointsParent.GetChild(i).GetChild(0).GetComponent<ParticleSystem>();
            p1.loop = true;
            p1.Play();
            ParticleSystem p2 = checkpointsParent.GetChild(i).GetChild(0).GetChild(0).GetComponent<ParticleSystem>();
            p2.loop = true;
            checkpointsParent.GetChild(i).GetComponent<Collider>().enabled = true;
            p2.Play();
            checkpointsParent.GetChild(i).GetChild(0).gameObject.SetActive(true);
        }
        MapTransformator.Instance.TransformMap(currentStage, out enemiesParent);

        if(vigilStones.Length >= currentStage) {
            vigilStones[currentStage-1].GetChild(0).gameObject.SetActive(true);
        }
        if(currentStage == 3) {
            peacockAltarEntrance.GetComponent<Collider>().enabled = false;
            peacockThirdSpeech.SetActive(true);
        }
        else if(currentStage == 5) {
            progressNPCs[0].gameObject.SetActive(false);
            progressNPCs[5].gameObject.SetActive(true);
        }
        else if(currentStage == 6) {
            progressNPCs[5].transform.GetChild(0).GetComponent<PowerOrb>().Dissipate();
        }
        nextOrbIndex++;

        if(witchsLair.childCount >= currentStage) {
            witchsLair.GetChild(currentStage-1).GetComponent<BarrierAnimation>().IsDamaged = true;
            barrierDamaged = true;
        }
            
        if(nextOrbIndex > 4) {
            nextOrbIndex = 0;
        }
        else if(nextOrbIndex == 1 && currentStage == 6) {
            nextOrbIndex = -1;
        }

        lastPowerUnlocked++;
        
        if(lastPowerUnlocked > 1) {
            UIManager.Instance.PromptPowerChange((PlayerSkill)orbIndex + 1);
            saveWhenClosePowerChange = true;
        }
        else if(lastPowerUnlocked == 1) {
            Player.Instance.SetSkills(PlayerSkill.OWL, PlayerSkill.BEAR);
            UIManager.Instance.ChangeSkills(PlayerSkill.OWL, PlayerSkill.BEAR);
            Player.Instance.CanControl = true;
            CameraBehavior.Instance.LockCameraForCutscene = false;
        }
        else if(lastPowerUnlocked == 0) {
            suicidalKoboldPlatform.SetActive(true);
            Player.Instance.SetSkills(PlayerSkill.OWL, PlayerSkill.NONE);
            UIManager.Instance.ChangeSkills(PlayerSkill.OWL, PlayerSkill.NONE);
            UIManager.Instance.OwlGiveOrbFollowup();
        }

        heldOrb = orbIndex;
        UIManager.Instance.ChangeMinimapObjective(true);
        SaveGame();
    }

    /*public void TakeOrb(int orbIndex) {
        
        if(orbIndex == nextOrbIndex) {

            currentStage++;
            MapTransformator.Instance.TransformMap(currentStage, out enemiesParent);
            nextOrbIndex++;
            
            if(nextOrbIndex > 4) {
                nextOrbIndex = 0;
            }
            else if(nextOrbIndex == 1 && currentStage == 6) {
                nextOrbIndex = -1;
            }

            lastPowerUnlocked++;
        }
        
        if((lastPowerUnlocked > 1 && orbIndex <= nextOrbIndex) || lastPowerUnlocked == 5) {
            UIManager.Instance.PromptPowerChange((PlayerSkill)orbIndex + 1);
        }
        else if(lastPowerUnlocked == 1 && orbIndex <= nextOrbIndex) {
            Player.Instance.SetSkills(PlayerSkill.OWL, PlayerSkill.BEAR);
            UIManager.Instance.ChangeSkills(PlayerSkill.OWL, PlayerSkill.BEAR);
        }

        heldOrb = orbIndex;
        UIManager.Instance.ChangeOrbText(heldOrb);
    }*/
    
    void EnterLionsGround() {

        lionsGroundEntities.SetActive(true);
        CameraBehavior.Instance.FocusOnPoint(lionsGroundEntities.transform.GetChild(0).position);
        UIManager.Instance.PlayLionEnterScene();
    }

    public void EnterAltar(Transform entrancePoint) {

        new Timer (0.6f, () => {
            if(this == null) return;
            if (!thisAudioSource.isPlaying)
                thisAudioSource.Play();
        });
        Tween volumeTween = new Tween(thisAudioSource.volume, musicVolume - thisAudioSource.volume, 1.2f, false, EasingType.Linear, (float volume) => { thisAudioSource.volume = volume; });
        volumeTween.OnComplete += () => {
            if(thisAudioSource.volume < 0.05f)
                thisAudioSource.Pause();
        };

        if(entrancePoint == lionsArena) {
            
            AnimateChangeLocation(new Vector3(lionsArena.position.x, 0.7f, lionsArena.position.z), EnterLionsGround);
        }
        else if(entrancePoint.gameObject == witchsHouseEntrance) {

            if(!playedWitchCs) {
                Player.Instance.IsInCutscene = true;
                enemiesParent.gameObject.SetActive(false);
                UIManager.Instance.PlayWitchsLairScene();
                witchsHouseEntrance.GetComponent<Collider>().enabled = false;
                playedWitchCs = true;
            }
        }
        else {
            if(hasShownFirstMap) {
                AnimateChangeLocation(entrancePoint.position);
            }
            else {
                hasShownFirstMap = true;
                AnimateChangeLocation(entrancePoint.position, () => {
                    ShowingFixedMinimap = true;
                    UIManager.Instance.ShowMinimap(true, true);
                    Player.Instance.CanControl = false;
                    CameraBehavior.Instance.LockCameraForCutscene = true;
                });
            }
        }
    }

    void AnimateChangeLocation(Vector3 targetPosition, UpdateAction onComplete = null) {

        const float transitionTime = 0.75f;
        Player.Instance.gameObject.layer = LayerMask.NameToLayer("PlayerNoEnemy");
        Player.Instance.CanControl = false;
        UIManager.Instance.AlphaMaskFullFade(false, transitionTime);
        new Timer(transitionTime, () => {
            Player.Instance.transform.position = targetPosition;
            CameraBehavior.Instance.CentralizePlayer();
            UIManager.Instance.AlphaMaskFullFade(true, transitionTime);
            new Timer(transitionTime, () => {
                Player.Instance.gameObject.layer = LayerMask.NameToLayer("Player");
                Player.Instance.CanControl = true;
                if(onComplete != null) {
                    onComplete();
                }
            });
        });
    }

    public void EndWitchsCutscene() {

        for(int i = 0; i < vigilStones.Length; i++) {
            vigilStones[i].GetChild(0).gameObject.SetActive(false);
            vigilStones[i].GetChild(0).transform.localPosition = new Vector3(0.0f, 4.0f, 0.0f);
        }
        Player.Instance.IsInCutscene = false;
        UIManager.Instance.HideWitchsEntities();
        UIManager.Instance.ShowLionOnMinimap();
        lionsPortal.SetActive(true);
        witch.SetActive(false);
        witchPostCs.transform.position = witch.transform.position;
        witchPostCs.transform.eulerAngles = witch.transform.eulerAngles;
        witchPostCs.SetActive(true);
        for(int i = 0; i < checkpointsParent.childCount; i++) {
            checkpointsParent.GetChild(i).GetChild(0).gameObject.SetActive(true);
        }
        checkpointsUsed = "";
        SaveGame();
    }

    public void BreakBarrier() {

        barrierDamaged = false;

        if(progressNPCs.Length > currentStage) {
            progressNPCs[currentStage].CanBeTalkedTo = true;
            if(progressNPCs[currentStage].transform.childCount > 1) {
                progressNPCs[currentStage].transform.GetChild(0).gameObject.SetActive(true);
            }
            else {
                progressNPCs[currentStage].nextPart.transform.GetChild(0).gameObject.SetActive(true);
            }
        }
        witchsLair.GetChild(currentStage-1).GetComponent<BarrierAnimation>().Vanish();
        UIManager.Instance.ChangeMinimapObjective(false);

        if (witchsLair.childCount > currentStage) {
            witchsLair.GetChild(currentStage).gameObject.SetActive(true);
        }
        else {
            UIManager.Instance.StartWitchsFirstConversation();
            Player.Instance.CanBreakBarrier = false;
        }
    }

    public void SuicidalKoboldPlatformFall() {

        suicidalKoboldJumped = true;

        new Timer(0.5f, () => {
            Vector3 initialPosition = suicidalKoboldPlatform.transform.position;
            Tween platformFall = new Tween(initialPosition, initialPosition - Vector3.up * 5.0f, 1.0f, false, EasingType.EaseIn, (Vector3 value) => {
                suicidalKoboldPlatform.transform.position = value;
            });
            platformFall.OnComplete += () => {
                suicidalKoboldPlatform.SetActive(false);
                suicidalKoboldPlatform.transform.position = initialPosition;
            };
        });
    }

    public void Peacock_OpenWay() {
        peacockAltarEntrance.GetComponent<Collider>().enabled = true;
        peacockThirdSpeech.SetActive(false);
        Player.Instance.CanControl = true;
        CameraBehavior.Instance.LockCameraForCutscene = false;
    }

    public void DestroyLionCsObjects() {

        lionsGroundEntities.SetActive(false);
        //Destroy(lionsGroundEntities);
    }

    public void LoadGame() {

        if(Lion.Instance != null) {
            
            Lion.Instance.Reset();
        }
        Player.Instance.LoadPlayerInfo();
        currentStage = PlayerPrefs.GetInt("currentStage");
        nextOrbIndex = PlayerPrefs.GetInt("nextOrb");
        heldOrb = PlayerPrefs.GetInt("heldOrb");
        lastPowerUnlocked = PlayerPrefs.GetInt("lastPowerUnlocked");
        barrierDamaged = (PlayerPrefs.GetInt("barrierDamaged") == 1);
        playedWitchCs = (PlayerPrefs.GetInt("playedWitchCs") == 1);
        suicidalKoboldJumped = (PlayerPrefs.GetInt("suicidalKoboldJumped") == 1);
        checkpointSavedPosition = new Vector3(PlayerPrefs.GetFloat("checkpointSavedPositionX"), PlayerPrefs.GetFloat("checkpointSavedPositionY"), PlayerPrefs.GetFloat("checkpointSavedPositionZ"));
        CameraBehavior.Instance.LockCamera = (PlayerPrefs.GetInt("cameraLocked") == 1);
        checkpointsUsed = PlayerPrefs.GetString("checkpointsUsed");
        string[] checkpoints = checkpointsUsed.Split(';');
        for (int i = 0; i < checkpoints.Length; i++) {
            int childID;
            if(int.TryParse(checkpoints[i], out childID)) {
                checkpointsParent.GetChild(childID).GetChild(0).gameObject.SetActive(false);
                checkpointsParent.GetChild(childID).GetComponent<Collider>().enabled = false;
            }
        }
        string[] enemiesDead = PlayerPrefs.GetString("enemiesDead").Split(';');
        for (int i = 0; i < enemiesDead.Length-1; i++) {
            defeatedEnemies.Add(int.Parse(enemiesDead[i]));
            defeatedEnemiesUpToCheckpoint.Add(int.Parse(enemiesDead[i]));
        }

        for(int i = 0; i < vigilStones.Length; i++) {
            if(i < currentStage) {
                vigilStones[i].GetChild(0).gameObject.SetActive(true);
            }
        }
        if(currentStage == 3 && Vector3.Distance(Player.Instance.transform.position, progressNPCs[2].transform.position) < 15.0f) {
            peacockAltarEntrance.GetComponent<Collider>().enabled = false;
            peacockThirdSpeech.SetActive(true);
        }
        else if(currentStage == 5) {
            progressNPCs[0].gameObject.SetActive(false);
            progressNPCs[5].gameObject.SetActive(true);
        }

        suicidalKoboldPlatform.SetActive(currentStage == 1 && !suicidalKoboldJumped);

        if(!barrierDamaged && progressNPCs.Length > currentStage) {
            progressNPCs[currentStage].CanBeTalkedTo = true;
            if(progressNPCs[currentStage].transform.childCount > 1) {
                progressNPCs[currentStage].transform.GetChild(0).gameObject.SetActive(true);
            }
            else {
                progressNPCs[currentStage].nextPart.transform.GetChild(0).gameObject.SetActive(true);
            }
        }
        else if(barrierDamaged && witchsLair.childCount >= currentStage) {
            witchsLair.GetChild(currentStage-1).GetComponent<BarrierAnimation>().IsDamaged = true;
        }

        for(int i = 0; i < witchsLair.childCount; i++) {
            witchsLair.GetChild(i).gameObject.SetActive(false);
        }
        if (witchsLair.childCount >= currentStage) {
            if(barrierDamaged) {
                witchsLair.GetChild(currentStage-1).gameObject.SetActive(true);
            }
            else if (witchsLair.childCount > currentStage){
                witchsLair.GetChild(currentStage).gameObject.SetActive(true);
            }
        }

        UIManager.Instance.ChangeMinimapObjective(barrierDamaged);
        //ReturnToCheckpoint();
        MapTransformator.Instance.TransformMap(currentStage, out enemiesParent);
        Player.Instance.transform.position = checkpointSavedPosition;
        CameraBehavior.Instance.CentralizePlayer();
        
        Vector3 playerPosition = Player.Instance.transform.position;
        Player.Instance.transform.position = new Vector3(0.0f, 1000.0f, 0.0f);
        Player.Instance.GetComponent<Rigidbody>().useGravity = false;
        ResetEnemiesToCheckpoint();
        new Timer(2.0f, () => {
            Player.Instance.transform.position = playerPosition;
            Player.Instance.transform.eulerAngles = Vector3.zero;
            Player.Instance.GetComponent<Rigidbody>().useGravity = true;
            CameraBehavior.Instance.CentralizePlayer();
            UIManager.Instance.AlphaMaskFullFade(true);
        });

        if (playedWitchCs) {
            EndWitchsCutscene();
        }
    }

    public void SaveGame() {

        Player.Instance.SavePlayerInfo();
        PlayerPrefs.SetInt("currentStage", currentStage);
        PlayerPrefs.SetInt("nextOrb", nextOrbIndex);
        PlayerPrefs.SetInt("heldOrb", heldOrb);
        PlayerPrefs.SetInt("lastPowerUnlocked", lastPowerUnlocked);
        PlayerPrefs.SetInt("barrierDamaged", (barrierDamaged) ? 1 : 0);
        PlayerPrefs.SetInt("playedWitchCs", (playedWitchCs) ? 1 : 0);
        PlayerPrefs.SetInt("suicidalKoboldJumped", (suicidalKoboldJumped) ? 1 : 0);
        PlayerPrefs.SetFloat("checkpointSavedPositionX", checkpointSavedPosition.x);
        PlayerPrefs.SetFloat("checkpointSavedPositionY", checkpointSavedPosition.y);
        PlayerPrefs.SetFloat("checkpointSavedPositionZ", checkpointSavedPosition.z);
        PlayerPrefs.SetInt("cameraLocked", CameraBehavior.Instance.LockCamera ? 1 : 0);
        PlayerPrefs.SetString("checkpointsUsed", checkpointsUsed);
        string enemiesDead = "";
        for (int i = 0; i < defeatedEnemiesUpToCheckpoint.Count; i++) {
            enemiesDead += defeatedEnemiesUpToCheckpoint[i] + ";";
        }
        PlayerPrefs.SetString("enemiesDead", enemiesDead);
    }

    public void SaveCheckpoint(int checkpointID, Vector3 checkpointPosition) {

        checkpointSavedPosition = checkpointPosition;

        if(!lionsPortal.activeInHierarchy) {

            if(currentStage != 6) {
                if(checkpointsUsed == "") {
                    checkpointsUsed = checkpointID.ToString();
                }
                else {
                    checkpointsUsed += ";" + checkpointID;
                }
            }
        
            checkpointsParent.GetChild(checkpointID).GetChild(0).GetComponent<ParticleSystem>().loop = false;
            checkpointsParent.GetChild(checkpointID).GetChild(0).GetChild(0).GetComponent<ParticleSystem>().loop = false;
            checkpointsParent.GetChild(checkpointID).GetComponent<Collider>().enabled = false;
        }

        defeatedEnemiesUpToCheckpoint = new List<int>();
        for (int i = 0; i < defeatedEnemies.Count; i++) {
            defeatedEnemiesUpToCheckpoint.Add(defeatedEnemies[i]);
        }
        SaveGame();
    }

    public void ReturnToCheckpoint(UpdateAction onComplete) {

        UIManager.Instance.AlphaMaskFullFade(false, 0.3f, () => {
            LoadGame();
        });
        if(onComplete != null) {
            new Timer(2.0f, () => {
                onComplete();
            });
        }

        /*MapTransformator.Instance.TransformMap(currentStage, out enemiesParent);
        Player.Instance.transform.position = checkpointSavedPosition;
        CameraBehavior.Instance.CentralizePlayer();
        ResetEnemiesToCheckpoint();*/
    }

    void ResetEnemiesToCheckpoint() {

        if(defeatedEnemiesUpToCheckpoint.Count == 0) {
            return;
        }
        
        defeatedEnemiesUpToCheckpoint.Sort();
        defeatedEnemiesUpToCheckpoint = defeatedEnemiesUpToCheckpoint.Distinct().ToList();
        int index = 0;
        int nextID = defeatedEnemiesUpToCheckpoint[index];

        List<Transform> enemiesGroups = new List<Transform>();

        for (int i = 0; i < enemiesParent.childCount; i++) {

            if (enemiesParent.GetChild(i).GetComponent<EnemiesGroup>()) {
                enemiesGroups.Add(enemiesParent.GetChild(i));
            }
            else if(enemiesParent.GetChild(i).GetComponent<Enemy>().IdInPreset >= nextID) {
                
                if(enemiesParent.GetChild(i).GetComponent<Enemy>().IdInPreset > nextID) {

                    while (enemiesParent.GetChild(i).GetComponent<Enemy>().IdInPreset > nextID) {
                        
                        Debug.Log("Out of place: " + enemiesParent.GetChild(i).GetComponent<Enemy>().IdInPreset + " / " + nextID);
                        index++;
                        if(index < defeatedEnemiesUpToCheckpoint.Count-1) {
                            nextID = defeatedEnemiesUpToCheckpoint[index];
                        }
                        else {
                            break;
                        }
                    }
                }

                if(enemiesParent.GetChild(i).GetComponent<Enemy>().IdInPreset == nextID) {

                    Destroy(enemiesParent.GetChild(i).gameObject, 0.01f);
                    if(index < defeatedEnemiesUpToCheckpoint.Count-1) {
                        index++;
                        nextID = defeatedEnemiesUpToCheckpoint[index];
                    }
                    else {
                        return;
                    }
                }
            }
        }

        for(int i = 0; i < enemiesGroups.Count; i++) {

            for(int j = 0; j < enemiesGroups[i].childCount; j++) {

                if(enemiesGroups[i].GetChild(j).GetComponent<Enemy>().IdInPreset > nextID) {

                    while (enemiesGroups[i].GetChild(j).GetComponent<Enemy>().IdInPreset > nextID) {
                        
                        Debug.Log("Out of place: " + enemiesGroups[i].GetChild(j).GetComponent<Enemy>().IdInPreset + " / " + nextID);
                        index++;
                        if(index < defeatedEnemiesUpToCheckpoint.Count-1) {
                            nextID = defeatedEnemiesUpToCheckpoint[index];
                        }
                        else {
                            break;
                        }
                    }
                }
                if (enemiesGroups[i].GetChild(j).GetComponent<Enemy>().IdInPreset == nextID) {

                    Destroy(enemiesParent.GetChild(i).gameObject, 1);
                    if(index < defeatedEnemiesUpToCheckpoint.Count-1) {
                        index++;
                        nextID = defeatedEnemiesUpToCheckpoint[index];
                    }
                    else {
                        return;
                    }
                }
            }
        }
    }

    public void AddToDefeatedEnemies(int enemyID) {
        
        defeatedEnemies.Add(enemyID);
    }

    public void ShowOwlsLion(bool show) {
        vigilStones[0].GetChild(0).GetChild(0).gameObject.SetActive(show);
    }
}
