﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utilities : MonoBehaviour {

    public static Vector3 GroundedVector3(Vector3 vector) {
        vector.y = 0.0f;
        return vector;
    }

    public static Vector2 Vector3to2(Vector3 vector) {
        vector.y = vector.z;
        return vector;
    }
}
