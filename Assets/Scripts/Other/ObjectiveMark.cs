﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveMark : MonoBehaviour {

    [SerializeField] float speed;
    [SerializeField] float maxSize;
    [SerializeField] float scaleLimit;
    [SerializeField] float scaleSpeed;
    const float speedDecrease = 0.5f;
    bool speedDecreased;
    bool decreasing = true;
	
	void Update () {

        int childToRelocate = -1;
        
        transform.localScale += Vector3.one * scaleSpeed * Time.deltaTime;
        if(transform.localScale.x < 1 - scaleLimit) {
            decreasing = false;
            scaleSpeed = Mathf.Abs(scaleSpeed);
        }
        else if(transform.localScale.x > 1 + scaleLimit) {
            decreasing = true;
            scaleSpeed = -Mathf.Abs(scaleSpeed);
        }

        if(!speedDecreased && !decreasing && transform.localScale.x > 1 + (scaleLimit * 0.33f)) {
            scaleSpeed = scaleSpeed * speedDecrease;
            speedDecreased = true;
        }
        else if(speedDecreased && decreasing) {
            scaleSpeed = scaleSpeed * (1 / speedDecrease);
            speedDecreased = false;
        }
        
        for(int i = 0; i < transform.childCount; i++) {

            RectTransform childTransform = transform.GetChild(i).GetComponent<RectTransform>();
            childTransform.sizeDelta += Vector2.one * speed * Time.deltaTime;// * maxSize / Mathf.Max(childTransform.sizeDelta.x, 10.0f);
            if(speed > 0 && childTransform.sizeDelta.x > maxSize) {
                childToRelocate = i;
                childTransform.sizeDelta = new Vector2(childTransform.sizeDelta.x % maxSize, childTransform.sizeDelta.y % maxSize);
            }
            else if( speed < 0 && childTransform.sizeDelta.x < 0) {
                childToRelocate = i;
                childTransform.sizeDelta = new Vector2(childTransform.sizeDelta.x + maxSize, childTransform.sizeDelta.y + maxSize);
            }
        }

        if(childToRelocate != -1) {
            if(speed > 0) {
                transform.GetComponent<Image>().color = transform.GetChild(childToRelocate).GetComponent<Image>().color;
                transform.GetChild(childToRelocate).SetAsLastSibling();
            }
            else {
                transform.GetComponent<Image>().color = transform.GetChild(0).GetComponent<Image>().color;
                transform.GetChild(childToRelocate).SetAsFirstSibling();
            }
        }
	}
}
