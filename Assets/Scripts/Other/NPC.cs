﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public enum NPCAction { NULL, GIVE_ORB, PEACOCK_THIRD, LAST_ORB, DISSIPATE, END_WITCH_CS, PROMPT_ALL_POWER }

public class NPC : MonoBehaviour {

    public string[] speech;
    public Vector2 talkBoxPosition;
    public Vector2 talkBoxSize;
    public TimelineAsset timelineEvent;
    public bool CanBeTalkedTo = true;
    public bool oneTimeTalk;
    public NPCAction action;
    public NPC nextPart;
}
