﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ChronosFramework;
using UnityEngine.Timeline;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class UIManager : Singleton<UIManager> {

    [SerializeField] GameObject developerModeFeedback;

    [Header("Player UI")]
    [SerializeField] GameObject playerResourcesParent;
    public GameObject PlayerResourcesParent { get { return playerResourcesParent; } }
    [SerializeField] Image[] hpBars;
    [SerializeField] Canvas enemiesHPCanvas;
    public Canvas EnemiesHPCanvas { get { return enemiesHPCanvas; } }
    [SerializeField] Image[] cooldownCircles;
    [SerializeField] Text powerNameText;
    [SerializeField] Color lifeGreen;
    [SerializeField] Color lifeRed;
    [SerializeField] Sprite[] powersSprites;
    [SerializeField] GameObject interactionPopup;
    PlayerSkill[] skillOnBtns = new PlayerSkill[2];
    Color alphaMaskColor;

    [Header("Minimap")]
    [SerializeField] GameObject minimap;
    [SerializeField] GameObject minimapObjective;
    [SerializeField] GameObject[] minimapEntrances;
    [SerializeField] Transform minimapPiecesParent;
    [SerializeField] RectTransform minimapMask;
    [SerializeField] GameObject[] minimapTooltips;
    bool animatingInitialMinimap = false;

    [Header("Conversations")]
    [SerializeField] GameObject talkBox;
    [SerializeField] Text talkText;
    [SerializeField] NPC awakeSimulatedNPC;
    [SerializeField] NPC vigilOwl;
    [SerializeField] NPC witchOpenBarrier;
    [SerializeField] NPC lionEnter;
    [SerializeField] float passTalkDelay;
    string[] currentTalk;
    int currentTalkIndex = 0;
    float ellapsedPassTalkTime = 0.0f;
    TimelineAsset timelineEventPostTalk;
    UpdateAction actionPostTalk;
    NPC currentSpeaker;
    NPC followupSpeaker;

    [Header("Cutscenes")]
    [SerializeField] GameObject witchsLairScenes2D;
    [SerializeField] TimelineAsset exitWitchsLairCs;
    [SerializeField] GameObject[] witchsLairEntities;
    public GameObject WitchsLairCsBarrier;

    [Header("Power Change")]
    [SerializeField] GameObject alphaMask;
    [SerializeField] GameObject powerChangeWindow;
    [SerializeField] GameObject allPowersChangeWindow;
    [SerializeField] Text pc_acquiredPowerText;
    [SerializeField] Image pc_acquiredPowerImg;
    [SerializeField] Image[] pc_powerOnBtnsImg;
    [SerializeField] Image[] apc_powerOnBtnsImg;
    PlayerSkill acquiringSkill;
    Dictionary<PlayerSkill, string> skillToString;

    [Header("Menu and Tutorials")]
    [SerializeField] GameObject menu;
    [SerializeField] Toggle cameraLockToggle;
    [SerializeField] Toggle smartCastToggle;
    [SerializeField] GameObject lockCameraTip;
    [SerializeField] Text smartCastText;
    [SerializeField] GameObject mapTabKey;
    [SerializeField] GameObject provisoryEndGameScreen;


    void Start() {

        if(GameManager.Instance.transform.childCount > 0) {
            PromptAllPowersChange();
        }

        talkBox.SetActive(false);
        DefineSkillToStringDictionary();
    }


    public void PlayStartingScene() {

        vigilOwl.gameObject.SetActive(true);
        alphaMaskColor = alphaMask.GetComponent<Image>().color;
        alphaMask.GetComponent<Image>().color = Color.black;
        alphaMask.SetActive(true);
        Player.Instance.CanControl = false;
        CameraBehavior.Instance.LockCameraForCutscene = true;
        CameraBehavior.Instance.FocusOnPoint(Player.Instance.transform.position + 10 * Vector3.forward);
        Player.Instance.SetSkills(PlayerSkill.NONE, PlayerSkill.NONE);
        playerResourcesParent.SetActive(false);

        StartConversation(awakeSimulatedNPC);
        actionPostTalk = () => {
            AlphaMaskFullFade(true);
            new Timer(0.01f, () => {
                Player.Instance.CanControl = false;
                CameraBehavior.Instance.LockCameraForCutscene = true;
                playerResourcesParent.SetActive(false);
            });
            new Timer(1.8f, () => {
                StartConversation(vigilOwl);
            });
        };
    }

    public void StartWitchsFirstConversation() {

        Player.Instance.CanControl = false;
        CameraBehavior.Instance.LockCameraForCutscene = true;
        playerResourcesParent.SetActive(false);
        new Timer(1.8f, () => {
            StartConversation(witchOpenBarrier);
        });
    }

    public void PlayWitchsLairScene() {

        Player.Instance.CanControl = false;
        CameraBehavior.Instance.CentralizePlayer();
        CameraBehavior.Instance.LockCameraForCutscene = true;
        PlayerResourcesParent.SetActive(false);

        UpdateAction onCompleteScene2D = () => {
            Player.Instance.transform.position = new Vector3(161.5f, Player.Instance.transform.position.y, -81.8f);
            Player.Instance.transform.eulerAngles = new Vector3(0.0f, -90.0f, 0.0f);
            for(int i = 0; i < witchsLairEntities.Length; i++) {
                witchsLairEntities[i].SetActive(true);
            }
            GetComponent<PlayableDirector>().playableAsset = exitWitchsLairCs;
            GetComponent<PlayableDirector>().Play();
        };
        PlayWitchsLairScene2D(onCompleteScene2D);
    }
    
    public void PlayWitchsLairScene2D(UpdateAction onCompleteScene2D) {

        witchsLairScenes2D.SetActive(true);
        Image scene2DParent = witchsLairScenes2D.GetComponent<Image>();
        Tween scene2DEnter = new Tween(0.0f, 1.0f, 0.75f, false, EasingType.Linear, (float alpha) => {
            scene2DParent.color = new Color(0.0f, 0.0f, 0.0f, alpha);
        });
        scene2DEnter.OnComplete += () => {
            
            Image scene2D = scene2DParent.transform.GetChild(0).GetComponent<Image>();
            scene2D.gameObject.SetActive(true);
            System.Action<float> FadeImage = (alpha) => {
                scene2D.color = new Color(1.0f, 1.0f, 1.0f, alpha);
            };
            new Tween(0.0f, 1.0f, 0.5f, false, EasingType.Linear, FadeImage);

            new Timer(3.0f, () => {
                scene2D.gameObject.SetActive(false);
                scene2D = scene2DParent.transform.GetChild(1).GetComponent<Image>();
                scene2D.gameObject.SetActive(true);

                new Timer(3.0f, () => {
                    Tween image2DLeave = new Tween(1.0f, 0.0f, 0.5f, false, EasingType.Linear, FadeImage);
                    image2DLeave.OnComplete += () => {
                        scene2D.gameObject.SetActive(false);
                        Tween scene2DLeave = new Tween(1.0f, 0.0f, 0.75f, false, EasingType.Linear, (float alpha) => {
                            scene2DParent.color = new Color(0.0f, 0.0f, 0.0f, alpha);
                        });
                        scene2DLeave.OnComplete += () => {
                            scene2DParent.gameObject.SetActive(false);
                            onCompleteScene2D();
                        };
                    };
                });
            });
        };
    }

    public void PlayLionEnterScene() {

        Player.Instance.CanControl = false;
        CameraBehavior.Instance.LockCameraForCutscene = true;
        playerResourcesParent.SetActive(false);
        new Timer(2.0f, () => {
            StartConversation(lionEnter);
        });
    }

    public void HideWitchsEntities() {
        for(int i = 0; i < witchsLairEntities.Length; i++) {
            Destroy(witchsLairEntities[i]);
        }
    }

    public void PlayEndingScene() {

    }

    void Update() {

        ManageConversationCutscene();

        if(Input.GetKeyDown(KeyCode.Escape) && (!alphaMask.activeSelf || menu.activeSelf)) {
            ToggleMenu();
        }
        if(Input.GetKeyUp(KeyCode.Tab)) {
            ShowMinimap(false);
        }

        if(!developerModeFeedback.activeSelf && GameManager.Instance.DeveloperMode) {
            developerModeFeedback.SetActive(true);
        }
        else if(developerModeFeedback.activeSelf && !GameManager.Instance.DeveloperMode) {
            developerModeFeedback.SetActive(false);
        }
    }

    void ManageConversationCutscene() {

        if(currentTalk != null) {

            ellapsedPassTalkTime += Time.deltaTime;

            if(ellapsedPassTalkTime > passTalkDelay && (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl))) {

                ellapsedPassTalkTime = 0.0f;

                if(currentTalkIndex < currentTalk.Length) {

                    talkText.text = currentTalk[currentTalkIndex];
                    currentTalkIndex++;
                }
                else if(timelineEventPostTalk != null) {

                    talkBox.SetActive(false);
                    currentTalk = null;
                    GetComponent<PlayableDirector>().playableAsset = timelineEventPostTalk;
                    GetComponent<PlayableDirector>().Play();
                }
                else if(actionPostTalk != null) {
                
                    talkBox.SetActive(false);
                    currentTalk = null;
                    UpdateAction previousAction = actionPostTalk;
                    actionPostTalk();
                    if(previousAction == actionPostTalk) {
                        actionPostTalk = null;
                        Player.Instance.CanControl = true;
                        CameraBehavior.Instance.LockCameraForCutscene = false;
                        playerResourcesParent.SetActive(true);
                    }
                }
                else if(followupSpeaker != null) {

                    NPC nextSpeaker = followupSpeaker;
                    followupSpeaker = null;
                    StartConversation(nextSpeaker);
                }
                else {

                    Player.Instance.CanControl = true;
                    CameraBehavior.Instance.LockCameraForCutscene = false;
                    talkBox.SetActive(false);
                    currentTalk = null;
                    playerResourcesParent.SetActive(true);
                }
            }
        }
    }

    void DefineSkillToStringDictionary() {

        skillToString = new Dictionary<PlayerSkill, string>();
        skillToString[PlayerSkill.NONE] = "";
        skillToString[PlayerSkill.OWL] = "Coruja";
        skillToString[PlayerSkill.BEAR] = "Urso";
        skillToString[PlayerSkill.PEACOCK] = "Pavão";
        skillToString[PlayerSkill.CAT] = "Gato";
        skillToString[PlayerSkill.WALRUS] = "Morsa";
    }

    public void ChangeSkills(PlayerSkill skillOnE, PlayerSkill skillOnR) {

        skillOnBtns[0] = skillOnE;
        skillOnBtns[1] = skillOnR;

        if ((int)skillOnBtns[0] != 0) {
            cooldownCircles[2].transform.parent.GetChild(1).GetComponent<Image>().sprite = powersSprites[(int)skillOnBtns[0] - 1];
        }
        else {
            cooldownCircles[2].transform.parent.GetChild(1).GetComponent<Image>().sprite = null;
        }
        if((int)skillOnBtns[1] != 0) {
            cooldownCircles[3].transform.parent.GetChild(1).GetComponent<Image>().sprite = powersSprites[(int)skillOnBtns[1] - 1];
        }
        else {
            cooldownCircles[3].transform.parent.GetChild(1).GetComponent<Image>().sprite = null;
        }
    }

    public void PutOnCooldown(int skill, float cooldown) {
        
        new Tween(1, 0, cooldown, false, EasingType.Linear, (percentage) => {
            cooldownCircles[skill].fillAmount = percentage;
        });
    }

    public void PromptPowerChange(PlayerSkill orbSkill) {

        if(orbSkill == skillOnBtns[0] || orbSkill == skillOnBtns[1]) {
            return;
        }

        alphaMask.SetActive(true);
        powerChangeWindow.SetActive(true);
        
        new Timer(0.1f, () => {
            Player.Instance.CanControl = false;
            CameraBehavior.Instance.LockCameraForCutscene = true;
        });

        acquiringSkill = orbSkill;
        pc_acquiredPowerImg.sprite = powersSprites[(int)orbSkill - 1];
        pc_acquiredPowerText.text = skillToString[orbSkill];
        pc_powerOnBtnsImg[0].sprite = powersSprites[(int)skillOnBtns[0] - 1];
        pc_powerOnBtnsImg[1].sprite = powersSprites[(int)skillOnBtns[1] - 1];
    }

    void PromptAllPowersChange() {
        
        alphaMask.SetActive(true);
        allPowersChangeWindow.SetActive(true);

        new Timer(0.1f, () => {
            Player.Instance.CanControl = false;
            CameraBehavior.Instance.LockCameraForCutscene = true;
        });

        apc_powerOnBtnsImg[0].sprite = null;
        apc_powerOnBtnsImg[1].sprite = null;
    }

    public void ClosePowerChangeWindow() {

        AlphaMaskFadeGroup(false, () => {

            if(GameManager.Instance.transform.childCount == 0) {
                GameManager.Instance.EnemiesParentActive = true;
            }
            powerChangeWindow.SetActive(false);
            allPowersChangeWindow.SetActive(false);
            
            if (followupSpeaker != null) {
                StartConversation(followupSpeaker);
                followupSpeaker = null;
            }
            else {
                Player.Instance.CanControl = true;
                CameraBehavior.Instance.LockCameraForCutscene = false;
            }

            if(GameManager.Instance.SaveWhenClosePowerChange) {
                GameManager.Instance.SaveWhenClosePowerChange = false;
                GameManager.Instance.SaveGame();
            }
        });
    }

    void AlphaMaskFadeGroup(bool fadeIn, UpdateAction onComplete = null) {

        System.Action<float> FadeAlpha = (alpha) => {
            alphaMask.GetComponent<CanvasGroup>().alpha = alpha;
        };

        Tween alphaMaskTween;
        if (fadeIn) {
            alphaMaskTween = new Tween(0, 1, 0.5f, false, EasingType.Linear, FadeAlpha);
        }
        else {
            alphaMaskTween = new Tween(1, 0, 0.5f, false, EasingType.Linear, FadeAlpha);
        }

        alphaMask.SetActive(true);
        alphaMaskTween.OnComplete += () => {
            alphaMask.SetActive(false);
            alphaMask.GetComponent<CanvasGroup>().alpha = 1;
            if(onComplete != null) {
                onComplete();
            }
        };
    }

    public void AlphaMaskFullFade(bool fadeIn, float time = 1.0f, UpdateAction onComplete = null) {

        alphaMask.SetActive(true);
        System.Action<Color> FadeColor = (color) => {
            alphaMask.GetComponent<Image>().color = color;
        };

        Tween alphaTween;
        bool active = true;

        if (fadeIn) {
            alphaTween = new Tween(new Color(0, 0, 0, 1), new Color(0, 0, 0, 0), time, false, EasingType.Linear, FadeColor);
            active = false;
        }
        else {
            alphaTween = new Tween(new Color(0, 0, 0, 0), new Color(0, 0, 0, 1), time, false, EasingType.Linear, FadeColor);
        }

        alphaTween.OnComplete += () => {
            alphaMask.SetActive(active);
            if(onComplete != null) {
                onComplete();
            }
        };
    }

    public void ChangePower(int btnIndex) {

        PlayerSkill auxSkill = acquiringSkill;
        acquiringSkill = skillOnBtns[btnIndex];
        skillOnBtns[btnIndex] = auxSkill;

        pc_acquiredPowerImg.sprite = powersSprites[(int)acquiringSkill - 1];
        pc_acquiredPowerText.text = skillToString[acquiringSkill];
        pc_powerOnBtnsImg[btnIndex].sprite = powersSprites[(int)skillOnBtns[btnIndex] - 1];

        ChangeSkills(skillOnBtns[0], skillOnBtns[1]);
        Player.Instance.SetSkills(skillOnBtns[0], skillOnBtns[1]);
    }

    public void DefinePower(int powerIndex) {
        
        if(apc_powerOnBtnsImg[0].sprite != null && apc_powerOnBtnsImg[1].sprite != null)
            return;

        int btnIndex = (apc_powerOnBtnsImg[0].sprite == null) ? 0 : 1;

        apc_powerOnBtnsImg[btnIndex].sprite = powersSprites[powerIndex];
        skillOnBtns[btnIndex] = (PlayerSkill)powerIndex + 1;
        ChangeSkills(skillOnBtns[0], skillOnBtns[1]);
        Player.Instance.SetSkills(skillOnBtns[0], skillOnBtns[1]);
    }

    public void RemovePower(int btnIndex) {

        apc_powerOnBtnsImg[btnIndex].sprite = null;
        skillOnBtns[btnIndex] = PlayerSkill.NONE;
        Player.Instance.SetSkills(skillOnBtns[0], skillOnBtns[1]);
    }

    public void StartConversation(NPC npc) {

        CameraBehavior.Instance.LockCameraForCutscene = true;
        playerResourcesParent.SetActive(false);
        currentTalkIndex = 1;
        currentSpeaker = npc;
        currentTalk = npc.speech;

        talkBox.SetActive(true);
        talkBox.GetComponent<RectTransform>().anchoredPosition = npc.talkBoxPosition;
        talkBox.GetComponent<RectTransform>().sizeDelta = npc.talkBoxSize;
        timelineEventPostTalk = npc.timelineEvent;
        talkText.alignment = TextAnchor.UpperLeft;
        talkText.text = currentTalk[0];

        if(npc.oneTimeTalk) {
            npc.CanBeTalkedTo = false;
        }
        if(npc.nextPart != null) {
            followupSpeaker = npc.nextPart;
        }
        else {
            followupSpeaker = null;
        }

        if(npc.action == NPCAction.NULL) {
            actionPostTalk = null;
        }
        else if (npc.action == NPCAction.GIVE_ORB) {
            actionPostTalk = EntityGiveOrb;
        }
        else if(npc.action == NPCAction.PEACOCK_THIRD) {
            actionPostTalk = GameManager.Instance.Peacock_OpenWay;
        }
        else if(npc.action == NPCAction.LAST_ORB) {
            actionPostTalk = GameManager.Instance.TakeNextOrb;
        }
        else if(npc.action == NPCAction.DISSIPATE) {
            actionPostTalk = npc.transform.GetChild(0).GetComponent<PowerOrb>().Dissipate;
        }
        else if(npc.action == NPCAction.END_WITCH_CS) {
            actionPostTalk = () => {
                Player.Instance.GetComponent<Animator>().avatar = null;
                PromptAllPowersChange();
                GameManager.Instance.EndWitchsCutscene();
                Destroy(npc.transform.parent.gameObject);
            };
        }
        else if(npc.action == NPCAction.PROMPT_ALL_POWER) {
            actionPostTalk = PromptAllPowersChange;
        }
    }

    void EntityGiveOrb() {

        currentTalkIndex = 1;
        currentTalk = new string[1];
        string orbAcquired = skillToString[(PlayerSkill)GameManager.Instance.CurrentStage+1];
        currentTalk[0] = "Poder " + orbAcquired + " adquirido!";
        if(PlayerSkill.OWL == (PlayerSkill)GameManager.Instance.CurrentStage+1) {
            currentTalk[0] += "\n\n(equipado na tecla E)";
        }
        else if(PlayerSkill.BEAR == (PlayerSkill)GameManager.Instance.CurrentStage+1) {
            currentTalk[0] += "\n\n(equipado na tecla R)";
        }

        talkBox.SetActive(true);
        talkBox.GetComponent<RectTransform>().anchoredPosition = new Vector2(750, 750);
        talkBox.GetComponent<RectTransform>().sizeDelta = new Vector2(420, 200);
        talkText.alignment = TextAnchor.UpperCenter;
        talkText.text = currentTalk[0];

        if(followupSpeaker == null) {
            currentSpeaker.transform.GetChild(0).GetComponent<PowerOrb>().Dissipate();
        }

        actionPostTalk = GameManager.Instance.TakeNextOrb;
    }

    public void OwlGiveOrbFollowup() {

        StartConversation(followupSpeaker);
    }

    public void ShowInteractionPopup(bool active, bool canInteract) {
        
        interactionPopup.SetActive(active);
        interactionPopup.transform.GetChild(0).gameObject.SetActive(canInteract);
        interactionPopup.transform.GetChild(1).gameObject.SetActive(!canInteract);
    }

    public void ChangeHP(int HP) {
        
        for(int i = 0; i < hpBars.Length; i++) {
            if(i < HP) {
                hpBars[i].color = lifeGreen;
            }
            else {
                hpBars[i].color = lifeRed;
            }
        }
    }

    public void ShowMinimap(bool show, bool forcedOpen = false) {
        
        if(forcedOpen/* && !GameManager.Instance.DeveloperMode*/) {

            minimap.SetActive(show);
            animatingInitialMinimap = true;
            mapTabKey.SetActive(false);
            minimapObjective.SetActive(false);
            minimapMask.sizeDelta = new Vector2(160.0f, 150.0f);

            new Timer(2.0f, () => {

                System.Action<Vector3> MapOpen = (size) => {
                    minimapMask.sizeDelta = size;
                };
                new Tween(minimapMask.sizeDelta, new Vector3(1000.0f, 895.0f, 0.0f), 1.5f, false, EasingType.EaseIn, MapOpen);
                new Timer(2.5f, () => {

                    minimapObjective.SetActive(true);
                    CanvasGroup minimapObjectiveCanvasGroup = minimapObjective.GetComponent<CanvasGroup>();
                    minimapObjectiveCanvasGroup.alpha = 0.0f;
                    System.Action<float> ObjectiveMarking = (alpha) => {
                        minimapObjectiveCanvasGroup.alpha = alpha;
                    };
                    new Tween(0.0f, 1.0f, 1.25f, false, EasingType.EaseIn, ObjectiveMarking);
                    new Timer(2.0f, () => {
                        mapTabKey.SetActive(true);
                        animatingInitialMinimap = false;
                    });
                });
            });
        }
        if(minimap.activeSelf && GameManager.Instance.ShowingFixedMinimap && !animatingInitialMinimap) {
            GameManager.Instance.ShowingFixedMinimap = false;
            Player.Instance.CanControl = true;
            CameraBehavior.Instance.LockCameraForCutscene = false;
        }
        if(show && mapTabKey.activeSelf) {
            mapTabKey.SetActive(GameManager.Instance.CurrentStage == 0);
        }

        if(!animatingInitialMinimap) {
            minimap.SetActive(show);

            for(int i = 0; i < minimapTooltips.Length; i++) {
                minimapTooltips[i].SetActive(false);
            }
        }
    }

    public void ShowMinimapPiece(int index, bool show) {
        minimapPiecesParent.GetChild(index).gameObject.SetActive(show);
    }

    public void ChangeMinimapObjective(bool goToWitch) {

        int stage = GameManager.Instance.CurrentStage;

        if(!goToWitch && stage < 5) {
            minimapObjective.GetComponent<RectTransform>().position = minimapEntrances[stage + 1].GetComponent<RectTransform>().position;
        }
        else if(goToWitch) {
            minimapObjective.GetComponent<RectTransform>().position = minimapEntrances[6].GetComponent<RectTransform>().position;
        }
        else if(stage == 5) {
            minimapObjective.GetComponent<RectTransform>().position = minimapEntrances[1].GetComponent<RectTransform>().position;
        }
        else if(stage == 6) {
            minimapObjective.GetComponent<RectTransform>().position = minimapEntrances[6].GetComponent<RectTransform>().position;
        }
    }

    public void ShowLionOnMinimap() {
        minimapEntrances[6].SetActive(false);
        //minimapEntrances[7].SetActive(true);
        minimapObjective.GetComponent<RectTransform>().position = minimapEntrances[7].GetComponent<RectTransform>().position;
    }

    public void ToggleMenu() {

        if(!menu.transform.parent.gameObject.activeSelf) {
            Time.timeScale = 0.0f;
            GameManager.Instance.GameIsPaused = true;
            menu.transform.parent.GetComponent<Image>().color = new Color(0, 0, 0, 0.5f);
            menu.transform.parent.gameObject.SetActive(true);
            menu.SetActive(true);
        }
        else {
            Time.timeScale = 1.0f;
            GameManager.Instance.GameIsPaused = false;
            menu.transform.parent.gameObject.SetActive(false);
            menu.SetActive(false);
        }
    }

    public void ShowProvisoryEndScreen() {

        System.Action<float> FadeAlpha = (alpha) => {
            provisoryEndGameScreen.GetComponent<CanvasGroup>().alpha = alpha;
        };
        
        provisoryEndGameScreen.SetActive(true);
        new Tween(0, 1, 1.25f, false, EasingType.Linear, FadeAlpha);
    }

    public void ToggleElement(int index) {

        if(index == 0) {
            CameraBehavior.Instance.LockCamera = cameraLockToggle.isOn;
            lockCameraTip.SetActive(!cameraLockToggle.isOn);
        }
        else if(index == 1) {
            Player.Instance.SmartCast = smartCastToggle.isOn;
            smartCastText.text = smartCastToggle.isOn ? "(usa habilidades direto pela tecla)" : "(confirma habilidades com clique)";
        }
    }

    public void SetAlphaMaskBlack() {
        alphaMask.SetActive(true);
        alphaMask.GetComponent<Image>().color = Color.black;
    }

    public void ReturnToMainMenu() {

        Time.timeScale = 1.0f;
        SceneManager.LoadScene("MainMenu");
    }
}
