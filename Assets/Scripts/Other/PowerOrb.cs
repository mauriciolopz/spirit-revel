﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class PowerOrb : MonoBehaviour {
    
    public int Index;

    public void Dissipate() {

        transform.GetChild(0).GetComponent<ParticleSystem>().loop = false;
        transform.GetChild(1).GetComponent<ParticleSystem>().loop = false;

        new Timer(6.0f, () => Destroy(gameObject));
    }
}
