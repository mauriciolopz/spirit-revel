﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class BarrierAnimation : MonoBehaviour {

    [SerializeField] float speed;
    [SerializeField] float damagedTiling;
    public bool IsDamaged;
    Renderer selfRenderer;
    bool changedTiling = false;

	void Start () {
        selfRenderer = GetComponent<Renderer>();
    }
	
	void Update () {

        if(!IsDamaged) {
            selfRenderer.material.mainTextureOffset += new Vector2(0, speed * Time.deltaTime);
        }
        else {
            selfRenderer.material.mainTextureOffset += new Vector2(0, (speed / 6) * Time.deltaTime);
            if(!changedTiling) {
                changedTiling = true;
                selfRenderer.material.mainTextureScale = new Vector2(1, damagedTiling);
            }
        }

        if (selfRenderer.material.mainTextureOffset.y > 100000) {
            selfRenderer.material.mainTextureOffset = Vector2.zero;
        }
    }

    public void Vanish() {

        Color c = selfRenderer.material.color;
        Tween vanishTween = new Tween(1.0f, 0.0f, 1.2f, false, EasingType.Linear, (float alpha) => {
            selfRenderer.material.color = new Color(c.r, c.g, c.b, alpha);
        });
        vanishTween.OnComplete += () => {
            gameObject.SetActive(false);
        };
    }

    public void Appear() {

        Color c = selfRenderer.material.color;
        Tween vanishTween = new Tween(0.0f, 1.0f, 1.2f, false, EasingType.Linear, (float alpha) => {
            selfRenderer.material.color = new Color(c.r, c.g, c.b, alpha);
        });
    }
}
